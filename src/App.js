import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { indigo, blue, red } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';

import withSignIn from './containers/functional-hocs/withSignIn';
import appRouteMappings from './utils/appRouteMappings';

import './global.css';
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      light: indigo[300],
      main: indigo[500],
      dark: indigo[700]
    },
    secondary: {
      light: blue[300],
      main: blue[500],
      dark: blue[700]
    },
    error: {
      light: red[300],
      main: red[500],
      dark: red[700]
    }
  }
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingCompleted: false
    };
  }

  componentWillMount = async () => {
    const self = this;

    await this.props.performLogIn();
    self.setState({
      isLoadingCompleted: true
    });
  }

  componentDidMount = () => {
    
  }

  renderRoutes = () => {
    const self = this;
    const appProps = {
      isLoggedOn: this.props.sessionStates.isLoggedOn,
      accountStatus: this.props.profileInfo ? this.props.profileInfo.accountStatus : null
    };
    return (
      <Router>
        <MuiThemeProvider theme={theme}>
          <div id="app">
            <CssBaseline />
            <Switch>
              <Route path="/" exact render={(routeProps) => {
                  if (self.props.sessionStates.isLoggedOn) {
                    return <Redirect to="/users" />
                  } else {
                    return <Redirect to="/login" />
                  }
                }} />
              <Route path="/redirect" exact render={(routeProps) => {
                  if (self.props.sessionStates.isLoggedOn) {
                    if (self.props.profileInfo.accountStatus === 'PENDING_USER_PROFILE') {
                      return <Redirect to='/complete-profile' />
                    } else {
                      if (routeProps.history.location.pathname === '/'
                        || routeProps.history.location.pathname === '/redirect'
                        || routeProps.history.location.pathname === '/login'
                        || routeProps.history.location.pathname === '/register'
                        || routeProps.history.location.pathname === '/complete-profile') {
                        return <Redirect to="/users" />
                      }
                      return <Redirect to={routeProps.history.location.pathname} />
                    }
                  } else {
                    return <Redirect to="/login" />
                  }
                }} />
              <Route path="/login" exact render={(routeProps) => {
                  return appRouteMappings('/login', appProps, routeProps);
                }} />
              <Route path="/register" exact render={(routeProps) => {
                  return appRouteMappings('/register', appProps, routeProps);
                }} />
              <Route path="/complete-profile" exact render={(routeProps) => {
                  return appRouteMappings('/complete-profile', appProps, routeProps);
                }} />
              <Route path="/users" exact render={(routeProps) => {
                  return appRouteMappings('/users', appProps, routeProps);
                }} />
              <Route path="/user/:userId" render={(routeProps) => {
                  return appRouteMappings('/user/:userId', appProps, routeProps);
                }} />
              <Route path="/profile" strict={false} render={(routeProps) => {
                  return appRouteMappings('/profile', appProps, routeProps);
                }} />
              <Route path="/adores" strict={false} render={(routeProps) => {
                  return appRouteMappings('/adores', appProps, routeProps);
                }} />
              <Route path="/messages" exact render={(routeProps) => {
                  return appRouteMappings('/messages', appProps, routeProps);
                }} />
              <Route path="/messages/:userId" exact render={(routeProps) => {
                  return appRouteMappings('/messages/:userId', appProps, routeProps);
                }} />
              <Route path="/notifications" exact render={(routeProps) => {
                  return appRouteMappings('/notifications', appProps, routeProps);
                }} />
              <Route path="/settings" exact render={(routeProps) => {
                  return appRouteMappings('/settings', appProps, routeProps);
                }} />
              <Redirect to="/login" />
            </Switch>
          </div>
        </MuiThemeProvider>
      </Router>
    );
  }

  render() {
    return (
      this.state.isLoadingCompleted ? this.renderRoutes() : <h1>Loading...</h1>
    );
  }
}

export default withSignIn(App);
