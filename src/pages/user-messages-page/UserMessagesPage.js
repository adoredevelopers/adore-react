import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import HeaderContainer from '../../containers/HeaderContainer';
import ConversationListControlContainer from '../../containers/ConversationListControlContainer';
import MessageBoardHeaderContainer from '../../containers/MessageBoardHeaderContainer';
import MessageActionControlContainer from '../../containers/MessageActionControlContainer';
import MessageBodySectionContainer from '../../containers/MessageBodySectionContainer';
import navigationLinks from '../../utils/navigationLinks';

import './user-messages-page.css';
const styles = theme => ({
  root: {
    height: '100%',
    backgroundColor: 'whitesmoke'
  },
  userMessagesPage: {
    maxWidth: '1024px',
    height: '100vh',
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: '64px',
    overflow: 'none'
  },
  userMessagesPageMobileHidden: {
    paddingTop: '0',
    '@media only screen and (min-width: 600px)': {
      paddingTop: '64px'
    }
  },
  gridContainer: {
    height: '100%'
  },
  gridItemMobileHidden: {
    display: 'none',
    '@media only screen and (min-width: 600px)': {
      display: 'block'
    }
  },
  messageBoard: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: '100%',
    backgroundColor: 'white',
    borderLeft: '0'
  },
  noConversationHistory: {
    margin: 'auto'
  }
});

const mapStateToProps = state => ({
  selfUserId: state.sessionStates.selfUserId,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({

});

class UserMessagesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showUserMessageListMobile: this.props.match && this.props.match.params.userId != null ? false : true,
      currentConversationId: '',
      currentConversationUserId: '',
      shouldScrollToMessageBottom: true
    };
  }

  componentWillMount() {
    // if the request param specifies userId then resolve the message using userId
    // otherwise use the first conversation if any
    if (this.props.match && this.props.match.params.userId != null) {
      this.resolveCurrentConversationIdFromUserId(this.props.match.params.userId);
    } else {
      if (this.props.conversations != null) {
        const conversationKeys = Object.keys(this.props.conversations);
        if (conversationKeys.length > 0) {
          const conversation = this.props.conversations[conversationKeys[0]];
          this.setState({
            currentConversationId: conversation._id,
            currentConversationUserId: conversation.owners[0] === this.props.selfUserId ? conversation.owners[1] : conversation.owners[0]
          })
        }
      }
    }
  }

  resolveCurrentConversationIdFromUserId = (userId) => {
    const conversationKeys = Object.keys(this.props.conversations);
    for (let i = 0; i < conversationKeys.length; i++) {
      const conversation = this.props.conversations[conversationKeys[i]];
      if (userId === conversation.messageUser._id) {
        this.setState({
          currentConversationId: conversation._id,
          currentConversationUserId: userId
        });
        break;
      }
    }
  }

  handleUserMessageListClick = (event, conversation) => {
    this.setState({
      showUserMessageListMobile: false,
      currentConversationId: conversation._id,
      currentConversationUserId: conversation.owners[0] === this.props.selfUserId ? conversation.owners[1] : conversation.owners[0]
    });
    this.setState({
      shouldScrollToMessageBottom: true
    });
  }

  handleBackToConversationListButtonClick = (event) => {
    this.setState({
      showUserMessageListMobile: true
    });
  }

  setShouldScrollToMessageBottom = (shouldScrollToMessageBottom) => {
    this.setState({
      shouldScrollToMessageBottom: shouldScrollToMessageBottom
    });
  }

  shouldScrollToMessageBottom = () => {
    return this.state.shouldScrollToMessageBottom;
  }

  render() {
    if (this.props.isLoggedOn) {
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const classes = this.props.classes;
        return (
          <div className={classes.root}>
            <HeaderContainer
              appBarPositionFixed={true}
              hasLeftDrawer={true}
              mobileHidden={this.state.showUserMessageListMobile ? false : true} />
            <div id="userMessagesPage" className={this.state.showUserMessageListMobile ? classes.userMessagesPage : classNames(classes.userMessagesPage, classes.userMessagesPageMobileHidden)}>
              <Grid container className={classes.gridContainer}>
                <Grid item xs={12} sm={4} className={this.state.showUserMessageListMobile ? '' : classes.gridItemMobileHidden}>
                  <ConversationListControlContainer
                    currentConversationId={this.state.currentConversationId}
                    handleUserMessageListClick={this.handleUserMessageListClick} />
                </Grid>
                <Grid item xs={12} sm={8} className={this.state.showUserMessageListMobile ? classes.gridItemMobileHidden : ''}>
                  {
                    this.props.conversations != null && this.props.conversations[this.state.currentConversationId] != null
                    ?
                    <div className={classes.messageBoard}>
                      <MessageBoardHeaderContainer
                        currentConversationId={this.state.currentConversationId}
                        handleBackToConversationListButtonClick={this.handleBackToConversationListButtonClick} />
                      <MessageBodySectionContainer
                        currentConversationId={this.state.currentConversationId}
                        shouldScrollToMessageBottom={this.state.shouldScrollToMessageBottom}
                        setShouldScrollToMessageBottom={this.setShouldScrollToMessageBottom} />
                      <MessageActionControlContainer
                        currentConversationUserId={this.state.currentConversationUserId}
                        setShouldScrollToMessageBottom={this.setShouldScrollToMessageBottom} />
                    </div>
                    :
                    <div className={classes.messageBoard}>
                      <span className={classes.noConversationHistory}>No conversation selected</span>
                    </div>
                  }
                </Grid>
              </Grid>
            </div>
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    }
  }
}

UserMessagesPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(UserMessagesPage));
