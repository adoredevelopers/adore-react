import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import { REGISTRATION_FORM_FIELDS } from '../../constants/field-groups/REGISTRATION_FORM_FIELDS';
import RegistrationTopBarContainer from '../../containers/RegistrationTopBarContainer';
import withSignIn from '../../containers/functional-hocs/withSignIn';
import navigationLinks from '../../utils/navigationLinks';
import FormikRegistrationFormContainer from '../../containers/FormikRegistrationFormContainer';
import NotificationSnackbar from '../../components/notification-snackbar/NotificationSnackbar';

import './registration-page.css';
const styles = theme => ({
  root: {

  },
  registrationPage: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  logoBox: {
    width: '200px',
    marginTop: '20%',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  logoImage: {
    width: '200px'
  }
});

class RegistrationPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      swipeView: {
        index: 0
      },
      notificationSnackBar: {
        open: false
      }
    };
  }

  handleSwipeViewChange = (index) => {
    this.setState({
      swipeView: {
        index: index
      }
    });
  }

  handleSwipeViewBack = () => {
    this.setState({
      swipeView: {
        index: this.state.swipeView.index - 1
      }
    });
  }

  handleSwipeViewNext = () => {
    this.setState({
      swipeView: {
        index: this.state.swipeView.index + 1
      }
    });
  }

  showNotificationSnackbar = () => {
    this.setState({
      notificationSnackBar: {
        open: true
      }
    });
  }

  handleNotificationSnackbarClose = () => {
    this.setState({
      notificationSnackBar: {
        open: false
      }
    });
  }

  render() {
    const {
      classes,
      isLoggedOn,
      accountStatus,
      completeProfile
    } = this.props;
    if (isLoggedOn && accountStatus !== 'PENDING_USER_PROFILE') {
      return <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
    } else {
      if (isLoggedOn && accountStatus === 'PENDING_USER_PROFILE' && !completeProfile) {
        return <Redirect to={navigationLinks('COMPLETE_PROFILE_PAGE_ROUTE')} />
      }
      if (!(isLoggedOn && accountStatus === 'PENDING_USER_PROFILE') && completeProfile) {
        return <Redirect to={navigationLinks('REGISTRATION_PAGE_ROUTE')} />
      }
      return (
        <div className={classes.root}>
          <div id="registrationPage" className={classes.registrationPage}>
            <RegistrationTopBarContainer
              label={REGISTRATION_FORM_FIELDS[this.state.swipeView.index].sectionLabel}
              completeProfile={completeProfile} />

            <FormikRegistrationFormContainer
              swipeViewState={this.state.swipeView}
              changeSwipeView={this.handleSwipeViewChange}
              handleSwipeViewBack={this.handleSwipeViewBack}
              handleSwipeViewNext={this.handleSwipeViewNext}
              showNotificationSnackbar={this.showNotificationSnackbar} />

            <NotificationSnackbar
              open={this.state.notificationSnackBar.open}
              message="Please resolve all errors in the submission form."
              variant="error"
              onClose={this.handleNotificationSnackbarClose} />
          </div>
        </div>
      );
    }
  }
}

RegistrationPage.propTypes = {
  isLoggedOn: PropTypes.bool.isRequired,
  accountStatus: PropTypes.string,
  completeProfile: PropTypes.bool,
};

export default withSignIn(withStyles(styles)(RegistrationPage));
