import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import LogoImage from '../../resources/images/adore_logo@3x.png';
import navigationLinks from '../../utils/navigationLinks';

import './notifications-page.css';
const styles = theme => ({
  root: {

  },
  notificationsPage: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto'
  }
});

class NotificationsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    if (this.props.isLoggedOn) {
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const classes = this.props.classes;
        return (
          <div className={classes.root}>
            <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />

            <div id="notificationsPage" className={classNames(classes.notificationsPage, 'page-content')}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <div className={classes.logo}>
                    <img className={classes.logoImage} src={LogoImage} alt="Adore" />
                  </div>
                </Grid>
              </Grid>
            </div>

            <Footer />
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')}/>
      );
    }
  }
}

NotificationsPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(NotificationsPage);
