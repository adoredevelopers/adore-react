import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import SocialIcon from '../../components/react-social-icons/social-icon';
import LogoImage from '../../resources/images/adore_logo@3x.png';
import navigationLinks from '../../utils/navigationLinks';
import appConfig from '../../config/appConfig';
import withSignIn from '../../containers/functional-hocs/withSignIn';

import './login-page.css';
const styles = theme => ({
  root: {

  },
  loginPage: {
    // maxWidth: '1024px',
    // marginLeft: 'auto',
    // marginRight: 'auto'
    position: 'absolute',
    margin: 'auto',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  centerBlock: {
    position: 'absolute',
    margin: 'auto',
    top: '0',
    bottom: '0',
    left: '0',
    right: '0'
  },
  logo: {
    maxWidth: '200px',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  logoImage: {
    width: '100%'
  },
  section: {
    marginTop: '40px',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '0 5%',
    maxWidth: '600px',
    textAlign: 'center'
  },
  loginInputSection: {
    marginTop: '20px'
  },
  input: {
    margin: '20px 0',
    width: '100%'
  },
  loginControlSection: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '48px'
  },
  socialIcon: {
    margin: '0 10px',
    cursor: 'pointer'
  },
  loginButton: {
    marginLeft: '20px',
    height: '40px'
  },
  forgotPasswordControlSection: {

  },
  registerControlSection: {
    marginTop: '20px'
  }
});

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    if (this.props.isLoggedOn) {
      return <Redirect to={navigationLinks('REDIRECT_ROUTE')} />;
    } else {
      const classes = this.props.classes;
      return (
        <div className={classes.root}>
          <div id="loginPage" className={classes.loginPage}>
            <div className={classes.logo}>
              <img className={classes.logoImage} src={LogoImage} alt="Adore" />
            </div>
            <div className={classNames(classes.section, classes.loginControlSection)}>
              <a href={appConfig.thirdPartyAuth.linkedIn2.url} target="_self">  
                <SocialIcon network="linkedin" color="#0077B5" className={classes.socialIcon} style={{width: '42px', height: '42px'}} />
              </a>
              <a href={appConfig.thirdPartyAuth.facebook.url} target="_self">
                <SocialIcon network="facebook" color="#3B5998" className={classes.socialIcon} style={{width: '42px', height: '42px'}} />
              </a>
              {/*
              <IconButton className={classes.thirdPartyLoginButton}>
                <svg width="45" height="45" viewBox="0 0 45 45" xmlns="http://www.w3.org/2000/svg">
                  <g fill="none" fillRule="evenodd">
                    <circle fill="#3B5998" cx="22.5" cy="22.5" r="22.5" />
                    <path d="M29.03 10.005L25.78 10c-3.647 0-6.005 2.38-6.005 6.066v2.797H16.51c-.28 0-.51.224-.51.502v4.053c0 .276.23.502.51.502h3.265v10.225c0 .276.23.502.51.502h4.26c.282 0 .51-.226.51-.502V23.92h3.818c.283 0 .51-.226.51-.502l.003-4.053c0-.133-.055-.26-.15-.356-.096-.095-.225-.147-.36-.147h-3.82v-2.37c0-1.14.276-1.72 1.784-1.72h2.188c.282 0 .51-.226.51-.502V10.51c0-.278-.228-.502-.51-.503z" fill="#FFF" />
                  </g>
                </svg>
              </IconButton>
              */}
            </div>
          </div>
        </div>
      );
    }
  }
}

LoginPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withSignIn(withStyles(styles)(LoginPage));
