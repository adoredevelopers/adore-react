import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import LogoImage from '../../resources/images/adore_logo@3x.png';
import navigationLinks from '../../utils/navigationLinks';

import './settings-page.css';
const styles = theme => ({
  root: {

  },
  settingsPage: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto'
  }
});

class SettingsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    if (this.props.isLoggedOn) {
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const classes = this.props.classes;
        return (
          <div className={classes.root}>
            <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />

            <div id="settingsPage" className={classNames(classes.settingsPage, 'page-content')}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <div className={classes.logo} onClick={this.handleClickOpen}>
                    <img className={classes.logoImage} src={LogoImage} alt="Adore" />
                  </div>
                </Grid>
              </Grid>
            </div>

            <Dialog
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description">
              <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Let Google help apps determine location. This means sending anonymous location data to
                  Google, even when no apps are running.
                </DialogContentText>
              </DialogContent>
            </Dialog>

            <Footer />
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    }
  }
}

SettingsPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(SettingsPage);
