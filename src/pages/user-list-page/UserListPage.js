import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import FilterSortToolbarContainer from '../../containers/FilterSortToolbarContainer';
import UserListContainer from '../../containers/UserListContainer';
import UserListPaginationContainer from '../../containers/UserListPaginationContainer';
import SecondaryListRow from '../../components/secondary-list-row/SecondaryListRow';
import ContentSuggestionCard from '../../components/content-suggestion-card/ContentSuggestionCard';
import navigationLinks from '../../utils/navigationLinks';
import NotificationSnackbar from '../../components/notification-snackbar/NotificationSnackbar';

import './user-list-page.css';
const styles = theme => ({
  root: {

  },
  userListPage: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  containerGrid: {
    overflow: 'hidden'
  },
  noOverflow: {
    overflow: 'hidden'
  }
});

class UserListPage extends Component {
  render() {
    if (this.props.isLoggedOn) {
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const classes = this.props.classes;
        return (
          <div className={classes.root}>
            <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />

            <div id="userListPage" className={classNames(classes.userListPage, 'page-content')}>
              <Grid container spacing={24} className={classes.noOverflow}>
                <Grid item xs={12} sm={8} className={classes.noOverflow}>
                  <FilterSortToolbarContainer />
                  <UserListContainer />
                  <UserListPaginationContainer
                    type="text"
                    position="static"
                    endWithSubmitButton={false} />
                </Grid>
                <Hidden xsDown>
                  <Grid item sm={4}>
                    <Grid container spacing={16}>
                      <Grid item xs={12}>
                        <ContentSuggestionCard />
                      </Grid>
                      <Grid item xs={12}>
                        <SecondaryListRow />
                      </Grid>
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </div>

            <Footer />

            {/*
            <NotificationSnackbar
              open={this.state.notificationSnackBar.open}
              message="Please resolve all errors in the submission form."
              variant="error"
              onClose={this.handleNotificationSnackbarClose} />-->
            */}
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    }
  }
}

UserListPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(UserListPage);
