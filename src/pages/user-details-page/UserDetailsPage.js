import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import DisplayImagesContainer from '../../containers/DisplayImagesContainer';
import DisplayImageCarouselContainer from '../../containers/DisplayImageCarouselContainer';
import TopInfoContainer from '../../containers/TopInfoContainer';
import TopMessagingActionsContainer from '../../containers/TopMessagingActionsContainer';
import TopBarMobileContainer from '../../containers/TopBarMobileContainer';
import BasicInfoListContainer from '../../containers/BasicInfoListContainer';
import DetailedInfoSectionContainer from '../../containers/DetailedInfoSectionContainer';
import IntroductionsSectionContainer from '../../containers/IntroductionsSectionContainer';
import PreferenceInfoSectionContainer from '../../containers/PreferenceInfoSectionContainer';
import navigationLinks from '../../utils/navigationLinks';

import './user-details-page.css';
const styles = theme => ({
  root: {

  },
  userDetailsPage: {
    overflow: 'hidden' // material-ui defect, where nested grid causes extra width
  },
  topSection: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '64px',
    backgroundColor: 'whitesmoke',
    height: '240px',
    padding: '0',
    '@media only screen and (min-width: 600px)': {
      padding: '20px 0'
    }
  },
  topSectionMobile: {
    width: '100%'
  },
  topSectionContent: {
    display: 'flex',
    flexDirection: 'row',
    flex: '1 1 0',
    maxWidth: '1024px',
    padding: '0 20px'
  },
  topBarMobile: {

  },
  imageCarousel: {
    width: '100%'
  },
  profileDisplayImages: {

  },
  profileTopInfoActions: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignContent: 'space-between',
    justifyContent: 'space-between',
    flex: '1 0 0'
  },
  profileTopInfo: {
    flex: '0 1 0',
    width: '100%'
  },
  profileTopActions: {
    width: '100%'
  },
  profileTopInfoMobile: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '10px'
  },
  bottomSection: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: '20px',
    padding: '0',
    '@media only screen and (min-width: 600px)': {
      padding: '20px'
    },
  },
  profileBasicInfo: {
    marginTop: '-20px',
    '@media only screen and (min-width: 600px)': {
      padding: '0'
    },
  },
  profileDetailedInfo: {
    marginTop: '20px'
  },
  profileAboutInfo: {
    marginTop: '20px'
  },
  profilePreferenceInfo: {
    marginTop: '-20px'
  }
});

class UserDetailsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.match.params.userId,
      currentImageIndex: 0,
      notificationSnackBar: {
        open: false
      }
    };
    this.styleOverrides = {
      profileTopInfoStyleOverrides: {
        nameAge: {
          color: '#d34a5a',
          fontSize: '26px',
          lineHeight: '26px',
          marginBottom: '5px'
        },
        residence: {
          fontSize: '16px',
          lineHeight: '20px'
        }
      },
      profileTopInfoMobileStyleOverrides: {
        nameAge: {
          color: '#d34a5a',
          fontSize: '22px',
          lineHeight: '22px',
          marginBottom: '2px'
        },
        residence: {
          fontSize: '14px',
          lineHeight: '14px'
        }
      }
    };
  }

  componentDidMount = () => {
    window.scrollTo(0, 0);
  }

  syncCurrentImageIndex = (newIndex) => {
    this.setState({
      currentImageIndex: newIndex
    });
  }

  showNotificationSnackbar = () => {
    this.setState({
      notificationSnackBar: {
        open: true
      }
    });
  }

  handleNotificationSnackbarClose = () => {
    this.setState({
      notificationSnackBar: {
        open: false
      }
    });
  }

  render() {
    if (this.props.isLoggedOn) {
      const routeProps = {
        history: this.props.history,
        location: this.props.location,
        match: this.props.match
      };
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const classes = this.props.classes;
        return (
          <div className={classes.root}>
            <Hidden xsDown>
              <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />
            </Hidden>

            <div id="userDetailsPage" className={classes.userDetailsPage}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Hidden xsDown>
                    <div className={classes.topSection}>
                      <div className={classes.topSectionContent}>
                        <div className={classes.profileDisplayImages}>
                          <DisplayImagesContainer {...routeProps} />
                        </div>
                        <div className={classes.profileTopInfoActions}>
                          <div className={classes.profileTopInfo}>
                            <TopInfoContainer
                              styleOverrides={this.styleOverrides.profileTopInfoStyleOverrides}
                              showNotificationSnackbar={this.showNotificationSnackbar}
                              {...routeProps} />
                          </div>
                          <div className={classes.profileTopActions}>
                            <TopMessagingActionsContainer
                              isMobileView={false}
                              userId={this.state.userId}
                              {...routeProps} />
                          </div>
                        </div>
                      </div>
                    </div>
                  </Hidden>
                  <Hidden smUp>
                    <div className={classes.topSectionMobile}>
                      <div className={classes.topBarMobile}>
                        <TopBarMobileContainer
                          currentImageIndex={this.state.currentImageIndex}
                          {...routeProps} />
                      </div>
                      <div className={classes.imageCarousel}>
                        <DisplayImageCarouselContainer
                          isMobile={true}
                          currentImageIndex={this.state.currentImageIndex}
                          syncCurrentImageIndex={this.syncCurrentImageIndex}
                          {...routeProps} />
                      </div>
                      <div className={classes.profileTopInfoMobile}>
                        <TopInfoContainer
                          styleOverrides={this.styleOverrides.profileTopInfoMobileStyleOverrides}
                          {...routeProps} />
                        <TopMessagingActionsContainer
                          isMobileView={true}
                          userId={this.state.userId}
                          {...routeProps} />
                      </div>
                    </div>
                  </Hidden>
                </Grid>
                <Grid item xs={12}>
                  <div className={classes.bottomSection}>
                    <Grid container spacing={40}>
                      <Grid item xs={12} sm={7} className={classes.bottomSectionGridItem}>
                        <div className={classes.profileBasicInfo}>
                          <BasicInfoListContainer
                            isEdit={false}
                            {...routeProps} />
                        </div>
                        <div className={classes.profileDetailedInfo}>
                          <DetailedInfoSectionContainer
                            isEdit={false}
                            {...routeProps} />
                        </div>
                        <div className={classes.profileAboutInfo}>
                          <IntroductionsSectionContainer
                            isEdit={false}
                            {...routeProps} />
                        </div>
                      </Grid>
                      <Grid item xs={12} sm={5}>
                        <div className={classes.profilePreferenceInfo}>
                          <PreferenceInfoSectionContainer
                            isEdit={false}
                            {...routeProps} />
                        </div>
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </div>

            <Footer />
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    }
  }
}

UserDetailsPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(UserDetailsPage);
