import React, { Component } from 'react';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import navigationLinks from '../../utils/navigationLinks';

import './user-adores-page.css';
const styles = theme => ({
  root: {

  },
  userAdoresPage: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  card: {
    maxWidth: 250,
  },
  media: {
    height: 0,
    //paddingTop: '56.25%', // 16:9
    paddingTop: '100%'
  },
});

class UserAdoresPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    if (this.props.isLoggedOn) {
      if (this.props.accountStatus === 'PENDING_USER_PROFILE') {
        return <Redirect to='/complete-profile' />;
      } else {
        const {classes, theme} = this.props;
        return (
          <div className={classes.root}>
            <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />
            
            <div id="userAdoresPage" className={classNames(classes.userAdoresPage, 'page-content')}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <div>
                    <SwipeableViews
                      index={this.state.value}
                      onChangeIndex={this.handleChangeIndex}>
                      <Typography component="div" style={{ padding: 8 * 3 }}>
                        <Card className={classes.card}>
                          <CardMedia
                            className={classes.media}
                            image="https://via.placeholder.com/250x250"
                            title="adorer" />
                          <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                              Lizard
                            </Typography>
                            <Typography component="p">
                              Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
                              across all continents except Antarctica
                            </Typography>
                          </CardContent>
                          <CardActions>
                            <Button size="small" color="primary">
                              Share
                            </Button>
                            <Button size="small" color="primary">
                              Learn More
                            </Button>
                          </CardActions>
                        </Card>
                      </Typography>
                      <Typography component="div" style={{ padding: 8 * 3 }}>Item Two</Typography>
                      <Typography component="div" style={{ padding: 8 * 3 }}>Item Three</Typography>
                    </SwipeableViews>
                  </div>
                </Grid>
              </Grid>
            </div>

            <AppBar position="fixed" color="default" style={{top: 'auto', bottom: 0}}>
              <Tabs
                value={this.state.value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="fullWidth"
                centered>
                <Tab icon={<PhoneIcon />} label="RECENTS" />
                <Tab icon={<FavoriteIcon />} label="FAVORITES" />
                <Tab icon={<PersonPinIcon />} label="NEARBY" />
              </Tabs>
            </AppBar>

            {/* <Footer /> */}
          </div>
        );
      }
    } else {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    }
  }
}

UserAdoresPage.propTypes = {
  classes: PropTypes.object.isRequired,
  isLoggedOn: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired
};

export default withStyles(styles)(UserAdoresPage);
