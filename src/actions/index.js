export const setLoggedOnAction = (isLoggedOn, selfUserId) => ({
  type: 'SET_LOGGED_ON',
  isLoggedOn,
  selfUserId
});

export const setProfileInfoAction = (profileInfo, callback) => ({
  type: 'SET_PROFILE_INFO',
  profileInfo,
  callback
});

export const setUserProfileAction = (userProfile, callback) => ({
  type: 'SET_USER_PROFILE',
  userProfile,
  callback
});

export const setFetchingUserListAction = (isFetching) => ({
  type: 'SET_FETCHING_USER_LIST',
  isFetching
});

export const setUserListAction = (users, total, skip, limit) => ({
  type: 'SET_USER_LIST',
  users,
  total,
  skip,
  limit
});

export const resetUserListCurrentPageAction = () => ({
  type: 'RESET_USER_LIST_CURRENT_PAGE_ACTION'
});

export const decrementCurrentPageAction = (users) => ({
  type: 'DECREMENT_CURRENT_PAGE',
  users
});

export const incrementCurrentPageAction = (users) => ({
  type: 'INCREMENT_CURRENT_PAGE',
  users
});

export const setUsersSortByAction = (sortBy, callback) => ({
  type: 'SET_USERS_SORT_BY',
  sortBy,
  callback
});

export const setUsersFiltersAction = (filters) => ({
  type: 'SET_USERS_FILTERS',
  filters
});

export const appendMoreUsersAction = (users, total, skip, limit) => ({
  type: 'APPEND_MORE_USERS',
  users,
  total,
  skip,
  limit
});

export const filterUserListAction = (filters) => ({
  type: 'FILTER_USER_LIST',
  filters
});

export const setConversationsAction = (conversations) => ({
  type: 'SET_CONVERSATIONS',
  conversations
});

export const prependMessagesAction = (conversationId, messages) => ({
  type: 'PREPEND_MESSAGES',
  conversationId,
  messages
});

export const addConversationAction = (conversation) => ({
  type: 'ADD_CONVERSATION',
  conversation
});

export const addMessageAction = (message) => ({
  type: 'ADD_MESSAGE',
  message
});
