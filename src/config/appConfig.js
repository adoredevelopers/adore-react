let appConfig = require('./default.json');

if (process.env.REACT_APP_ENV != null) {
  appConfig = require('./' + process.env.REACT_APP_ENV + '.json');
}

export default appConfig;
