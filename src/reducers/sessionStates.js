let initialState = {
  isLoggedOn: false,
  selfUserId: null
};

const sessionStates = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_LOGGED_ON':
      newState = {
        ...state,
        isLoggedOn: action.isLoggedOn,
        selfUserId: action.selfUserId
      };
      break;
    default:
      newState = state;
  };
  return newState;
};

export default sessionStates;
