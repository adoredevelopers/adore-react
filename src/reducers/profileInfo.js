let initialState = null;
// {
//   "_id": "59ed730a016ea4166006aa48",
//   "email": "chaolu.dev@gmail.com",
//   "updatedAt": "2017-10-23T04:41:46.197Z",
//   "createdAt": "2017-10-23T04:41:46.197Z",
//   "lastActiveAt": 1508733706197,
//   "misc": {
//     "report": [],
//     "resetPassword": {
//       "expirationDate": null,
//       "authorizationToken": null
//     },
//     "unreadBadgeCount": 0
//   },
//   "stats": {
//     "visits": 0,
//     "adorers": 0,
//     "admirers": 0
//   },
//   "preference": {
//     "interests": [],
//     "drinking": [],
//     "smoking": [],
//     "residentialStatus": [],
//     "ethnicities": [],
//     "religion": [],
//     "income": null,
//     "education": [],
//     "languages": [],
//     "hasChildren": [],
//     "martialStatus": [],
//     "maxHeight": null,
//     "minHeight": null,
//     "maxAge": null,
//     "minAge": null,
//     "gender": []
//   },
//   "introductions": {
//     "aboutMe": ""
//   },
//   "profile": {
//     "lifestyle": {
//       "interests": [],
//       "placedTraveled": [],
//       "drinking": null,
//       "smoking": null
//     },
//     "background": {
//       "ageArrived": null,
//       "citizenship": null,
//       "residentialStatus": null,
//       "birthPlace": {
//         "country": null,
//         "province": null,
//         "city": null
//       },
//       "ethnicities": [],
//       "religion": null,
//       "income": null,
//       "jobTitle": "Technical Consultant",
//       "company": "Perficient",
//       "industry": "Computer Software",
//       "fieldOfStudy": null,
//       "graduateInstitution": null,
//       "bachelorInstitution": null,
//       "highestEducation": null
//     },
//     "basic": {
//       "willingToRelocate": null,
//       "postalCode": null,
//       "residence": {
//         "country": "ca",
//         "province": null,
//         "city": null
//       },
//       "languages": [],
//       "hasChildren": null,
//       "martialStatus": null,
//       "bodyType": null,
//       "height": null,
//       "birthday": null,
//       "gender": null,
//       "name": "Chao Lu"
//     }
//   },
//   "upFor": [],
//   "loc": {
//     "coordinates": [],
//     "type": "Point"
//   },
//   "displayImages": [
//     "https://media.licdn.com/mpr/mprx/0_Yidc0qXDkbH3MWCaxnoJsvkSh66ipHaaxhBsJzETHtb8Rdy8j1eRUxGPjPc"
//   ],
//   "accountType": "USER",
//   "thirdPartyAuthId": "cmtClS2OFx",
//   "thirdPartyAuthProvider": "linkedin",
//   "accountStatus": "ACTIVE",
//   "nickname": "Chao Lu",
//   "__v": 0
// };

const profileInfo = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_PROFILE_INFO':
      newState = action.profileInfo;
      break;
    default:
      newState = state;
  };

  action.callback && action.callback(newState);
  
  return newState;
};

export default profileInfo;
