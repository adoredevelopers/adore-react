import { combineReducers } from 'redux';
import sessionStates from './sessionStates';
import profileInfo from './profileInfo';
import conversations from './conversations';
import userList from './userList';
import settings from './settings';

const appReducers = combineReducers({
  sessionStates,
  profileInfo,
  conversations,
  userList,
  settings
});

export default appReducers;
