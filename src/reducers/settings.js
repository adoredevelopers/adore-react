let initialState = {};

const settings = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_SETTINGS':
      newState = action.settings;
      break;
    default:
      newState = state;
  };
  return newState;
};

export default settings;
