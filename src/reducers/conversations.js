// import APIClient from '../utils/services/APIClient';
// const api = new APIClient();

let initialState = {};

const conversations = (state = initialState, action) => {
  let newState = {};
  let conversation = {};
  let deltaConversationMap = {};
  switch (action.type) {
    case 'SET_CONVERSATIONS':
      action.conversations.forEach(function(conversation) {
        newState[conversation._id] = conversation;
      });
      break;
    case 'PREPEND_MESSAGES':
      conversation = state[action.conversationId];
      let updatedConversation = Object.assign({}, conversation);
      updatedConversation.messages = [...action.messages, ...updatedConversation.messages];
      deltaConversationMap[action.conversationId] = updatedConversation;
      Object.assign(newState, state, deltaConversationMap);
      break;
    case 'ADD_CONVERSATION':
      deltaConversationMap[action.conversation._id] = action.conversation;
      Object.assign(newState, state, deltaConversationMap);
      break;
    case 'ADD_MESSAGE':
      conversation = state[action.message.conversationId];
      if (conversation != null) {
        let updatedConversation = Object.assign({}, conversation);
        if (updatedConversation.messages != null) {
          updatedConversation.messages.push(action.message);
        } else {
          updatedConversation.messages = [action.message];
        }
        deltaConversationMap[action.message.conversationId] = updatedConversation;
      } else {
        // meaning it's an incoming message initiate by a new conversation user (first message)
        // to do, fetch the conversation from database
      }
      Object.assign(newState, state, deltaConversationMap);
      break;
    default:
      newState = state;
  };
  return newState;
};

export default conversations;
