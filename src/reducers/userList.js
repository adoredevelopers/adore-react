let initialState = {
  isFetching: false,
  currentPage: 0,
  total: 0,
  skip: 0,
  limit: 0,
  currentPosition: 0,
  sortBy: 0,
  filters: {}, // will be populated by App.js on bootstrap
  users: [
    // {
    //   userId: 'chao',
    //   name: 'Chao',
    //   age: 28,
    //   residence: {
    //     city: 'Vaughan',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'charles',
    //   name: 'Charles',
    //   age: 29,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jenny',
    //   name: 'Jenny',
    //   age: 23,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'frank',
    //   name: 'Frank',
    //   age: 30,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'mary',
    //   name: 'Mary',
    //   age: 22,
    //   residence: {
    //     city: 'Missisauga',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'john',
    //   name: 'John',
    //   age: 26,
    //   residence: {
    //     city: 'North York',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jingwen',
    //   name: '晋文',
    //   age: 28,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'wendy',
    //   name: 'Wendy',
    //   age: 26,
    //   residence: {
    //     city: 'Ottawa',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'trish',
    //   name: 'Patrisha',
    //   age: 20,
    //   residence: {
    //     city: 'Scarborough',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'sam',
    //   name: 'Samantha',
    //   age: 18,
    //   residence: {
    //     city: 'Milton',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'chao1',
    //   name: 'Chao',
    //   age: 28,
    //   residence: {
    //     city: 'Vaughan',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'charles1',
    //   name: 'Charles',
    //   age: 29,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jenny1',
    //   name: 'Jenny',
    //   age: 23,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'frank1',
    //   name: 'Frank',
    //   age: 30,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'mary1',
    //   name: 'Mary',
    //   age: 22,
    //   residence: {
    //     city: 'Missisauga',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'john1',
    //   name: 'John',
    //   age: 26,
    //   residence: {
    //     city: 'North York',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jingwen1',
    //   name: '晋文',
    //   age: 28,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'wendy1',
    //   name: 'Wendy',
    //   age: 26,
    //   residence: {
    //     city: 'Ottawa',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'trish1',
    //   name: 'Patrisha',
    //   age: 20,
    //   residence: {
    //     city: 'Scarborough',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'sam1',
    //   name: 'Samantha',
    //   age: 18,
    //   residence: {
    //     city: 'Milton',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'chao2',
    //   name: 'Chao',
    //   age: 28,
    //   residence: {
    //     city: 'Vaughan',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'charles2',
    //   name: 'Charles',
    //   age: 29,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jenny2',
    //   name: 'Jenny',
    //   age: 23,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'frank2',
    //   name: 'Frank',
    //   age: 30,
    //   residence: {
    //     city: 'Toronto',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'mary2',
    //   name: 'Mary',
    //   age: 22,
    //   residence: {
    //     city: 'Missisauga',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'john2',
    //   name: 'John',
    //   age: 26,
    //   residence: {
    //     city: 'North York',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'jingwen2',
    //   name: '晋文',
    //   age: 28,
    //   residence: {
    //     city: 'Markham',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'wendy2',
    //   name: 'Wendy',
    //   age: 26,
    //   residence: {
    //     city: 'Ottawa',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'trish2',
    //   name: 'Patrisha',
    //   age: 20,
    //   residence: {
    //     city: 'Scarborough',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // },
    // {
    //   userId: 'sam2',
    //   name: 'Samantha',
    //   age: 18,
    //   residence: {
    //     city: 'Milton',
    //     province: 'ON',
    //     country: 'Canada'
    //   }
    // }
  ]
};

const userList = (state = initialState, action) => {
  let newState;
  switch (action.type) {
    case 'SET_FETCHING_USER_LIST':
      newState= {
        ...state,
        isFetching: action.isFetching
      };
      break;
    case 'SET_USER_LIST':
      newState = {
        ...state,
        users: action.users,
        total: action.total,
        skip: action.skip,
        limit: action.limit
      };
      break;
    case 'RESET_USER_LIST_CURRENT_PAGE':
      newState = {
        ...state,
        currentPage: 0
      };
      break;
    case 'DECREMENT_CURRENT_PAGE':
      newState = {
        ...state,
        currentPage: state.currentPage > 0 ? state.currentPage - 1 : 0,
        skip: state.skip - state.limit
      };
      break;
    case 'INCREMENT_CURRENT_PAGE':
      newState = {
        ...state,
        currentPage: state.currentPage + 1,
        skip: state.skip + state.limit
      };
      break;
    case 'SET_USERS_SORT_BY':
      newState = {
        ...state,
        sortBy: action.sortBy
      };
      break;
    case 'SET_USERS_FILTERS':
      newState = {
        ...state,
        filters: action.filters
      };
      break;
    case 'APPEND_MORE_USERS':
      newState = {
        ...state,
        users: state.users.concat(action.users),
        total: action.total,
        skip: action.skip,
        limit: action.limit
      };
      break;
    default:
      newState = state;
  };
  return newState;
};

export default userList;
