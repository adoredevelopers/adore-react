import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';

const styles = theme => ({
  root: {

  }
});

class MainPageLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      snackBar: {
        open: false,
        message: '',
        action: null
      }
    }
  }

  openSnackBarWithMessageAndAction = (message, action) => {
    this.setState({
      snackBar: {
        open: true,
        message: message,
        action: action
      }
    });
  }

  closeSnackBar = () => {
    this.setState({
      snackBar: {
        open: false,
        message: '',
        action: null
      }
    });
  }

  render() {
    const {
      classes,
      ...otherProps
    } = this.props;
    return (
      <div className={classes.root}>
        {
          React.cloneElement(this.props.children, {
            openSnackBarWithMessageAndAction: this.openSnackBarWithMessageAndAction,
            closeSnackBar: this.closeSnackBar,
            ...otherProps
          })
        }
        <Snackbar
          anchorOrigin={{vertical: 'top', horizontal: 'right'}}
          open={this.state.snackBar.open}
          onClose={this.handleRequestClose}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={<span id="message-id">I love snacks</span>} />
      </div>
    );
  }
}

MainPageLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired
};

export default withStyles(styles)(MainPageLayout);
