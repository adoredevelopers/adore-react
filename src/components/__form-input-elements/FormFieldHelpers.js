const getFormTextField = function({className, id, type, value, isDisabled, hasError, helperText, onChange, onBlur}) {
  return (
    <TextField
      className={className}
      id={id}
      type={type}
      value={value != null ? value : ''}
      disabled={isDisabled}
      error={hasError}
      helperText={helperText != null ? helperText : null}
      onChange={onChange}
      onBlur={onBlur}
      // InputProps={{
      //   onBlur: this.handleBlur
      // }}
      // inputProps={{
      //   max: moment().subtract(18, 'years').format('YYYY-MM-DD'),
      //   min: moment().subtract(100, 'years').format('YYYY-MM-DD')
      // }}
      {...otherProps} />
  )
};

const getFormSelectField = function() {

};

const getFormRadioField = function() {

};

export default {
  getFormTextField,
  getFormSelectField,
  getFormRadioField
};