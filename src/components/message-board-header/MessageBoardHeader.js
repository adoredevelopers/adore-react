import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ImageLoader from 'react-image-loader2';

import navigationLinks from '../../utils/navigationLinks';
import TopInfo from '../../components/top-info/TopInfo';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    height: '70px',
    padding: '10px',
    zIndex: '999'
  },
  backToConversationListButton: {
    display: 'block',
    width: '30px',
    '@media only screen and (min-width: 600px)': {
      display: 'none'
    }
  },
  navigateBeforeIcon: {
    width: '30px',
    height: '30px'
  },
  messageHeaderAvatar: {
    width: '50px',
    height: '50px',
    lineHeight: '50px',
    textAlign: 'center',
    borderRadius: '50%',
    color: 'ivory',
    backgroundColor: '#333'
  },
  messageHeaderUserInfo: {
    marginLeft: '10px'
  },
  messageHeaderGap: {
    flex: '1 1 0'
  },
  messageHeaderActions: {
    marginTop: 'auto',
    marginBottom: 'auto',
    width: '30px'
  }
});

class MessageBoardHeader extends Component {
  constructor(props) {
    super(props);
    this.styleOverrides = {
      topInfoStyleOverrides: {
        nameAge: {
          fontSize: '18px',
          lineHeight: '28px',
          whiteSpace: 'nowrap',
          overflowX: 'hidden'
        },
        residence: {
          fontSize: '14px',
          lineHeight: '20px',
          whiteSpace: 'nowrap',
          overflowX: 'hidden'
        }
      }
    };
  }

  render() {
    const {
      classes,
      conversations,
      currentConversationId,
      handleBackToConversationListButtonClick
    } = this.props;
    const conversationId = currentConversationId == null ? conversations[Object.keys(conversations)[0]] : currentConversationId;
    return (
      <Paper className={classes.root}>
        <IconButton className={classes.backToConversationListButton} onClick={handleBackToConversationListButtonClick}>
          <NavigateBeforeIcon className={classes.navigateBeforeIcon} />
        </IconButton>
        <Link to={navigationLinks('USER_DETAILS_PAGE_ROUTE', conversations[conversationId].messageUser._id)}>
          <ImageLoader
            className={classes.messageHeaderAvatar}
            src={conversations[conversationId].messageUser.displayImages[0][appConfig.imageStorage][constants.imageSizes.SMALL]} />
        </Link>
        <div className={classes.messageHeaderUserInfo}>
          <TopInfo styleOverrides={this.styleOverrides.topInfoStyleOverrides} userInfo={conversations[conversationId].messageUser} />
        </div>
        <div className={classes.messageHeaderGap} />
        <IconButton className={classes.messageHeaderActions}><MoreVertIcon /></IconButton>
      </Paper>
    );
  }
}

MessageBoardHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  conversations: PropTypes.object.isRequired,
  currentConversationId: PropTypes.string,
  handleBackToConversationListButtonClick: PropTypes.func.isRequired
}

export default withStyles(styles)(MessageBoardHeader);
