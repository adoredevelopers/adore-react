import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

export const DialogTitle = withStyles(theme => ({
  root: {
    display: 'flex',
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2
  },
  titleText: {
    flex: 1
  },
  closeButton: {
    padding: '3px',
    color: theme.palette.grey[500]
  },
}))(props => {
  const { children, classes, className, classesExt, onClose } = props;
  return (
    <MuiDialogTitle disableTypography className={classNames(classes.root, className, classesExt.root || {})}>
      <Typography className={classes.titleText} variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="Close" className={classNames(classes.closeButton, classesExt.closeButton || {})} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

export const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing.unit * 2,
  },
}))(MuiDialogContent);

export const DialogActions = withStyles(theme => ({
  root: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit,
  },
}))(MuiDialogActions);
