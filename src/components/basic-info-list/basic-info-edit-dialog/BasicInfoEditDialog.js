import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import InfoEditDialogContainer from '../../../containers/InfoEditDialogContainer';
import PROFILEINFO_EDIT_DIALOG_FORM_FIELDS from '../../../constants/field-groups/PROFILEINFO_EDIT_DIALOG_FORM_FIELDS';

const styles = theme => ({
  root: {

  }
});

class BasicInfoEditDialog extends Component {
  render() {
    const {
      classes,
      ...otherProps
    } = this.props;

    return (
      <InfoEditDialogContainer tabSectionFields={PROFILEINFO_EDIT_DIALOG_FORM_FIELDS} {...otherProps} />
    );
  }
}

BasicInfoEditDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  maxWidth: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(BasicInfoEditDialog);
