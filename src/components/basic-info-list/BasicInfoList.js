import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';

import BasicInfoEditDialog from './basic-info-edit-dialog/BasicInfoEditDialog';

const styles = theme => ({
  root: {

  },
  basicInfoListTitle: {
    margin: '0 10px',
    '@media only screen and (min-width: 600px)': {
      margin: '0'
    }
  },
  basicInfoList: {
    margin: '0',
    padding: '20px',
    listStyle: 'none',
    background: 'whitesmoke'
  },
  basicInfoRow: {
    position: 'relative',
    display: 'flex',
    alignItems: 'center',
    overflow: 'hidden',
    listStyle: 'none'
  },
  basicInfoRowIcon: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '24px',
    height: '24px'
  },
  basicInfoRowText: {
    paddingLeft: '30px',
    lineHeight: '24px'
  }
});

class BasicInfoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editDialog: {
        open: false
      }
    };
  }

  handleRequestClose = () => {
    this.setState({
      editDialog: {
        open: false
      }
    });
  };

  render() {
    const classes = this.props.classes;
    const getBasicInfoListTitle = (titleText) => {
      if (this.props.isEdit) {
        return (
          <Typography
            variant="h5"
            component="h3"
            style={{display: 'flex', alignItems: 'center', marginBottom: '10px', color: '#4a90e2', cursor: 'pointer'}}
            className={classes.basicInfoListTitle}
            onClick={(event) => {
              this.setState({
                editDialog: {
                  open: true
                }
              });
            }}>
            {titleText}
            <EditIcon style={{marginLeft: '5px', width: '18px', height: '18px'}} />
          </Typography>
        );
      }
      return;
    };
    const listItemsLength = Object.keys(this.props.infoData).length;
    const listItems = Object.keys(this.props.infoData).map((key, idx) => {
      return (
        this.props.infoData[key] != null && this.props.infoData[key].text !== ''
        ?
        <li className={classes.basicInfoRow} style={idx + 1 === listItemsLength ? {marginBottom: '0'} : {marginBottom: '15px'}} key={idx}>
          <div className={classes.basicInfoRowIcon}>
            <PersonOutlineIcon />
          </div>
          <Typography className={classes.basicInfoRowText} component="span">{this.props.infoData[key].text}</Typography>
        </li>
        :
        null
      );
    });
    return (
      <div className={classes.root}>
        {getBasicInfoListTitle('Basic profile')}
        <ul style={Object.assign({}, this.props.style)} className={classes.basicInfoList}>
          {listItems}
        </ul>
        <BasicInfoEditDialog
          open={this.state.editDialog.open}
          maxWidth="sm"
          onClose={this.handleRequestClose} />
      </div>
    );
  }
}

BasicInfoList.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  isEdit: PropTypes.bool,
  infoData: PropTypes.object.isRequired
};

export default withStyles(styles)(BasicInfoList);
