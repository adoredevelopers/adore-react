import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    height: '50px',
    backgroundColor: 'rgba(0,0,0,0.35)',
    zIndex: '100'
  },
  topBarMobileBackButton: {
    width: '50px',
    height: '50px',
    color: 'ivory'
  },
  topBarMobileMoreButton: {
    width: '30px',
    height: '50px',
    color: 'ivory'
  },
  arrowBackIcon: {
    width: '30px',
    height: '30px'
  },
  imageIndexCounter: {
    color: 'white',
    lineHeight: '50px'
  },
  moreVertIcon: {
    width: '30px',
    height: '30px'
  }
});

class TopBarMobile extends Component {
  handleTopBarMobileBackButtonClick = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.props.history.goBack();
  }

  render() {
    const classes = this.props.classes;
    return (
      <div className={classes.root}>
        <IconButton
          color="secondary"
          className={classes.topBarMobileBackButton}
          onClick={this.handleTopBarMobileBackButtonClick}>
          <ArrowBackIcon className={classes.arrowBackIcon} />
        </IconButton>
        <Typography
          className={classes.imageIndexCounter}
          variant="h6"
          component="span">
          {this.props.currentImageIndex + 1} / {this.props.numberOfDisplayImages}
        </Typography>
        <IconButton
          color="secondary"
          className={classes.topBarMobileMoreButton}>
          <MoreVertIcon className={classes.moreVertIcon} />
        </IconButton>
      </div>
    );
  }
}

TopBarMobile.propTypes = {
  classes: PropTypes.object.isRequired,
  currentImageIndex: PropTypes.number.isRequired,
  numberOfDisplayImages: PropTypes.number.isRequired
}

export default withStyles(styles)(TopBarMobile);
