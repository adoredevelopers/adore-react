import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = () => ({
  root: {
    width: '100%'
  },
  radioGroup: {
    flexDirection: 'row',
    paddingTop: '12px',
    zIndex: 0
  },
  radio: {
    // padding: '0 12px'
  }
});

class FormikRadioField extends Component {
  handleChange = event => {
    this.props.onChange(this.props.fieldProps.fieldId, event.currentTarget.value);
  };

  handleBlur = () => {
    this.props.onBlur(this.props.fieldProps.fieldId, true);
  };

  render() {
    const {
      classes,
      fieldProps,
      id,
      value,
      error,
      touched
    } = this.props;
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={touched && !!error}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <RadioGroup
          id={id}
          value={value}
          className={classes.radioGroup}
          onChange={this.handleChange}>
          {
            fieldProps.fieldValues.map((fieldValue, idx) => {
              return (
                <FormControlLabel
                  label={fieldValue.label}
                  value={fieldValue.value.toString()}
                  control={<Radio color="primary" className={classes.radio} disabled={fieldProps.isDisabled} />}
                  key={fieldProps.fieldLabel + '_' + idx} />
              );
            })
          }
        </RadioGroup>
        {
          touched && !!error && <FormHelperText>{error}</FormHelperText>
        }
      </FormControl>
    );
  }
}

FormikRadioField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired
}

export default withStyles(styles)(FormikRadioField);