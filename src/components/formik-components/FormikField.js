import React from 'react';
import classNames from 'classnames';
import Fab from '@material-ui/core/Fab';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import FormHelperText from '@material-ui/core/FormHelperText';

import FormikTextField from './FormikTextField';
import FormikRadioField from './FormikRadioField';
import FormikSelectionField from './FormikSelectionField';
import DisplayImagesContainer from '../../containers/DisplayImagesContainer';
import ChangePhotosDialogContainer from '../../containers/ChangePhotosDialogContainer';

export default function FormikField(props) {
  const {
    classes,
    sectionField,
    formikVars,
    handlers,
    photoDialogOpen,
  } = props;
  if (sectionField.fieldType === 'imageField') {
    return (
      <div className={classes.displayImagesFieldRow}>
        <div>
          <DisplayImagesContainer />
          {
            formikVars.errors[sectionField.fieldId] != null
            ?
            <FormHelperText error={true}>{ formikVars.errors[sectionField.fieldId] }</FormHelperText>
            : 
            null
          }
        </div>
        <Fab
          color="secondary"
          aria-label="change photos"
          className={classes.changePhotosButton}
          onClick={handlers.handleChangePhotosButtonClick}>
          <AddAPhotoIcon />
        </Fab>
        <ChangePhotosDialogContainer
          open={photoDialogOpen}
          maxWidth="sm"
          fullScreen={window.innerWidth < 600 ? true : false}
          fieldProps={sectionField}
          onClose={handlers.handleChangePhotoDialogRequestClose} />
        <input
          id={sectionField.fieldId}
          type="text"
          style={{ display: 'none' }}
          value={formikVars.values[sectionField.fieldId]}
          onChange={(event) => {
            handlers.setFieldValue(sectionField.fieldId, event.target.value);
          }} />
      </div>
    );
  } else if (sectionField.fieldType === 'radioField') {
    return (
      <div className={classes.fieldRow}>
        <FormikRadioField
          fieldProps={sectionField}
          id={sectionField.fieldId}
          value={formikVars.values[sectionField.fieldId]}
          error={formikVars.errors[sectionField.fieldId]}
          touched={formikVars.touched[sectionField.fieldId]}
          onChange={handlers.setFieldValue}
          onBlur={(fieldId, touched) => {
            handlers.setFieldTouched(fieldId, touched);
            sectionField.fieldOnBlur && sectionField.fieldOnBlur(formikVars.values);
          }} />
      </div>
    );
  } else if (sectionField.fieldType === 'selectionField') {
    return (
      <div className={classes.fieldRow}>
        <FormikSelectionField
          fieldProps={sectionField}
          id={sectionField.fieldId}
          value={formikVars.values[sectionField.fieldId]}
          error={formikVars.errors[sectionField.fieldId]}
          touched={formikVars.touched[sectionField.fieldId]}
          onChange={handlers.setFieldValue}
          onBlur={(fieldId, touched) => {
            handlers.setFieldTouched(fieldId, touched);
            sectionField.fieldOnBlur && sectionField.fieldOnBlur(formikVars.values);
          }} />
      </div>
    );
  } else if (sectionField.fieldType === 'comboSelectionField') {
    return (
      <div className={classes.fieldRow}>
        {
          sectionField.itemFields.map((itemFieldProps, itemFieldIdx) => {
            const isLastItem = (itemFieldIdx === sectionField.itemFields.length - 1);
            const resolvedClassName = isLastItem
              ? classNames(classes.fieldRowItem, classes.fieldRowItemLast)
              : classes.fieldRowItem;
            return (
              <div className={resolvedClassName} key={'sectionItemField_' + itemFieldIdx}>
                <FormikSelectionField
                  fieldProps={itemFieldProps}
                  id={sectionField.fieldId}
                  value={formikVars.values[itemFieldProps.fieldId]}
                  error={formikVars.errors[itemFieldProps.fieldId]}
                  touched={formikVars.touched[itemFieldProps.fieldId]}
                  onChange={handlers.setFieldValue}
                  onBlur={(fieldId, touched) => {
                    handlers.setFieldTouched(fieldId, touched);
                    sectionField.fieldOnBlur && sectionField.fieldOnBlur(formikVars.values);
                  }} />
              </div>
            );
          })
        }
      </div>
    );
  } else if (sectionField.fieldType === 'comboTextField') {
    return (
      <div className={classes.fieldRow}>
        {
          sectionField.itemFields.map((itemFieldProps, itemFieldIdx) => {
            const isLastItem = (itemFieldIdx === sectionField.itemFields.length - 1);
            const resolvedClassName = isLastItem
              ? classNames(classes.fieldRowItem, classes.fieldRowItemLast)
              : classes.fieldRowItem;
            return (
              <div className={resolvedClassName} key={'sectionItemField_' + itemFieldIdx}>
                <FormikTextField
                  fieldProps={itemFieldProps}
                  id={sectionField.fieldId}
                  value={formikVars.values[itemFieldProps.fieldId]}
                  error={formikVars.errors[itemFieldProps.fieldId]}
                  touched={formikVars.touched[itemFieldProps.fieldId]}
                  onChange={handlers.setFieldValue}
                  onBlur={(fieldId, touched) => {
                    handlers.setFieldTouched(fieldId, touched);
                    sectionField.fieldOnBlur && sectionField.fieldOnBlur(formikVars.values);
                  }} />
              </div>
            );
          })
        }
      </div>
    );
  } else {
    return (
      <div className={classes.fieldRow}>
        <FormikTextField
          fieldProps={sectionField}
          id={sectionField.fieldId}
          value={formikVars.values[sectionField.fieldId]}
          error={formikVars.errors[sectionField.fieldId]}
          touched={formikVars.touched[sectionField.fieldId]}
          onChange={handlers.setFieldValue}
          onBlur={(fieldId, touched) => {
            handlers.setFieldTouched(fieldId, touched);
            sectionField.fieldOnBlur && sectionField.fieldOnBlur(formikVars.values);
          }} />
      </div>
    );
  }
}
