import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';

const styles = () => ({
  root: {
    width: '100%'
  },
  textField: {

  }
});

class FormikTextField extends Component {
  resolveTextFieldType = (textFieldType) => {
    switch (textFieldType) {
      case 'textField':
        return 'text';
      case 'emailTextField':
        return 'email';
      case 'passwordTextField':
        return 'password';
      case 'numberTextField':
        return 'number';
      case 'dateTextField':
        return 'date';
      case 'colorTextField':
        return 'color';
      case 'hiddenTextField':
        return 'hidden';
      default:
        return '';
    }
  }

  handleChange = event => {
    this.props.onChange(this.props.fieldProps.fieldId, event.currentTarget.value);
  };

  handleBlur = () => {
    this.props.onBlur(this.props.fieldProps.fieldId, true);
  };

  render() {
    const {
      classes,
      fieldProps,
      id,
      value,
      error,
      touched
    } = this.props;
    const textFieldType = this.resolveTextFieldType(fieldProps.fieldSubType);
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={touched && !!error}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <TextField
          id={id}
          type={this.resolveTextFieldType(fieldProps.fieldSubType)}
          value={value}
          className={classes.textField}
          disabled={fieldProps.isDisabled}
          error={touched && !!error}
          helperText={touched && !!error && error}
          onChange={this.handleChange}
          InputProps={{
            onBlur: this.handleBlur
          }}
          inputProps={
            fieldProps.fieldId === 'birthday' && textFieldType === 'date'
            ?
            {
              max: moment().subtract(18, 'years').format('YYYY-MM-DD'),
              min: moment().subtract(100, 'years').format('YYYY-MM-DD')
            }
            :
            {}
          } />
      </FormControl>
    );
  }
}

FormikTextField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired
}

export default withStyles(styles)(FormikTextField);