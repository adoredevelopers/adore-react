import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Select from "react-select";
import Creatable from 'react-select/lib/Creatable';

const styles = () => ({
  root: {
    width: '100%'
  },
  selectionField: {
    marginTop: '10px',
    minWidth: '100px'
  }
});

class FormikSelectionField extends Component {
  handleChange = value => {
    this.props.onChange(this.props.fieldProps.fieldId, value);
  };

  handleBlur = () => {
    this.props.onBlur(this.props.fieldProps.fieldId, true);
  };

  render() {
    const {
      classes,
      fieldProps,
      id,
      value,
      error,
      touched
    } = this.props;
    const SelectionField = fieldProps.isCreatable ? Creatable : Select;
    const getOptions = (options, selectedOption) => {
      if (selectedOption instanceof Array) {
        selectedOption.forEach((option) => {
          options = mergeOption(options, option);
        });
      } else {
        mergeOption(options, selectedOption);
      }
      return options;
    };
    const mergeOption = (options, selectedOption) => {
      if (selectedOption != null && selectedOption !== '') {
        let found = false;
        for (let i = 0; i < options.length; i++) {
          if (options[i].value === selectedOption.value) {
            found = true;
            break;
          }
        }
        if (!found) {
          options.push(selectedOption);
        }
      }
      return options;
    };
    return (
      <FormControl
        className={classes.root}
        required={fieldProps.isRequired}
        error={touched && !!error}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <SelectionField
          id={id}
          instanceId={id}
          isMulti={fieldProps.isMulti}
          options={getOptions(fieldProps.fieldValues, value)}
          value={value}
          className={classes.selectionField}
          isDisabled={fieldProps.isDisabled}
          onChange={this.handleChange}
          onBlur={this.handleBlur} />
          {
            touched && !!error && <FormHelperText>{error}</FormHelperText>
          }
      </FormControl>
    );
  }
}

FormikSelectionField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
}

export default withStyles(styles)(FormikSelectionField);