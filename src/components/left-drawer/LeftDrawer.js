import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Hidden from '@material-ui/core/Hidden';
import UserListIcon from '@material-ui/icons/ViewList';
import FavouiteIcon from '@material-ui/icons/Favorite';
// import MessagesIcon from '@material-ui/icons/Forum';
import MessagesIcon from '@material-ui/icons/Chat';
import NotificationsIcon from '@material-ui/icons/Notifications';
// import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import SettingsIcon from '@material-ui/icons/Settings';
// import LogoutIcon from '@material-ui/icons/Delete';
import LogoutIcon from '@material-ui/icons/Report';

import navigationLinks from '../../utils/navigationLinks';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';
import LogoImage from '../../resources/images/adore_logo@2x.png';
// import DefaultProfileImage from '../../resources/images/default_profile_image.jpg';

const styles = theme => ({
  root: {
    width: '250px'
  },
  topBar: {
    backgroundColor: 'whitesmoke'
  },
  toolBar: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center'
  },
  logoImageLink: {
    height: '56px',
    '@media only screen and (min-width: 600px)': {
      height: '64px'
    }
  },
  logoImage: {
    height: '56px',
    '@media only screen and (min-width: 600px)': {
      height: '64px'
    }
  },
  drawerContent: {
    marginTop: '56px',
    '@media only screen and (min-width: 600px)': {
      marginTop: '64px'
    }
  },
  userInfoListItem: {
    cursor: 'pointer'
  },
  userInfo: {
    display: 'flex',
    alignItems: 'center',
    height: '60px'
  },
  avatar: {
    width: '40px',
    height: '40px'
  },
  userInfoText: {
    marginLeft: '10px',
    height: '60px'
  },
  userInfoTextUsername: {
    padding: '12px 0 0 0',
    height: '36px',
    lineHeight: '20px',
    fontSize: '18px'
  },
  userInfoTextSmall: {
    padding: '0 0 14px 0',
    height: '24px',
    lineHeight: '14px',
    fontSize: '12px',
    color: 'gray'
  },
  userInfoDivider: {
    margin: '-1px 15px 0px 15px'
  },
  list: {
    width: '250px',
    flex: 'initial',
  },
  listFull: {
    width: 'auto',
    flex: 'initial',
  }
});

class LeftDrawer extends Component {
  handleClose = (event) => {
    this.props.closeDrawer();
  };

  render() {
    const classes = this.props.classes;
    const mailFolderListItems = (
      <div>
        <Link to={navigationLinks('USER_LIST_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <ListItem button onClick={this.handleClose}>
            <ListItemIcon>
              <UserListIcon />
            </ListItemIcon>
            <ListItemText primary="Users" />
          </ListItem>
        </Link>
        <Link to={navigationLinks('USER_ADORES_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <ListItem button onClick={this.handleClose}>
            <ListItemIcon>
              <FavouiteIcon />
            </ListItemIcon>
            <ListItemText primary="Adores" />
          </ListItem>
        </Link>
        <Link to={navigationLinks('USER_MESSAGES_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <ListItem button onClick={this.handleClose}>
            <ListItemIcon>
              <MessagesIcon />
            </ListItemIcon>
            <ListItemText primary="Messages" />
          </ListItem>
        </Link>
      </div>
    );
    const otherMailFolderListItems = (
      <div>
        <Link to={navigationLinks('NOTIFICATIONS_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <ListItem button>
            <ListItemIcon>
              <NotificationsIcon />
            </ListItemIcon>
            <ListItemText primary="Notifications" />
          </ListItem>
        </Link>
        <Link to={navigationLinks('SETTINGS_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <ListItem button>
            <ListItemIcon>
              <SettingsIcon />
            </ListItemIcon>
            <ListItemText primary="Settings" />
          </ListItem>
        </Link>
        <Hidden smUp>
          <ListItem button onClick={this.props.performLogOut}>
            <ListItemIcon>
              <LogoutIcon />
            </ListItemIcon>
            <ListItemText primary="Sign Out" />
          </ListItem>
        </Hidden>
      </div>
    );
    const fullList = (
      <div>
        <List className={classes.list} disablePadding>
          {mailFolderListItems}
        </List>
        <Divider />
        <List className={classes.list} disablePadding>
          {otherMailFolderListItems}
        </List>
      </div>
    );
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Drawer
          anchor="left"
          open={this.props.drawerOpen}
          onClose={this.handleClose}>
          <AppBar className={classes.topBar}>
            <Toolbar className={classes.toolBar}>
              <Link to={navigationLinks('HOME_ROUTE')} className={classes.logoImageLink}>
                <img className={classes.logoImage} src={LogoImage} alt="Adore" />
              </Link>
            </Toolbar>
          </AppBar>
          <div className={classes.drawerContent}>
            <Hidden smUp>
              <div>
                <Link to={navigationLinks('EDIT_PROFILE_PAGE_ROUTE')} style={{color: 'black', textDecoration: 'none', outline: '0'}}>
                  <ListItem className={classes.userInfoListItem}>
                    <div className={classes.userInfo}>
                      <Avatar className={classes.avatar} src={this.props.userInfo.displayImage[appConfig.imageStorage][constants.imageSizes.SMALL]} />
                      <div className={classes.userInfoText}>
                        <div className={classes.userInfoTextUsername}>{this.props.userInfo.name}</div>
                        <div className={classes.userInfoTextSmall}>click to modify profile</div>
                      </div>
                    </div>
                  </ListItem>
                </Link>
                <Divider className={classes.userInfoDivider} />
              </div>
            </Hidden>
            {fullList}
          </div>
        </Drawer>
      </div>
    );
  }
}

LeftDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  drawerOpen: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
  performLogOut: PropTypes.func.isRequired
};

export default withStyles(styles)(LeftDrawer);
