import React, { Component } from 'react';
import { withStyles } from '@material-ui/core'
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SearchIcon from '@material-ui/icons/Search';
import EditIcon from '@material-ui/icons/Edit';
import ImageLoader from 'react-image-loader2';

import SearchBar from '../search-bar/SearchBar';
import { getAgeFromBirthDate } from '../../utils/helpers/commonHelpers';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    backgroundColor: 'white',
    border: '1px solid lightgray'
  },
  searchFilterBar: {
    display: 'flex',
    height: '50px',
    padding: '0 0 0 10px',
    borderBottom: '1px solid lightgray'
  },
  searchInputBox: {
    flex: '1 1 0',
    alignSelf: 'center'
  },
  searchButton: {
    alignSelf: 'flex-end',
    width: '30px'
  },
  editButton: {
    alignSelf: 'flex-end',
    width: '30px'
  },
  conversationList: {
    flex: '1 1 0',
    overflowX: 'none',
    overflowY: 'auto'
  },
  conversationListAvatar: {
    width: '40px',
    height: '40px',
    lineHeight: '40px',
    textAlign: 'center',
    borderRadius: '50%',
    color: 'ivory',
    backgroundColor: '#333'
  }
});

class ConversationListControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: null,
      filterHiddenConversations : {},
    }
  }
  
  handleSearchTermChange = (event) => {
    this.setState({
      searchTerm: event.currentTarget.value
    }, () => {
      this.performSearch(this.state.searchTerm);
    });
  }

  performSearch = (searchTerm) => {
    if (searchTerm != null) {
      let filterHiddenConversations = this.state.filterHiddenConversations;
      this.props.conversations && Object.keys(this.props.conversations).forEach((conversationKey, idx) => {
        const conversation = this.props.conversations[conversationKey];
        const searchTermUpperCase = searchTerm.toUpperCase();
        if (conversation.messageUser.profile.basic.name.toUpperCase().startsWith(searchTermUpperCase)) {
        // if (conversation.messageUser.profile.basic.name.toUpperCase().indexOf(searchTermUpperCase) !== -1) {
          filterHiddenConversations[conversationKey] = false; // not hidden
        } else {
          filterHiddenConversations[conversationKey] = true; // hide
        }
      });
      this.setState({
        filterHiddenConversations: filterHiddenConversations
      });
    }
  }

  render() {
    const {
      classes,
      conversations,
      currentConversationId,
      handleUserMessageListClick
    } = this.props;
    const conversationKeys = Object.keys(conversations);
    const listItems = conversationKeys.map((conversationKey, idx) => {
      const conversation = conversations[conversationKey];
      const styleBackgroundColor = (conversation._id === currentConversationId) ? {backgroundColor: 'rgba(0,0,0,0.1)'} : {backgroundColor: 'transparent'};
      const styleDisplay = this.state.filterHiddenConversations[conversation._id] ? {display: 'none'} : {display: 'flex'};
      return (
        <ListItem
          button
          style={Object.assign(styleBackgroundColor, styleDisplay)}
          key={conversation._id}
          onClick={(event) => handleUserMessageListClick(event, conversation)}>
          <ImageLoader
            className={classes.conversationListAvatar}
            src={conversation.messageUser.displayImages[0][appConfig.imageStorage][constants.imageSizes.SMALL]} />
          <ListItemText primary={conversation.messageUser.profile.basic.name + ', ' + getAgeFromBirthDate(conversation.messageUser.profile.basic.birthday)} secondary={conversation.messages[conversation.messages.length - 1].message} />
        </ListItem>
      );
    });
    return (
      <div className={classes.root}>
        {/*
        <div className={classes.searchFilterBar}>
          <Input
            placeholder="find user by name..."
            className={classes.searchInputBox}
            onChange={this.handleSearchTermChange} />
          <IconButton className={classes.searchButton} disableRipple onClick={this.performSearch}><SearchIcon /></IconButton>
          <IconButton className={classes.editButton}><EditIcon /></IconButton>
        </div>
        */}
        <SearchBar performSearch={this.handleSearchTermChange} />
        <List className={classes.conversationList}>
          {listItems}
        </List>
      </div>
    );
  }
}

ConversationListControl.propTypes = {
  classes: PropTypes.object.isRequired,
  conversations: PropTypes.object.isRequired,
  currentConversationId: PropTypes.string.isRequired,
  handleUserMessageListClick: PropTypes.func.isRequired
}

export default withStyles(styles)(ConversationListControl);
