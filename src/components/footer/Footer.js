import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ImageLoader from 'react-image-loader2';

import NotificationSnackbarContainer from '../../containers/NotificationSnackbarContainer';
import ProudlyCanadianImage from '../../resources/images/proudly_canadian@2x.png';

const styles = theme => ({
  root: {
    width: '100%',
    padding: '20px',
    backgroundColor: 'rgb(33, 33, 33)'
  },
  proudlyCanadian: {
    margin: '40px auto 40px',
    padding: '0',
    width: '200px',
    height: '34px',
    backgroundColor: '#212121'
  },
  img: {
    width: '200px',
    height: '34px',
    lineHeight: '34px',
    textAlign: 'center',
    color: 'ivory'
  }
});

class Footer extends Component {
  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <div className={classes.proudlyCanadian}>
              <ImageLoader className={classes.img} src={ProudlyCanadianImage} alt="Proudly Canadian" />
            </div>
            <NotificationSnackbarContainer />
          </Grid>
        </Grid>
      </div>
    );
  }
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object
};

export default withStyles(styles)(Footer);
