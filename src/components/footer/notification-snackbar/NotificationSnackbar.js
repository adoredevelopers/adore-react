import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
    padding: '20px',
    backgroundColor: 'rgb(33, 33, 33)'
  }
});

class NotificationSnackbar extends Component {
  render() {
    const classes = this.props.classes;
    return (
      <div className={classes.root}>
      </div>
    );
  }
}

NotificationSnackbar.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object
};

export default withStyles(styles)(NotificationSnackbar);
