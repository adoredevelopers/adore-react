import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {

  }
});

class DetailedInfoSection extends Component {
  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>

      </div>
    );
  }
}

DetailedInfoSection.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  edit: PropTypes.bool
};

export default withStyles(styles)(DetailedInfoSection);
