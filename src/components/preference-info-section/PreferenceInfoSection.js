import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';

import PreferenceInfoEditDialog from './preference-info-edit-dialog/PreferenceInfoEditDialog';

const styles = theme => ({
  root: {

  },
  preferenceInfoRow: {

  },
  preferenceInfoRowTitle: {
    fontSize: '22px',
    lineHeight: '40px',
    color: '#d34a5a',
    margin: '0 10px',
    '@media only screen and (min-width: 600px)': {
      margin: '0'
    }
  },
  preferenceInfoRowText: {
    padding: '10px',
    fontSize: '16px',
    lineHeight: '26px'
  }
});

class PreferenceInfoSection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editDialog: {
        open: false
      }
    };
  }
  
  handleRequestClose = () => {
    this.setState({
      editDialog: {
        open: false
      }
    });
  }

  render() {
    const {
      classes,
      infoData
    } = this.props;
    const commonInterestsSection = this.props.isEdit ? '' : (
      <div className={classes.preferenceInfoRow}>
        <Typography variant="h5" component="h3" className={classes.preferenceInfoRowTitle}>Common interests</Typography>
        <Typography component="p" paragraph className={classes.preferenceInfoRowText}>
          唱歌, 旅游
        </Typography>
      </div>
    );
    const getRowTitle = (titleText) => {
      if (this.props.isEdit) {
        return (
          <Typography
            variant="h5"
            component="h3"
            style={{display: 'flex', alignItems: 'center', color: '#4a90e2', cursor: 'pointer'}}
            className={classes.preferenceInfoRowTitle}
            onClick={(event) => {
              this.setState({
                editDialog: {
                  open: true
                }
              });
            }}>
            {titleText}
            <EditIcon style={{marginLeft: '5px', width: '18px', height: '18px'}} />
          </Typography>
        );
      } else {
        return <Typography variant="h5" component="h3" className={classes.preferenceInfoRowTitle}>{titleText}</Typography>;
      }
    };
    const getRowBody = () => {
      const hasPreference = Object.values(infoData).reduce((accum, curr) => accum + (curr == null ? 0 : curr.text.length), 0) > 0;
      if (hasPreference) {
        return Object.keys(infoData).map((key, idx) => {
          if (infoData[key] != null) {
            return (
              <Typography component="p" paragraph className={classes.preferenceInfoRowText} key={'preferenceRow_' + idx}>
                {infoData[key].text}
              </Typography>
            );
          } else {
            return null;
          }
        });
      } else {
        return (
          <Typography
            component="p"
            paragraph
            className={classes.preferenceInfoRowText}
            style={{color: 'darkgrey', fontSize: '14px', fontStyle: 'italic'}}>
            Leave blank...
          </Typography>
        );
      }
    };
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        {commonInterestsSection}
        <div className={classes.preferenceInfoRow}>
          {getRowTitle('Looking for')}
          {getRowBody()}
        </div>
        <PreferenceInfoEditDialog
          open={this.state.editDialog.open}
          maxWidth="xs"
          onClose={this.handleRequestClose} />
      </div>
    );
  }
}

PreferenceInfoSection.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  isEdit: PropTypes.bool,
  infoData: PropTypes.object.isRequired
};

export default withStyles(styles)(PreferenceInfoSection);
