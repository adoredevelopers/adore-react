import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import Button from '@material-ui/core/Button';

import FormikField from '../formik-components/FormikField';
import FormikState from '../formik-components/FormikState';
// import { DialogTitle, DialogContent, DialogActions } from '../custom-dialog/CustomDialog';
import UserProfile from '../../models/UserProfile';
import { normalizeFormValues } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    
  },
  dialog: {

  },
  dialogPaper: {
    maxHeight: '100%'
  },
  dialogTitle: {
    // padding: '10px 0',
    padding: 0,
    // border: 'none',
    backgroundColor: theme.palette.background.appBar
  },
  // dialogTitleCloseButton: {
  //   width: '48px',
  //   padding: '12px',
  //   margin: '0 10px'
  // },
  dialogContent: {
    flex: '1 1 auto',
    padding: 0
  },
  tabView: {
    padding: '36px',
    overflowY: 'auto'
  },
  fieldRow: {
    display: 'block',
    marginBottom: '20px',
    width: '100%',
    '@media only screen and (min-width: 600px)': {
      display: 'flex',
      justifyContent: 'space-between'
    }
  },
  fieldRowItem: {
    marginBottom: '20px',
    '@media only screen and (min-width: 600px)': {
      flex: '1 1 0',
      marginRight: '10px',
      marginBottom: '0'
    }
  },
  fieldRowItemLast: {
    marginRight: '0'
  },
  formControl: {
    width: '100%'
  },
  radioGroup: {
    flexDirection: 'row',
    zIndex: 0
  },
  input: {
    width: '100%'
  },
  select: {
    marginTop: '10px',
    minWidth: '100px'
  }
});

class InfoEditDialog extends Component {
  constructor(props) {
    super(props);
    this.DIALOG_NON_FULLSCREEN_WINDOW_HEIGHT_RATIO = 0.9;
    this.DIALOG_TITLE_HEIGHT = 48;
    this.DIALOG_ACTIONS_HEIGHT = 54;
    this.state = {
      tabBar: {
        activeTabIndex: 0
      },
      showFormikState: true,
      windowInnerHeight: window.innerHeight
    };
  }
  
  componentDidMount = () => {
    this.updateViewPortDimensions();
    window.addEventListener('resize', this.updateViewPortDimensions);
  }
  
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateViewPortDimensions);
  }

  updateViewPortDimensions = () => {
    this.setState({
      windowInnerHeight: window.innerHeight
    });
  }
  
  // event handling functions
  onDialogEnter = () => {
    this.setState({
      tabBar: {
        activeTabIndex: 0
      }
    });
  }

  handleTabBarChange = (event, index) => {
    this.setState({
      tabBar: {
        activeTabIndex: index
      }
    });
  }

  handleTabSwipeChange = (index) => {
    this.setState({
      tabBar: {
        activeTabIndex: index
      }
    });
  }

  handleCancel = (event) => {
    this.props.resetFormFields();
    this.handleRequestClose();
  }

  handleSubmit = (event) => {
    this.props.handleSubmit(event);
    if (!this.props.isValid) {
      // this.setState({
      //   tabBar: {
      //     activeTabIndex: 0
      //   }
      // });
      this.props.showNotificationSnackbar();
    }
    this.handleRequestClose();
  }

  handleRequestClose = (event) => {
    this.setState({
      tabBar: {
        activeTabIndex: 0
      }
    });
    this.props.onClose();
  }

  render() {
    const {
      classes,
      values,
      touched,
      dirty,
      errors,
      handleChange,
      handleBlur,
      isSubmitting,
      handleSubmit,
      handleReset,

      open,
      maxWidth,
      setLoggedOn,
      setConversations,
      addConversation,
      addMessage,
      tabSectionFields,
      profileInfoDelta,
      resetFormFields,
      handleFormFieldChange,
      submitServiceRequest,
      onClose,
      ...otherProps
    } = this.props;
    const tabViewStyleHeight = 
      this.state.windowInnerHeight < 600
      ?
      this.state.windowInnerHeight - this.DIALOG_TITLE_HEIGHT - this.DIALOG_ACTIONS_HEIGHT
      :
      this.state.windowInnerHeight * this.DIALOG_NON_FULLSCREEN_WINDOW_HEIGHT_RATIO - this.DIALOG_TITLE_HEIGHT - this.DIALOG_ACTIONS_HEIGHT;
    const formikVars = {
      values,
      errors,
      touched
    };
    const handlers = {
      setFieldValue: this.props.setFieldValue,
      setFieldTouched: this.props.setFieldTouched,
    };
    return (
      <form className={classes.root} onSubmit={this.handleSubmit.bind(this)}>
        <Dialog
          className={classes.dialog}
          classes={{
            paper: classes.dialogPaper,
          }}
          open={open}
          maxWidth={maxWidth}
          fullScreen={this.state.windowInnerHeight < 600 ? true : false}
          onClose={onClose}
          onEnter={this.onDialogEnter}
          onBackdropClick={this.handleCancel}
          onEscapeKeyDown={this.handleCancel}>
          <DialogTitle className={classes.dialogTitle}>
            <Tabs
              variant="fullWidth"
              value={this.state.tabBar.activeTabIndex}
              indicatorColor="primary"
              onChange={this.handleTabBarChange}>
              {
                tabSectionFields.map((tabSection, idx) => {
                  return (
                    <Tab label={tabSection.sectionLabel} key={'tabTitle_' + idx} style={tabSectionFields.length === 1 ? {maxWidth: '100%'} : {}} />
                  );
                })
              }
            </Tabs>
          </DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <SwipeableViews
              index={this.state.tabBar.activeTabIndex}
              onChangeIndex={this.handleTabSwipeChange}>
              {
                tabSectionFields.map((tabSection, idx) => {
                  return (
                    <div className={classes.tabView} style={{height: tabViewStyleHeight}} key={'tabContent_' + idx}>
                      {
                        tabSection.sectionFields.map((sectionField, sectionFieldIdx) => {
                          return <FormikField
                            classes={classes}
                            sectionField={sectionField}
                            formikVars={formikVars}
                            handlers={handlers}
                            photoDialogOpen={false}
                            key={'sectionField_' + sectionFieldIdx} />
                        })
                      }
                    </div>
                  );
                })
              }
            </SwipeableViews>

            <FormikState show={this.state.showFormikState} {...this.props} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleCancel}>Cancel</Button>
            <Button color="primary" onClick={this.handleSubmit}>Submit</Button>
          </DialogActions>
        </Dialog>
      </form>
    );
  }
}

InfoEditDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  tabSectionFields: PropTypes.array.isRequired,
  profileInfoDelta: PropTypes.object.isRequired,
  resetFormFields: PropTypes.func.isRequired,
  submitServiceRequest: PropTypes.func.isRequired,
  updateUserProfile: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  maxWidth: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

export default withFormik({
  validationSchema: Yup.object().shape({
    // displayImages: Yup.array()
    //   .min("Please choose at least one display image.")
    //   .max("Cannot exceed maximum of 8 display images."),
    // email: Yup.string()
    //   .required('Email is required!')
    //   .email('Email format incorrect'),
    // password: Yup.string()
    //   .required('Password is required!')
    //   .min(8, 'Password has to be minimum 8 characters.')
    //   .max(16, 'password cannot exceed 16 characters.'),
    // passwordVerify: Yup.string()
    //   .required('Password verification is required!')
    //   .oneOf([Yup.ref('password')], 'Password does not match.'),
    // gender: Yup.array()
    //   .of(
    //     Yup.object().shape({
    //       label: Yup.string().required(),
    //       value: Yup.string().required()
    //     })
    //   )
    //   .required("Gender is required!")
    name: Yup.string()
    .required('Name is required!'),
    gender: Yup.string()
      .required("Gender is required!"),
    birthday: Yup.string()
      .required("Birthday is required!"),
    height: Yup.string()
      .required('Height is required!'),
    martialStatus: Yup.string()
      .required('Martial status is required!'),
    languages: Yup.string()
      .required('Language is required!'),
    postalCode: Yup.string()
      .required('Postal code is required!'),
    // country: Yup.string()
    //   .required('Country is required!'),
    // province: Yup.string()
    //   .required('Province is required!'),
    // city: Yup.string()
    //   .required('City is required!')
  }),
  mapPropsToValues: props => {
    if (props.profileInfo != null) {
      const userProfile = new UserProfile();
      userProfile.initFromApplicationState(props.profileInfo);
      return userProfile.getRegistrationFormFields();
    }
    const values = {};
    return values;
  },
  enableReinitialize: true,
  handleSubmit: (values, formikBag) => {
    const normalizedValues = normalizeFormValues(values);
    const userProfile = new UserProfile();
    userProfile.initFromRegistrationForm(normalizedValues);
    const payload = userProfile.getAllFields(true);
    payload.accountStatus = 'ACTIVE';
    console.debug(payload);
    formikBag.props.updateUserProfile(payload);
    setTimeout(() => {
      formikBag.setSubmitting(false);
    }, 1000);
  },
  displayName: "InfoEditForm"
})(withStyles(styles)(InfoEditDialog));
