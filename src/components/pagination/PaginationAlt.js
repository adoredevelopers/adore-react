import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    margin: '0',
    padding: '8px',
    flexDirection: 'row',
    justifyContent: 'space-between',
    background: '#fafafa',
    '@media only screen and (min-width: 600px)': {

    }
  },
  button: {

  },
  prevButton: {

  },
  nextButton: {

  }
});

class Pagination extends Component {
  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Button className={classNames(classes.button, classes.prevButton)}>&lt; Previous</Button>
        <Button className={classNames(classes.button, classes.nextButton)}>Next 	&gt;</Button>
      </div>
    );
  }
}

Pagination.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object
};

export default withStyles(styles)(Pagination);
