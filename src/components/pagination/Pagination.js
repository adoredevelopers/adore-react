import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

const styles = theme => ({
  root: {

  }
});

class Pagination extends Component {
  submitServiceRequest = () => {
    const self = this;
    this.props.submitServiceRequest(function(hasError) {
      if (hasError) {
        self.props.displayErrorDialog();
      }
    });
  }

  handleBack = () => {
    this.props.handleBack();
    this.scrollToTop();
  }

  handleNext = () => {
    this.props.handleNext();
    this.scrollToTop();
  }
  
  scrollToTop = () => {
    window.scrollTo(0, 0);
  }

  render() {
    const {
      classes,
      selfUserId,
      isFetching,
      skip,
      limit,
      total,
      displayErrorDialog,
      sortBy,
      filters,
      getQuerySort,
      handleQueryChange,
      steps,
      activeStep,
      endWithSubmitButton,
      handleBack,
      handleNext,
      isSubmitting,
      formDirty,
      handleReset,
      handleSubmit,
      ...otherProps
    } = this.props;
    const resolveRightSideButtons = () => {
      if (endWithSubmitButton && activeStep === steps - 1) {
        return (
          <div>
            <Button
              type="reset"
              disabled={!formDirty || isSubmitting}
              onClick={handleReset}>
              Reset
            </Button>
            <Button
              type="submit"
              color="secondary"
              disabled={isSubmitting}
              onClick={handleSubmit}>
              Submit
              <KeyboardArrowRight />
            </Button>
          </div>
        );
      } else {
        return (
          <Button
            onClick={this.handleNext}
            disabled={activeStep < steps - 1 ? false : true}>
            Next
            <KeyboardArrowRight />
          </Button>
        );
      }
    }
    return (
      <MobileStepper
        className={classes.root}
        backButton={
          <Button
            onClick={this.handleBack}
            disabled={activeStep === 0}>
            <KeyboardArrowLeft />
            Back
          </Button>
        }
        nextButton={resolveRightSideButtons()}
        steps={steps}
        activeStep={activeStep}
        {...otherProps} />
    );
  }
}

Pagination.propTypes = {
  classes: PropTypes.object.isRequired,
  steps: PropTypes.number.isRequired,
  activeStep: PropTypes.number.isRequired,
  endWithSubmitButton: PropTypes.bool,
  handleBack: PropTypes.func,
  handleNext: PropTypes.func,
  isSubmitting: PropTypes.bool,
  formDirty: PropTypes.bool,
  handleReset: PropTypes.func,
  handleSubmit: PropTypes.func
};

export default withStyles(styles)(Pagination);
