import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  root: {

  },
  textField: {
    width: '20px'
  },
  menu: {
    minWidth: '100px'
  }
});

class AgeDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      anchorEl: null
    };
  }

  handleClick = (event) => {
    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = (event) => {
    this.setState({
      open: false,
      anchorEl: null
    });
    if (event.currentTarget.textContent !== '') {
      this.props.updateSelectedAge(event.currentTarget.textContent);
    }
  };

  buildMenuItems = () => {
    let menuItems = [];
    for (let age = 18; age < 40; age++) {
      menuItems.push(<MenuItem key={age} onClick={this.handleRequestClose}>{age}</MenuItem>);
    }
    return menuItems;
  };

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <TextField
          className={classes.textField}
          value={this.props.selectedAge}
          aria-owns="simple-menu"
          aria-haspopup="true"
          onClick={this.handleClick} />
        <Menu
          className={classes.menu}
          anchorEl={this.state.anchorEl}
          open={this.state.open}
          onClose={this.handleRequestClose}>
          {this.buildMenuItems}
        </Menu>
      </div>
    );
  }
}

AgeDropdown.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  selectedAge: PropTypes.string,
  updateSelectedAge: PropTypes.func
};

export default withStyles(styles)(AgeDropdown);
