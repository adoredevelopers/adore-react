import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { withFormik } from 'formik';
import * as Yup from 'yup';
import SwipeableViews from 'react-swipeable-views';

import { REGISTRATION_FORM_FIELDS } from '../../constants/field-groups/REGISTRATION_FORM_FIELDS';
import FormikField from '../formik-components/FormikField';
import FormikState from '../formik-components/FormikState';
import RegistrationPaginationContainer from '../../containers/RegistrationPaginationContainer';
import UserProfile from '../../models/UserProfile';
import { normalizeFormValues } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {

  },
  swipeView: {
    flex: '1 1 0',
    padding: '40px 20px',
    marginLeft: 'auto',
    marginRight: 'auto',
    maxWidth: '600px'
  },
  fieldRow: {
    display: 'block',
    marginBottom: '20px',
    width: '100%',
    '@media only screen and (min-width: 600px)': {
      display: 'flex',
      justifyContent: 'space-between'
    }
  },
  displayImagesFieldRow: {
    display: 'flex',
    width: '100%',
    marginBottom: '40px'
  },
  fieldRowItem: {
    marginBottom: '20px',
    '@media only screen and (min-width: 600px)': {
      flex: '1 1 0',
      marginRight: '10px',
      marginBottom: '0'
    }
  },
  fieldRowItemLast: {
    marginRight: '0'
  },
  changePhotosButton: {
    display: 'none',
    alignSelf: 'flex-end',
    '@media only screen and (min-width: 600px)': {
      display: 'block'
    },
  }
});

class FormikRegistrationForm extends Component {
  constructor(props) {
    super(props);
    this.TOP_BAR_HEIGHT = window.innerHeight < 600 ? 56 : 64;
    this.BOTTOM_STEPPER_HEIGHT = 54;
    this.swipeableViewRef = React.createRef();
    this.state = {
      changePhotosDialog: {
        open: false
      },
      showFormikState: false,
      windowInnerHeight: window.innerHeight
    };
  }

  componentDidMount = () => {
    this.updateViewPortDimensions();
    window.addEventListener('resize', this.updateViewPortDimensions);
  }
  
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateViewPortDimensions);
  }

  updateViewPortDimensions = () => {
    this.TOP_BAR_HEIGHT = window.innerHeight < 600 ? 56 : 64;
    this.setState({
      windowInnerHeight: window.innerHeight
    });
  }

  handleChangePhotosButtonClick = (event) => {
    this.setState({
      changePhotosDialog: {
        open: true
      }
    });
  }

  handleChangePhotoDialogRequestClose = (event) => {
    this.setState({
      changePhotosDialog: {
        open: false
      }
    });
  }

  handleSwipeViewBack = () => {
    this.props.handleSwipeViewBack();
    this.swipeViewScrollToTop();
  }

  handleSwipeViewNext = () => {
    this.props.handleSwipeViewNext();
    this.swipeViewScrollToTop();
  }

  handleSwipeViewTransitionEnd = () => {
    this.swipeViewScrollToTop();
  }

  swipeViewScrollToTop = () => {
    this.swipeableViewRef.current.rootNode
      .getElementsByClassName('react-swipeable-view-container')[0]
      .childNodes[this.props.swipeViewState.index].scrollTop = 0;
  }

  handleSubmit = (event) => {
    this.props.handleSubmit(event);
    if (!this.props.isValid) {
      this.props.changeSwipeView(0);
      this.props.showNotificationSnackbar();
    }
  }

  render() {
    const {
      classes,
      values,
      touched,
      dirty,
      errors,
      handleChange,
      handleBlur,
      setFieldValue,
      setFieldTouched,
      isSubmitting,
      handleSubmit,
      handleReset,

      swipeViewState,
    } = this.props;
    const swipeViewStyleHeight = this.state.windowInnerHeight - this.TOP_BAR_HEIGHT - this.BOTTOM_STEPPER_HEIGHT;
    const formikVars = {
      values,
      errors,
      touched
    };
    const handlers = {
      handleChangePhotosButtonClick: this.handleChangePhotosButtonClick,
      handleChangePhotoDialogRequestClose: this.handleChangePhotoDialogRequestClose,
      setFieldValue: this.props.setFieldValue,
      setFieldTouched: this.props.setFieldTouched,
    };
    return (
      <form className={classes.root} onSubmit={this.handleSubmit.bind(this)}>
        <SwipeableViews
          ref={this.swipeableViewRef}
          index={swipeViewState.index}
          onTransitionEnd={this.handleSwipeViewTransitionEnd}>
          {
            REGISTRATION_FORM_FIELDS.map((section, swipeViewIdx) => {
              return (
                <div className={classes.swipeView} style={{height: swipeViewStyleHeight}} key={'swipeViewContent_' + swipeViewIdx}>
                  {
                    section.sectionFields.map((sectionField, sectionFieldIdx) => {
                      return <FormikField
                        classes={classes}
                        sectionField={sectionField}
                        formikVars={formikVars}
                        handlers={handlers}
                        photoDialogOpen={this.state.changePhotosDialog.open}
                        key={'sectionField_' + sectionFieldIdx} />
                    })
                  }
                </div>
              );
            })
          }
        </SwipeableViews>

        <FormikState show={this.state.showFormikState} {...this.props} />

        <RegistrationPaginationContainer
          style={{ background: 'whitesmoke' }}
          type="dots"
          position="bottom"
          steps={REGISTRATION_FORM_FIELDS.length}
          activeStep={swipeViewState.index}
          endWithSubmitButton={true}
          handleBack={this.handleSwipeViewBack}
          handleNext={this.handleSwipeViewNext}
          isSubmitting={isSubmitting}
          formDirty={dirty}
          handleReset={handleReset} />
      </form>
    );
  }
};

FormikRegistrationForm.propTypes = {
  swipeViewState: PropTypes.object.isRequired,
  changeSwipeView: PropTypes.func.isRequired,
  handleSwipeViewBack: PropTypes.func.isRequired,
  handleSwipeViewNext: PropTypes.func.isRequired
};

export default withFormik({
  validationSchema: Yup.object().shape({
    // displayImages: Yup.array()
    //   .min("Please choose at least one display image.")
    //   .max("Cannot exceed maximum of 8 display images."),
    displayImages: Yup.string()
      .required('Please upload at least one profile image.'),
    email: Yup.string()
      .required('Email is required!')
      .email('Email format incorrect'),
    password: Yup.string()
      .required('Password is required!')
      .min(8, 'Password has to be minimum 8 characters.')
      .max(16, 'password cannot exceed 16 characters.'),
    passwordVerify: Yup.string()
      .required('Password verification is required!')
      .oneOf([Yup.ref('password')], 'Password does not match.'),
    // gender: Yup.array()
    //   .of(
    //     Yup.object().shape({
    //       label: Yup.string().required(),
    //       value: Yup.string().required()
    //     })
    //   )
    //   .required("Gender is required!")
    name: Yup.string()
    .required('Name is required!'),
    gender: Yup.string()
      .required("Gender is required!"),
    birthday: Yup.string()
      .required("Birthday is required!"),
    height: Yup.string()
      .required('Height is required!'),
    martialStatus: Yup.string()
      .required('Martial status is required!'),
    languages: Yup.string()
      .required('Language is required!'),
    postalCode: Yup.string()
      .required('Postal code is required!'),
    // country: Yup.string()
    //   .required('Country is required!'),
    // province: Yup.string()
    //   .required('Province is required!'),
    // city: Yup.string()
    //   .required('City is required!')
  }),
  mapPropsToValues: props => {
    const userProfile = new UserProfile();
    if (props.profileInfo != null) {
      userProfile.initFromApplicationState(props.profileInfo);
    }
    return userProfile.getRegistrationFormFields();
  },
  handleSubmit: (values, formikBag) => {
    const normalizedValues = normalizeFormValues(values);
    const userProfile = new UserProfile();
    userProfile.initFromRegistrationForm(normalizedValues);
    const payload = userProfile.getAllFields(true);
    payload.accountStatus = 'ACTIVE';
    console.debug(payload);
    formikBag.props.updateUserProfile(payload);
    setTimeout(() => {
      formikBag.setSubmitting(false);
    }, 1000);
  },
  displayName: "RegistrationForm"
})(withStyles(styles)(FormikRegistrationForm));
