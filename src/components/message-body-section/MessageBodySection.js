import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CircularProgress from '@material-ui/core/CircularProgress';
import Chip from '@material-ui/core/Chip';
import PersonIcon from '@material-ui/icons/Person';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import SyncIcon from '@material-ui/icons/Sync';
import green from '@material-ui/core/colors/green';
import ImageLoader from 'react-image-loader2';
import moment from 'moment';

import navigationLinks from '../../utils/navigationLinks';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';

const styles = theme => ({
  root: {
    flex: '1 1 0',
    overflowX: 'none',
    overflowY: 'auto'
  },
  messageRowWrapper: {

  },
  messageRow: {
    display: 'flex',
    margin: '20px 0'
  },
  moreMessagesButton: {
    margin: 'auto'
  },
  fabProgress: {
    color: green[500],
    position: "absolute",
    top: -6,
    left: -6,
    zIndex: 1
  },
  timestampRow: {
    padding: '10px 0',
    width: '100%',
    textAlign: 'center'
  },
  timestampChip: {
    height: '22px',
    fontSize: '11px',
    fontWeight: 'bold',
    color: 'white',
    background: 'darkgray',
    borderRadius: '4px'
  },
  messageAvatarButton: {
    minWidth: '46px',
    width: '46px',
    height: '46px',
    margin: '0 16px'
  },
  messageAvatarButtonMobile: {
    minWidth: '36px',
    width: '36px',
    height: '36px',
    margin: '0 16px'
  },
  messageAvatar: {
    width: '46px',
    height: '46px'
  },
  messageAvatarMobile: {
    width: '36px',
    height: '36px'
  },
  messageAvatarImage: {
    width: '46px',
    height: '46px'
  },
  messageAvatarImageMobile: {
    width: '36px',
    height: '36px'
  },
  messageText: {
    padding: '11px',
    lineHeight: '24px',
    whiteSpace: 'pre-line'
  },
  messageRowPadding: {
    flex: '1 1 0',
    minWidth: '84px'
  },
  messageRowPaddingMobile: {
    flex: '1 1 0',
    minWidth: '74px'
  }
});

class MessageBodySection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      moreMessagesLoading: false,
      moreMessagesSuccess: true
    };
    this.scrollToMessageBottomTimer = null;
  }

  componentWillUnmount = () => {
    clearTimeout(this.scrollToMessageBottomTimer);
  }

  setMessageBottomPlaceholderDOM = (dom) => {
    this.messageBottomPlaceholderDOM = dom;
    if (this.props.shouldScrollToMessageBottom) {
      this.scrollToBottom(this.props.shouldScrollToMessageBottom);
    }
  }

  scrollToBottom = () => {
    const self = this;
    if (this.props.shouldScrollToMessageBottom && this.messageBottomPlaceholderDOM) {
      this.scrollToMessageBottomTimer = setTimeout(function() {
        self.messageBottomPlaceholderDOM.scrollIntoView();
      }, 200);
    }
  }

  handleMoreMessagesButtonClick = (event) => {
    const self = this;
    this.props.setShouldScrollToMessageBottom(false);
    if (!this.state.moreMessagesLoading) {
      this.setState(
        {
          moreMessagesLoading: true,
          moreMessagesSuccess: false
        },
        () => {
          // todo: fetch more messages
          const conversationId = self.props.currentConversationId;
          const beforeCreatedAt = self.props.conversations[conversationId].messages[0].createdAt;
          self.props.fetchMoreMessages(conversationId, beforeCreatedAt).then(function() {
            self.setState({
              moreMessagesLoading: false,
              moreMessagesSuccess: true
            });
          });
        }
      );
    }
  }

  render() {
    this.scrollToBottom();
    const {
      classes,
      selfUserId,
      conversations,
      currentConversationId
    } = this.props;
    const conversationId = currentConversationId == null ? conversations[Object.keys(conversations)[0]] : currentConversationId;
    const conversation = conversations[conversationId];
    const conversationUser = conversation.messageUser;
    const getTimestampRow = (idx) => {
      let durationInMinutes = 0;
      let timestampDisplay = '';
      if (idx > 0) {
        const prevMessage = conversation.messages[idx - 1];
        const currentMessage = conversation.messages[idx];
        const duration = moment.duration(moment(currentMessage.createdAt).diff(moment(prevMessage.createdAt)));
        durationInMinutes = duration.asMinutes();
      }
      if (durationInMinutes > 5 || idx === 0) {
        const now = moment();
        const year = now.year();
        const month = now.month();
        const dayOfMonth = now.date();
        const messageDate = moment(conversation.messages[idx].createdAt);
        const messageYear = messageDate.year();
        const messageMonth = messageDate.month();
        const messageDayOfMonth = messageDate.date();
        const messageDayOfWeek = messageDate.day();
        let displayFullDate = true;
        if (messageYear < year) {
          displayFullDate = true;
        } else if (messageMonth < month) {
          displayFullDate = true;
        } else if (messageDayOfMonth + 6 < dayOfMonth) {
          displayFullDate = true;
        } else {
          displayFullDate = false;
        }
        if (displayFullDate) {
          timestampDisplay += messageDate.format('MMM D, YYYY HH:mm');
        } else {
          const diffInDay = dayOfMonth - messageDayOfMonth;
          if (diffInDay === 0) {
            timestampDisplay += '';
          } else if (diffInDay === 1) {
            timestampDisplay += 'Yesterday ';
          } else if (diffInDay > 1) {
            switch (messageDayOfWeek) {
              case 0:
                timestampDisplay += 'Sunday ';
                break;
              case 1:
                timestampDisplay += 'Monday ';
                break;
              case 2:
                timestampDisplay += 'Tuesday ';
                break;
              case 3:
                timestampDisplay += 'Wednesday ';
                break;
              case 4:
                timestampDisplay += 'Thursday ';
                break;
              case 5:
                timestampDisplay += 'Friday ';
                break;
              case 6:
                timestampDisplay += 'Saturday ';
                break;
              default:
                timestampDisplay += '';
            }
          } else {
            // should not reach here, assertion error
            console.error('assert: diffInDay is negative.');
          }
          timestampDisplay += messageDate.format('HH:mm');
        }
        return (
          <div className={classes.timestampRow}>
            <Chip label={timestampDisplay} className={classes.timestampChip}></Chip>
          </div>
        );
      } else {
        return null;
      }
    };
    return (
      <div className={classes.root}>
        <div className={classes.messageRow}>
          <Fab
            size="small"
            color="secondary"
            className={classes.moreMessagesButton}
            disabled={this.state.moreMessagesLoading ? true : false}
            onClick={this.handleMoreMessagesButtonClick}>
            {this.state.moreMessagesSuccess ? <KeyboardArrowUpIcon /> : <SyncIcon />}
          </Fab>
          {this.state.moreMessagesLoading && <CircularProgress size={52} className={classes.fabProgress} />}
        </div>
        {
          conversation.messages.map((message, idx) => {
            if (selfUserId === message.recipientId) {
              return (
                <div className={classes.messageRowWrapper} key={'message_' + idx}>
                  {getTimestampRow(idx)}
                  <div className={classes.messageRow}>
                    <Hidden xsDown>
                      <Fab className={classes.messageAvatarButton}>
                        <Link to={navigationLinks('USER_DETAILS_PAGE_ROUTE', conversationUser._id)}>
                          <Avatar className={classes.messageAvatar}>
                            <ImageLoader className={classes.messageAvatarImage} src={conversationUser.displayImages[0][appConfig.imageStorage][constants.imageSizes.SMALL]} alt="Avatar" />
                          </Avatar>
                        </Link>
                      </Fab>
                    </Hidden>
                    <Hidden smUp>
                      <Fab className={classes.messageAvatarButtonMobile}>
                        <Link to={navigationLinks('USER_DETAILS_PAGE_ROUTE', conversationUser._id)}>
                          <Avatar className={classes.messageAvatarMobile}>
                            <ImageLoader className={classes.messageAvatarImageMobile} src={conversationUser.displayImages[0][appConfig.imageStorage][constants.imageSizes.SMALL]} alt="Avatar" />
                          </Avatar>
                        </Link>
                      </Fab>
                    </Hidden>
                    <div className={classNames('message-bubble-left')} style={{marginRight: '20px'}}>
                      <Typography component="span" className={classes.messageText}>{message.message}</Typography>
                    </div>
                  </div>
                </div>
              );
            } else {
              return (
                <div className={classes.messageRowWrapper} key={'message_' + idx}>
                  {getTimestampRow(idx)}
                  <div className={classes.messageRow}>
                    <Hidden xsDown>
                      <div className={classes.messageRowPadding}></div>
                    </Hidden>
                    <Hidden smUp>
                      <div className={classes.messageRowPaddingMobile}></div>
                    </Hidden>
                    <div className={classNames('message-bubble-right')} style={{marginRight: '16px'}}>
                      <Typography component="span" className={classes.messageText}>{message.message}</Typography>
                    </div>
                    <Fab className={classes.messageAvatarButton} style={{display: 'none'}}>
                      <Avatar className={classes.messageAvatar}><PersonIcon /></Avatar>
                    </Fab>
                  </div>
                </div>
              );
            }
          })
        }
        <div
          className={classes.messageRow}
          key={'message_bottom_placeholder'}
          ref={this.setMessageBottomPlaceholderDOM} />
      </div>
    );
  }
}

MessageBodySection.propTypes = {
  classes: PropTypes.object.isRequired,
  selfUserId: PropTypes.string.isRequired,
  conversations: PropTypes.object.isRequired,
  currentConversationId: PropTypes.string,
  shouldScrollToMessageBottom: PropTypes.bool.isRequired,
  setShouldScrollToMessageBottom: PropTypes.func.isRequired,
  fetchMoreMessages: PropTypes.func.isRequired
}

export default withStyles(styles)(MessageBodySection);
