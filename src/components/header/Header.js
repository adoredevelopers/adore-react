import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
import DrawerToggleIcon from '@material-ui/icons/Sort';
import PeopleIcon from '@material-ui/icons/People';

import navigationLinks from '../../utils/navigationLinks';
import LeftDrawerContainer from '../../containers/LeftDrawerContainer';
import AppBarButtonGroupContainer from '../../containers/AppBarButtonGroupContainer';
import LogoImage from '../../resources/images/adore_logo@2x.png';

import './header.css';
const styles = theme => ({
  root: {

  },
  rootMobileHidden: {
    display: 'none',
    '@media only screen and (min-width: 600px)': {
      display: 'block'
    }
  },
  appBar: {
    position: 'relative',
    backgroundColor: 'whitesmoke'
  },
  appBarPositionFixed: {
    backgroundColor: 'whitesmoke'
  },
  drawerIcon: {
    width: '34px',
    height: '34px',
    fill: 'rgba(0, 0, 0, 0.87);'
  },
  logoImageLink: {
    height: '56px',
    '@media only screen and (min-width: 600px)': {
      height: '64px'
    }
  },
  logoImage: {
    marginRight: '10px',
    height: '56px',
    '@media only screen and (min-width: 600px)': {
      height: '64px'
    }
  },
  menuButton: {
    height: '64px',
    lineHeight: '100%',
    textTransform: 'capitalize'
  },
  linkMenuButton: {
    minWidth: '48px'
  },
  userListMenuButton: {
    marginLeft: '20px'
  },
  menuButtonText: {
    marginLeft: '10px'
  }
});

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leftDrawer: {
        open: false
      }
    };
  }

  handleOpenLeftDrawer = (event) => {
    this.setState({
      leftDrawer: {
        open: !this.state.leftDrawer.open
      }
    });
  };

  handleCloseLeftDrawer = (event) => {
    this.setState({
      leftDrawer: {
        open: false
      }
    });
  };

  render() {
    const classes = this.props.classes;
    const drawerButton = (
      <IconButton onClick={this.handleOpenLeftDrawer}>
        <DrawerToggleIcon className={classes.drawerIcon} />
      </IconButton>
    );
    const leftDrawer = (
      <LeftDrawerContainer
        drawerOpen={this.state.leftDrawer.open}
        closeDrawer={this.handleCloseLeftDrawer}
        userInfo={this.props.userInfo} />
    );
    return (
      <div style={Object.assign({}, this.props.style)} className={this.props.mobileHidden ? classNames(classes.root, classes.rootMobileHidden) : classes.root}>
        <AppBar className={this.props.appBarPositionFixed ? classes.appBarPositionFixed : classes.appBar}>
          <Toolbar>
            <Hidden smUp>
              {this.props.hasLeftDrawer ? drawerButton : null}
            </Hidden>
            <Link to={navigationLinks('HOME_ROUTE')} className={classes.logoImageLink}>
              <img className={classes.logoImage} src={LogoImage} alt="Adore" />
            </Link>
            <Hidden xsDown>
              <Link to={navigationLinks('USER_LIST_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
                <Button
                  className={classNames(classes.menuButton, classes.linkMenuButton, classes.userListMenuButton, 'header_menuButton')}>
                  <PeopleIcon />
                  <span className={classes.menuButtonText}>Users</span>
                </Button>
              </Link>
            </Hidden>
            <Hidden xsDown>
              <AppBarButtonGroupContainer userInfo={this.props.userInfo} />
            </Hidden>
          </Toolbar>
        </AppBar>
        <Hidden smUp>
          {this.props.hasLeftDrawer ? leftDrawer : ''}
        </Hidden>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  mobileHidden: PropTypes.bool,
  appBarPositionFixed: PropTypes.bool.isRequired,
  hasLeftDrawer: PropTypes.bool.isRequired
};

export default withStyles(styles)(Header);
