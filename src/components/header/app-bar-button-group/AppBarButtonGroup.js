import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuList from '@material-ui/core/MenuList';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import DropdownArrowIcon from '@material-ui/icons/ArrowDropDown';
import ProfileIcon from '@material-ui/icons/AccountBox';
import FavouiteIcon from '@material-ui/icons/Favorite';
// import MessagesIcon from '@material-ui/icons/Forum';
import MessagesIcon from '@material-ui/icons/Chat';
// import LogoutIcon from '@material-ui/icons/Delete';
import LogoutIcon from '@material-ui/icons/Report';
import NotificationsIcon from '@material-ui/icons/Notifications';
// import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import SettingsIcon from '@material-ui/icons/Settings';

import navigationLinks from '../../../utils/navigationLinks';
import appConfig from '../../../config/appConfig';
import constants from '../../../constants/constants';
// import DefaultProfileImage from '../../../resources/images/default_profile_image.jpg';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-end',
    width: '100%',
    height: '100%'
  },
  profileDropdown: {

  },
  menuButton: {
    height: '64px',
    lineHeight: '100%',
    textTransform: 'capitalize'
  },
  avatar: {
    marginRight: '6px',
    width: '36px',
    height: '36px',
    border: 'solid 2px white'
  },
  listItemIcon: {
    marginRight: '0'
  },
  linkMenuButton: {
    minWidth: '48px'
  }
});

class AppBarButtonGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileDropdown: {
        open: false,
        anchorEl: null
      }
    };
  }

  handleProfileDropdownClick = (event) => {
    this.setState({
      profileDropdown: {
        open: true,
        anchorEl: event.currentTarget
      }
    });
  }

  handleProfileDropdownRequestClose = (event) => {
    this.setState({
      profileDropdown: {
        open: false,
        anchorEl: null
      }
    });
  }

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <div className={classes.profileDropdown}>
          <Button
            className={classNames(classes.menuButton, 'header_menuButton')}
            aria-owns="simple-menu"
            aria-haspopup="true"
            onClick={this.handleProfileDropdownClick}>
            <Avatar className={classes.avatar} src={this.props.userInfo.displayImage[appConfig.imageStorage][constants.imageSizes.SMALL]} />
            {this.props.userInfo.name}
            <DropdownArrowIcon />
          </Button>
          <Menu
            anchorEl={this.state.profileDropdown.anchorEl}
            open={this.state.profileDropdown.open}
            onClose={this.handleProfileDropdownRequestClose}>
            <MenuList style={{outline: 0}}>
              <Link to={navigationLinks('EDIT_PROFILE_PAGE_ROUTE')} style={{textDecoration: 'none', outline: '0'}}>
                <ListItem button onClick={this.handleProfileDropdownRequestClose}>
                  <ListItemIcon className={classes.listItemIcon}>
                    <ProfileIcon />
                  </ListItemIcon>
                  <ListItemText primary="Profile" />
                </ListItem>
              </Link>
              <Link to={navigationLinks('USER_ADORES_PAGE_ROUTE')} style={{textDecoration: 'none', outline: '0'}}>
                <ListItem button onClick={this.handleProfileDropdownRequestClose}>
                  <ListItemIcon className={classes.listItemIcon}>
                    <FavouiteIcon />
                  </ListItemIcon>
                  <ListItemText primary="Adores" />
                </ListItem>
              </Link>
              <Link to={navigationLinks('USER_MESSAGES_PAGE_ROUTE')} style={{textDecoration: 'none', outline: '0'}}>
                <ListItem button onClick={this.handleProfileDropdownRequestClose}>
                  <ListItemIcon className={classes.listItemIcon}>
                    <MessagesIcon />
                  </ListItemIcon>
                  <ListItemText primary="Messages" />
                </ListItem>
              </Link>
              <ListItem button onClick={this.props.performLogOut}>
                <ListItemIcon className={classes.listItemIcon}><LogoutIcon /></ListItemIcon>
                <ListItemText primary="Log Out" />
              </ListItem>
            </MenuList>
          </Menu>
        </div>
        <Link to={navigationLinks('NOTIFICATIONS_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <Button
            className={classNames(classes.menuButton, classes.linkMenuButton, 'header_menuButton')}>
            <NotificationsIcon />
          </Button>
        </Link>
        <Link to={navigationLinks('SETTINGS_PAGE_ROUTE')} style={{textDecoration: 'none'}}>
          <Button
            className={classNames(classes.menuButton, classes.linkMenuButton, 'header_menuButton')}>
            <SettingsIcon />
          </Button>
        </Link>
      </div>
    );
  }
}

AppBarButtonGroup.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  performLogOut: PropTypes.func.isRequired
};

export default withStyles(styles)(AppBarButtonGroup);
