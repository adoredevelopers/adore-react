import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import InfoRow from '../info-row/InfoRow';

const styles = theme => ({
  root: {

  }
});

class IntroductionsSection extends Component {
  constructor(props) {
    super(props);
    this.fieldDOMs = {};
  }

  handleEditClick = (fieldId) => {
    this.props.handleSectionFieldClick(fieldId, this.fieldDOMs[fieldId]);
  }

  handleChange = (fieldId, fieldValue) => {
    this.props.handleSectionFieldChange(fieldId, fieldValue);
  }

  handleCancel = (fieldId) => {
    this.props.resetSectionField(fieldId);
  }

  handleSubmit = async (fieldId) => {
    this.props.submitServiceRequest(fieldId);
  }

  render() {
    const {
      classes,
      handleSectionFieldClick,
      handleSectionFieldChange,
      resetSectionField,
      submitServiceRequest,
      ...otherProps
    } = this.props;
    const infoRows = Object.keys(this.props.introductionsFields).map((fieldId, idx) => {
      const infoField = this.props.introductionsFields[fieldId];
      return (
        <InfoRow
          inputRef={(elem) => {
            this.fieldDOMs[fieldId] = elem;
          }}
          key={'infoRow_' + idx}
          isEdit={this.props.isEdit}
          infoField={infoField}
          fieldRows={6}
          onEditClick={this.handleEditClick}
          onChange={this.handleChange}
          onCancel={this.handleCancel}
          onSubmit={this.handleSubmit} />
      );
    });
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        {infoRows}
      </div>
    );
  }
}

IntroductionsSection.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  isEdit: PropTypes.bool,
  handleSectionFieldClick: PropTypes.func,
  handleSectionFieldChange: PropTypes.func,
  resetSectionField: PropTypes.func,
  submitServiceRequest: PropTypes.func
};

export default withStyles(styles)(IntroductionsSection);
