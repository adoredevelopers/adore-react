import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';

// import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import Select from 'react-virtualized-select';
import createFilterOptions from 'react-select-fast-filter-options';
import AGES from '../../../constants/field-data/ages';
import COUNTRY_REGIONS from '../../../constants/field-data/countryRegions';
import { buildFiltersQuery } from '../../../utils/helpers/commonHelpers';

const styles = theme => ({
  filters: {

  },
  fieldRow: {
    display: 'block',
    marginBottom: '20px',
    width: '100%',
    '@media only screen and (min-width: 600px)': {
      display: 'flex',
      justifyContent: 'space-between'
    }
  },
  fieldRowItem: {
    marginBottom: '20px',
    '@media only screen and (min-width: 600px)': {
      flex: '1 1 0',
      marginRight: '10px',
      marginBottom: '0'
    }
  },
  formControl: {
    width: '100%'
  },
  radioGroup: {
    flexDirection: 'row',
    zIndex: 0
  },
  select: {
    marginTop: '10px',
    minWidth: '200px'
  }
});

class FiltersDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: this.props.filters,
      countries: COUNTRY_REGIONS.map((countryRegion, idx) => {
        return {
          label: countryRegion.countryName,
          value: countryRegion.countryName
        };
      }),
      regionSelectDisabled: this.props.filters.country != null ? false : true,
      regions: this.props.filters.country != null ? this.getRegionsFromSelectedCountry(this.props.filters.country) : []
    };
  }

  getRegionsFromSelectedCountry = (selectedCountry) => {
    let regions = [];
    for (let i = 0; i < COUNTRY_REGIONS.length; i++) {
      if (COUNTRY_REGIONS[i].countryName === selectedCountry) {
        regions = COUNTRY_REGIONS[i].regions;
        break;
      }
    }
    return regions.map((region, idx) => {
      return {
        label: region.name,
        value: region.shortCode
      };
    });
  }
  
  handleGenderRadioChange = (event, value) => {
    const filters = {
      ...this.state.filters,
      gender: value
    };
    this.setState({
      filters
    });
  }

  handleMinAgeSelectionChange = (option) => {
    const filters = {
      ...this.state.filters,
      minAge: option.value
    };
    this.setState({
      filters
    });
  }

  handleMaxAgeSelectionChange = (option) => {
    const filters = {
      ...this.state.filters,
      maxAge: option.value
    };
    this.setState({
      filters
    });
  }

  handlePhotoVerifiedCheckboxChange = (event) => {
    const checked = event.currentTarget.checked;
    const filters = {
      ...this.state.filters,
      photoVerified: checked
    };
    this.setState({
      filters
    });
  }

  handleCountrySelectionChange = (option) => {
    const filters = {
      ...this.state.filters,
      country: option != null ? option.value : null,
      province: null
    };
    let newState = this.state;
    if (option != null) {
      newState = {
        filters,
        regionSelectDisabled: false,
        regions: this.getRegionsFromSelectedCountry(option.value)
      };
    } else {
      newState = {
        filters,
        regionSelectDisabled: true,
        regions: []
      };
    }
    this.setState(newState);
  }

  handleRegionSelectChange = (option) => {
    const filters = {
      ...this.state.filters,
      region: option != null ? option.value : null
    };
    this.setState({
      filters
    });
  }

  handleSubmit = () => {
    this.props.setUsersFilters(this.state.filters);
    const query = {
      _id: {
        $ne: this.props.selfUser._id
      },
      ...buildFiltersQuery(this.state.filters),
      $sort: this.props.getQuerySort(this.props.sortBy)
    };
    this.props.handleQueryChange(query);
    this.props.onClose();
  }

  render() {
    const {
      classes,
      selfUser,
      sortBy,
      filters,
      getQuerySort,
      handleQueryChange,
      setUsersFilters,
      filterUserList,
      ...otherProps
    } = this.props;
    return (
      <Dialog
        {...otherProps}>
        <DialogTitle>Apply Filters</DialogTitle>
        <DialogContent>
          <div className={classes.filters}>
            {/* Gender */}
            <div className={classes.fieldRow}>
              <FormControl required={false} className={classes.formControl}>
                <FormLabel>Gender</FormLabel>
                <RadioGroup
                  aria-label="Gender"
                  className={classes.radioGroup}
                  value={this.state.filters.gender}
                  onChange={this.handleGenderRadioChange}>
                  <FormControlLabel label="Male" value="Male" control={<Radio color="primary" />} />
                  <FormControlLabel label="Female" value="Female" control={<Radio color="primary" />} />
                </RadioGroup>
              </FormControl>
            </div>

            {/* Age Range */}
            <div className={classes.fieldRow}>
              <div className={classes.fieldRowItem}>
                <FormControl required={false} className={classes.formControl}>
                  <FormLabel>Min Age</FormLabel>
                  <Select
                    filterOptions={createFilterOptions({options: AGES})}
                    options={AGES}
                    clearable={false}
                    value={this.state.filters.minAge}
                    className={classes.select}
                    style={{borderRadius: '0'}}
                    onChange={this.handleMinAgeSelectionChange} />
                </FormControl>
              </div>
              <div className={classes.fieldRowItem} style={{marginRight: '0'}}>
                <FormControl required={false} className={classes.formControl}>
                  <FormLabel>Max Age</FormLabel>
                  <Select
                    filterOptions={createFilterOptions({options: AGES})}
                    options={AGES}
                    clearable={false}
                    value={this.state.filters.maxAge}
                    className={classes.select}
                    style={{borderRadius: '0'}}
                    onChange={this.handleMaxAgeSelectionChange} />
                </FormControl>
              </div>
            </div>

            {/* Has Photo */}
            <div className={classes.fieldRow}>
              <FormControl required={false} className={classes.formControl}>
                <FormLabel>Has photo?</FormLabel>
                <div className={classes.checkbox}>
                  <FormControlLabel
                    label="Yes"
                    control={
                      <Checkbox
                        color="primary"
                        classes={{
                          checked: classes.checked,
                        }}
                        value={this.state.filters.photoVerified ? 'Yes' : 'No'}
                        checked={this.state.filters.photoVerified}
                        onChange={this.handlePhotoVerifiedCheckboxChange} />
                    } />
                </div>
              </FormControl>
            </div>

            <div className={classes.divider} />

            {/* Location */}
            <div className={classes.fieldRow}>
              <div className={classes.fieldRowItem}>
                <FormControl required={false} className={classes.formControl}>
                  <FormLabel>Country/Region</FormLabel>
                  <Select
                    filterOptions={createFilterOptions({options: this.state.countries})}
                    options={this.state.countries}
                    value={this.state.filters.country}
                    className={classes.select}
                    style={{borderRadius: '0'}}
                    onChange={this.handleCountrySelectionChange} />
                </FormControl>
              </div>
              <div className={classes.fieldRowItem}>
                <FormControl required={false} className={classes.formControl}>
                  <FormLabel>State/Province</FormLabel>
                  <Select
                    filterOptions={createFilterOptions({options: this.state.regions})}
                    options={this.state.regions}
                    value={this.state.filters.region}
                    disabled={this.state.regionSelectDisabled}
                    className={classes.select}
                    style={{borderRadius: '0'}}
                    onChange={this.handleRegionSelectChange} />
                </FormControl>
              </div>
            </div>

            <div className={classes.divider} />

            {/* Advanced Filters */}
            <div className={classes.fieldRowItem} style={{marginRight: '0', display: 'none'}}>
              <FormControl required={false} className={classes.formControl}>
                <FormLabel>Advanced Filters</FormLabel>
              </FormControl>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onClose}>Cancel</Button>
          <Button color="primary" onClick={this.handleSubmit}>Filter</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

FiltersDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  selfUser: PropTypes.object.isRequired,
  filters: PropTypes.object.isRequired,
  sortBy: PropTypes.number.isRequired,
  maxWidth: PropTypes.string.isRequired,
  fullScreen: PropTypes.bool.isRequired,
  setUsersFilters: PropTypes.func.isRequired,
  getQuerySort: PropTypes.func.isRequired,
  handleQueryChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(FiltersDialog);
