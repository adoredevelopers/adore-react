import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import DropdownArrowIcon from '@material-ui/icons/ArrowDropDown';
import FilterIcon from '@material-ui/icons/FilterList';

import FiltersDialogContainer from '../../containers/FiltersDialogContainer';
import { buildFiltersQuery } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    display: 'flex',
    margin: '10px 0',
    padding: '8px',
    flexDirection: 'row',
    justifyContent: 'space-between',
    background: '#fafafa',
    '@media only screen and (min-width: 600px)': {
      margin: '0 0 20px 0'
    }
  },
  sortByLabel: {
    marginLeft: '12px',
    fontSize: '14px',
    textTransform: 'uppercase'
  },
  sortDropdown: {

  },
  filterButton: {

  }
});

class FilterSortToolbar extends Component {
  constructor(props) {
    super(props);
    this.sortByOptions = [
      'Active',
      'Nearby',
      'Popularity',
      'New User'
    ];
    this.state = {
      sortDropdown: {
        open: false,
        anchorEl: null,
        sortBy: 0
      },
      filtersDialog: {
        open: false
      }
    };
  }

  handleSortDropdownClick = (event) => {
    let sortDropdown = this.state.sortDropdown;
    sortDropdown.open = true;
    sortDropdown.anchorEl = event.currentTarget
    this.setState({
      sortDropdown
    });
  };

  handleSortDropdownOptionSelect = (event) => {
    const sortBy = parseInt(event.currentTarget.dataset['index'], 10);
    let sortDropdown = this.state.sortDropdown;
    sortDropdown.sortBy = sortBy;
    this.setState({
      sortDropdown
    });
    this.handleSortChange(sortBy);
    this.handleSortDropdownRequestClose(event);
  };

  handleSortChange = (sortBy) => {
    this.props.setUsersSortBy(sortBy);
    const query = {
      _id: {
        $ne: this.props.selfUserId
      },
      ...buildFiltersQuery(this.props.filters),
      $sort: this.props.getQuerySort(sortBy)
    };
    this.props.handleQueryChange(query);
  }

  handleSortDropdownRequestClose = (event) => {
    let sortDropdown = this.state.sortDropdown;
    sortDropdown.open = false;
    this.setState({
      sortDropdown
    });
  };

  handleFilterButtonClick = (event) => {

    this.setState({
      filtersDialog: {
        open: true
      }
    });
  };

  handleRequestClose = (event) => {
    this.setState({
      filtersDialog: {
        open: false
      }
    });
  };

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <div className={classes.sortDropdown}>
          <span className={classes.sortByLabel}>SORT BY: </span>
          <Button
            aria-owns="simple-menu"
            aria-haspopup="true"
            onClick={this.handleSortDropdownClick}>
            {this.sortByOptions[this.state.sortDropdown.sortBy]}
            <DropdownArrowIcon />
          </Button>
          <Menu
            anchorEl={this.state.sortDropdown.anchorEl}
            open={this.state.sortDropdown.open}
            onClose={this.handleSortDropdownRequestClose}>
            {
              this.sortByOptions.map((option, idx) => {
                return (
                  <MenuItem
                    selected={idx === this.state.sortDropdown.sortBy}
                    data-index={idx}
                    onClick={this.handleSortDropdownOptionSelect}
                    key={'menuItem_' + idx}>
                    {option}
                  </MenuItem>
                );
              })
            }
          </Menu>
        </div>
        <div className={classes.filterButton} onClick={this.handleFilterButtonClick}>
          <Button><FilterIcon /> Filter</Button>
        </div>
        <FiltersDialogContainer
          open={this.state.filtersDialog.open}
          maxWidth="sm"
          fullScreen={window.innerWidth < 600 ? true : false}
          getQuerySort={this.props.getQuerySort}
          handleQueryChange={this.props.handleQueryChange}
          onClose={this.handleRequestClose} />
      </div>
    );
  }
}

FilterSortToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  getQuerySort: PropTypes.func.isRequired,
  handleQueryChange: PropTypes.func.isRequired
};

export default withStyles(styles)(FilterSortToolbar);
