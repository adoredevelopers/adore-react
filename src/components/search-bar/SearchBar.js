import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';

const styles = {
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: "100%",
  },
  input: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
};

function SearchBar(props) {
  const { classes, performSearch } = props;

  return (
    <Paper className={classes.root} elevation={1}>
      {/* menu button
      <IconButton className={classes.iconButton} aria-label="Menu">
        <MenuIcon />
      </IconButton>
      */}
      <InputBase className={classes.input} placeholder="Type to filter names" onChange={performSearch} />
      <IconButton className={classes.iconButton} aria-label="Search" onClick={performSearch}>
        <SearchIcon />
      </IconButton>
      {/* accessory button
      <Divider className={classes.divider} />
      <IconButton color="primary" className={classes.iconButton} aria-label="Directions">
        <DirectionsIcon />
      </IconButton>
      */}
    </Paper>
  );
}

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,
  performSearch: PropTypes.func
};

export default withStyles(styles)(SearchBar);