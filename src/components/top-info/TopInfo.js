import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';

import BasicInfoEditDialog from '../basic-info-list/basic-info-edit-dialog/BasicInfoEditDialog';
import { getAgeFromBirthDate } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
  },
  nameAge: {
    fontSize: '22px'
  },
  residence: {
    fontSize: '14px'
  }
});

class TopInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editDialog: {
        open: false
      }
    };
    this.styleOverrides = this.props.styleOverrides || {};
  }
  
  handleRequestClose = () => {
    this.setState({
      editDialog: {
        open: false
      }
    });
  }

  render() {
    const classes = this.props.classes;
    const userInfo = this.props.userInfo;
    return (
      <div style={Object.assign({})} className={classes.root}>
        <Typography
          style={Object.assign({}, this.styleOverrides.nameAge)}
          variant="h5"
          component="span"
          className={classes.nameAge}
          onClick={this.props.isEdit ? (event) => {
            this.setState({
              editDialog: {
                open: true
              }
            });
          } : null}>
          {userInfo.profile.basic.name}, {getAgeFromBirthDate(userInfo.profile.basic.birthday)}{this.props.isEdit ? <EditIcon style={{marginLeft: '5px', width: '18px', height: '18px'}} /> : ''}
        </Typography>
        <Typography
          style={Object.assign({}, this.styleOverrides.residence)}
          variant="caption"
          component="span"
          className={classes.residence}>
          {userInfo.profile.basic.city}, {userInfo.profile.basic.province}, {userInfo.profile.basic.country}
        </Typography>
        <BasicInfoEditDialog
          open={this.state.editDialog.open}
          maxWidth="sm"
          onClose={this.handleRequestClose}
          showNotificationSnackbar={this.props.showNotificationSnackbar} />
      </div>
    );
  }
}

TopInfo.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  styleOverrides: PropTypes.object,
  userInfo: PropTypes.object.isRequired,
  isEdit: PropTypes.bool
};

export default withStyles(styles)(TopInfo);
