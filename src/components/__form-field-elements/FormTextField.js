import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import moment from 'moment';

import withFieldValidations from '../../containers/functional-hocs/withFieldValidations';
import { getDataFieldValueFromDataObjectWithFieldPathAndFieldId } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    width: '100%'
  },
  textField: {
    
  }
});

class FormTextField extends Component {
  componentDidMount() {
    if (this.props.fieldProps.isRequired) {
      let validationResult = this.props.validate();
      validationResult.suppressFormErrorDisplay = true;
      this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    }
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   hasError: false,
    //   errorMessage: null
    // };
    this.previousBlurValue = null;
  }
  
  resolveTextFieldType = (textFieldType) => {
    switch (textFieldType) {
      case 'textField':
        return 'text';
      case 'emailTextField':
        return 'email';
      case 'passwordTextField':
        return 'password';
      case 'numberTextField':
        return 'number';
      case 'dateTextField':
        return 'date';
      case 'colorTextField':
        return 'color';
      case 'hiddenTextField':
        return 'hidden';
      default:
        return '';
    }
  }

  resolveValue = () => {
    const value = getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this.props.dataObject, this.props.fieldProps);
    if (value && this.props.fieldProps.fieldSubType === 'dateTextField') {
      const date = new Date(value);
      let month = date.getUTCMonth() + 1;
      month = month < 10 ? '0' + month : month;
      let dayOfMonth = date.getUTCDate();
      dayOfMonth = dayOfMonth < 10 ? '0' + dayOfMonth : dayOfMonth;
      return date.getUTCFullYear() + '-' + month + '-' + dayOfMonth;
    }
    return value;
  }

  handleChange = (event) => {
    const fieldProps = this.props.fieldProps;
    const value = event.currentTarget.value;
    let validationResult = this.props.validate(value);
    validationResult.suppressFormErrorDisplay = false;
    // this.setState(validationResult);
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    this.props.handleFormFieldChange(fieldProps, value);
  }

  // handle blur action if the fieldProps defines the blur method
  handleBlur = (event) => {
    const fieldDefHandleBlur = this.props.fieldProps.fieldDefHandleBlur;
    if (fieldDefHandleBlur != null) {
      const value = event.target.value;
      if (!this.props.validationResult.hasError && value !== this.previousBlurValue) {
        fieldDefHandleBlur(value, this.props.updateFormFieldValidity, this.props.handleFormFieldChange);
      }
      this.previousBlurValue = value;
    }
  }

  resetValue = () => {
    // this.setState({
    //   hasError: false,
    //   errorMessage: null,
    //   value: this.resolveValue()
    // });
    const validationResult = {
      hasError: false,
      errorMessage: null,
      suppressFormErrorDisplay: false
    };
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
  }

  render() {
    const {
      classes,
      fieldProps,
      dataObject,
      dependencyFields,
      validate,
      validationResult,
      fieldDefHandleBlur,
      updateFormFieldValidity,
      handleFormFieldChange,
      ...otherProps
    } = this.props;
    const value = this.resolveValue();
    const getTextField = () => {
      if (fieldProps.fieldId === 'birthday' && this.resolveTextFieldType(fieldProps.fieldSubType) === 'date') {
        return (
          <TextField
            id={fieldProps.fieldId}
            type={this.resolveTextFieldType(fieldProps.fieldSubType)}
            value={value != null ? value : ''}
            className={classes.textField}
            disabled={fieldProps.isDisabled}
            error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}
            helperText={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.errorMessage : null}
            onChange={this.handleChange}
            InputProps={{
              onBlur: this.handleBlur
            }}
            inputProps={{
              max: moment().subtract(18, 'years').format('YYYY-MM-DD'),
              min: moment().subtract(100, 'years').format('YYYY-MM-DD')
            }}
            {...otherProps} />
        );
      } else {
        return (
          <TextField
            id={fieldProps.fieldId}
            type={this.resolveTextFieldType(fieldProps.fieldSubType)}
            value={value != null ? value : ''}
            className={classes.textField}
            disabled={fieldProps.isDisabled}
            error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}
            helperText={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.errorMessage : null}
            onChange={this.handleChange}
            InputProps={{
              onBlur: this.handleBlur
            }}
            {...otherProps} />
        );
      }
    };
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        {getTextField()}
      </FormControl>
    );
  }
}

FormTextField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
  dataObject: PropTypes.object.isRequired,
  dependencyFields: PropTypes.object,
  validate: PropTypes.func,
  validationResult: PropTypes.object,
  fieldDefHandleBlur: PropTypes.func,
  updateFormFieldValidity: PropTypes.func.isRequired,
  handleFormFieldChange: PropTypes.func.isRequired
}

export default withFieldValidations(withStyles(styles)(FormTextField));
