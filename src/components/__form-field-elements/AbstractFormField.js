import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

import withFieldValidations from '../../containers/functional-hocs/withFieldValidations';
import withUpdateAppStateProfileInfoEdit from '../../containers/functional-hocs/withUpdateAppStateEdit';
import { getDataFieldValueFromDataObjectWithFieldPathAndFieldId } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    width: '100%'
  },
  textField: {

  }
});

/* abstract - OUTDATED */
class AbstractFormField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      errorMessage: null,
      value: this.resolveValue()
    };
    this.previousBlurValue = null;
  }
  
  componentDidMount = () => {
    if (this.props.fieldProps.subscribeToAppStateChange) {
      this.unsubscribe = this.props.subscribeToAppStateChange(() => {
        this.props.updateComponentWithSubscriptionAppState(this.state.value, self.setState);
      })
    }
  }

  componentWillUnmount = () => {
    this.unsubscribe && this.unsubscribe();
  }

  resolveValue = () => {
    // const value = getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this.props.dataObject, this.props.fieldProps);
    console.debug('No default implementation for resolveValue(), it has to be overridden.');
  }

  handleChange = (event) => {
    console.debug('No default implementation for handleChange(), it has to be overridden.');
  }

  handleBlur = (event) => {
    // const fieldDefHandleBlur = this.props.fieldProps.fieldDefHandleBlur;
    // if (fieldDefHandleBlur != null) {
    //   const value = event.target.value;
    //   if (!this.state.hasError && this.previousBlurValue !== this.state.value) {
    //     fieldDefHandleBlur(value, this.props.updateAppState);
    //   }
    //   this.previousBlurValue = value;
    // }
    console.debug('No default implementation for handleBlur(), it has to be overridden.');
  }

  resetValue = () => {
    this.setState({
      hasError: false,
      errorMessage: null,
      value: this.resolveValue()
    });
    // console.debug('No default implementation for resetValue(), it has to be overridden.');
  }

  render() {
    const {
      classes,
      fieldProps,
      dataObject,
      validate,
      fieldDefHandleChange,
      updateAppState,
      subscribeToAppStateChange,
      updateComponentWithSubscriptionAppState,
      ...otherProps
    } = this.props;

    const renderTextField = () => {
      <FormControl required={fieldProps.isRequired} className={classes.root} error={this.state.hasError}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <TextField
          id={fieldProps.fieldId}
          type={this.resolveTextFieldType(fieldProps.fieldSubType)}
          value={this.state.value || ''}
          className={classes.textField}
          disabled={fieldProps.isDisabled}
          error={this.state.hasError}
          helperText={this.state.errorMessage}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          {...otherProps} />
      </FormControl>
    };
    
    const renderCheckboxField = () => {
      <FormControl required={fieldProps.isRequired} className={classes.root} error={this.state.hasError}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <FormGroup className={classes.checkboxGroup}>
          {
            fieldProps.fieldValues.map((fieldValue, idx) => {
              return (
                <FormControlLabel
                  id={fieldProps.fieldId}
                  key={fieldProps.fieldLabel + '_' + idx}
                  control={
                    <Checkbox
                      color="primary"
                      value={fieldValue.value}
                      checked={this.state.value != null ? this.state.value.indexOf(fieldValue.value) !== -1 : false}
                      className={classes.radioGroup}
                      disabled={fieldProps.isDisabled}
                      onChange={this.handleChange} />
                  }
                  label={fieldValue.label}
                  {...otherProps} />
              )
            }, this)
          }
        </FormGroup>
        {this.state.hasError && this.state.errorMessage ? <FormHelperText>{this.state.errorMessage}</FormHelperText> : null}
      </FormControl>
    };

    const renderRadioField = () => {
      <FormControl required={fieldProps.isRequired} className={classes.root} error={this.state.hasError}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <RadioGroup
          id={fieldProps.fieldId}
          value={this.state.value != null ? this.state.value.toString() : null}
          className={classes.radioGroup}
          onChange={this.handleChange}
          {...otherProps}>
          {
            fieldProps.fieldValues.map((fieldValue, idx) => {
              return (
                <FormControlLabel
                  label={fieldValue.label}
                  value={fieldValue.value.toString()}
                  control={<Radio color="primary" disabled={fieldProps.isDisabled} />}
                  key={fieldProps.fieldLabel + '_' + idx} />
              );
            })
          }
        </RadioGroup>
        {this.state.hasError && this.state.errorMessage ? <FormHelperText>{this.state.errorMessage}</FormHelperText> : null}
      </FormControl>
    };

    const renderSelectionField = () => {
      <FormControl required={fieldProps.isRequired} className={classes.root} error={this.state.hasError}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <VirtualizedSelect
          id={fieldProps.fieldId}
          selectComponent={creatable ? Creatable : Select}
          filterOptions={createFilterOptions({options: this.state.options})}
          options={this.state.options}
          value={this.state.value}
          className={classes.select}
          disabled={fieldProps.isDisabled}
          onChange={this.handleChange}
          {...otherProps} />
          {this.state.hasError && this.state.errorMessage ? <FormHelperText>{this.state.errorMessage}</FormHelperText> : null}
      </FormControl>
    };

    const renderField = () => {
      switch (this.props.formFieldComponentType) {
        case 'FormTextField':
          return renderTextField()
        case 'FormCheckboxField':
          return renderCheckboxField()
        case 'FormRadioField':
          return renderRadioField()
        case 'FormSelectionField':
          return renderSelectionField()
        default:
          return renderTextField()
      }
    }

    return (
      renderField()
    );
  }
}

AbstractFormField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
  dataObject: PropTypes.object,
  validate: PropTypes.func,
  fieldDefHandleChange: PropTypes.func,
  updateAppState: PropTypes.func.isRequired,
  subscribeToAppStateChange: PropTypes.func,
  updateComponentWithSubscriptionAppState: PropTypes.func
}

export default withUpdateAppStateProfileInfoEdit(withFieldValidations(withStyles(styles)(AbstractFormField)));
