import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';

import withFieldValidations from '../../containers/functional-hocs/withFieldValidations';
import { getDataFieldValueFromDataObjectWithFieldPathAndFieldId } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    width: '100%'
  },
  checkboxGroup: {
    flexDirection: 'row',
    zIndex: 0
  }
});

class FormCheckboxField extends Component {
  componentDidMount() {
    if (this.props.fieldProps.isRequired) {
      let validationResult = this.props.validate();
      validationResult.suppressFormErrorDisplay = true;
      this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    }
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   hasError: false,
    //   errorMessage: null
    // };
    this.previousBlurValue = null;
  }

  resolveValue = () => {
    return getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this.props.dataObject, this.props.fieldProps);
  }

  handleChange = (event) => {
    const fieldProps = this.props.fieldProps;
    const value = event.currentTarget.value;
    const checked = event.currentTarget.checked;
    let validationResult = this.props.validate(checked.toString());
    // this.setState(validationResult);
    validationResult.suppressFormErrorDisplay = false;
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    let currentValue = this.resolveValue();
    let newValue = currentValue;
    if (Object.prototype.toString.call(newValue) === '[object Array]') {
      if (newValue != null && newValue.indexOf(value) === -1 && checked) {
        newValue.push(value);
      }
      if (newValue != null && newValue.indexOf(value) !== -1 && !checked) {
        const idx = newValue.indexOf(value);
        if (idx !== -1) newValue.splice(idx, 1);
      }
      // otherwise such as newValue == null, do nothing
    } else {
      newValue = checked;
    }
    this.props.handleFormFieldChange(fieldProps, newValue);
  }

  // handle blur action if the fieldProps defines the blur method
  handleBlur = (event) => {
    const fieldDefHandleBlur = this.props.fieldProps.fieldDefHandleBlur;
    if (fieldDefHandleBlur != null) {
      const value = event.target.value;
      if (!this.state.hasError && this.previousBlurValue !== this.state.value) {
        fieldDefHandleBlur(value, this.props.updateFormFieldValidity, this.props.handleFormFieldChange);
      }
      this.previousBlurValue = value;
    }
  }

  resetValue = () => {
    // this.setState({
    //   hasError: false,
    //   errorMessage: null,
    //   value: this.resolveValue()
    // });
    const validationResult = {
      hasError: false,
      errorMessage: null,
      suppressFormErrorDisplay: false
    };
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
  }

  render() {
    const {
      classes,
      fieldProps,
      dataObject,
      dependencyFields,
      validate,
      validationResult,
      fieldDefHandleBlur,
      updateFormFieldValidity,
      handleFormFieldChange,
      ...otherProps
    } = this.props;
    const value = this.resolveValue();
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <FormGroup className={classes.checkboxGroup}>
          {
            fieldProps.fieldValues.map((fieldValue, idx) => {
              return (
                <FormControlLabel
                  id={fieldProps.fieldId}
                  key={fieldProps.fieldLabel + '_' + idx}
                  control={
                    <Checkbox
                      color="primary"
                      value={fieldValue.label}
                      checked={value != null ? value.indexOf(fieldValue.label) !== -1 : false}
                      className={classes.checkboxGroup}
                      disabled={fieldProps.isDisabled}
                      onChange={this.handleChange} />
                  }
                  label={fieldValue.label}
                  {...otherProps} />
              )
            }, this)
          }
        </FormGroup>
        {
          validationResult && !validationResult.suppressFormErrorDisplay && validationResult.hasError
          ?
          <FormHelperText>{validationResult.errorMessage}</FormHelperText>
          :
          null
        }
      </FormControl>
    );
  }
}

FormCheckboxField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
  dataObject: PropTypes.object.isRequired,
  dependencyFields: PropTypes.object,
  validate: PropTypes.func,
  validationResult: PropTypes.object,
  fieldDefHandleBlur: PropTypes.func,
  updateFormFieldValidity: PropTypes.func.isRequired,
  handleFormFieldChange: PropTypes.func.isRequired
}

export default withFieldValidations(withStyles(styles)(FormCheckboxField));
