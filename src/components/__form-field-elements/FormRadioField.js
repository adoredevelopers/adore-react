import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';

import withFieldValidations from '../../containers/functional-hocs/withFieldValidations';
import { getDataFieldValueFromDataObjectWithFieldPathAndFieldId } from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    width: '100%'
  },
  radioGroup: {
    flexDirection: 'row',
    zIndex: 0
  }
});

class FormRadioField extends Component {
  componentDidMount() {
    if (this.props.fieldProps.isRequired) {
      let validationResult = this.props.validate();
      validationResult.suppressFormErrorDisplay = true;
      this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    }
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   hasError: false,
    //   errorMessage: null
    // };
    this.previousBlurValue = null;
  }

  resolveValue = () => {
    return getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this.props.dataObject, this.props.fieldProps)
  }

  handleChange = (event, value) => {
    const fieldProps = this.props.fieldProps;
    value = this.props.fieldProps.fieldSubType === 'booleanRadioField' ? value === 'true' : value;
    let validationResult = this.props.validate(value);
    validationResult.suppressFormErrorDisplay = false;
    // this.setState(validationResult);
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    this.props.handleFormFieldChange(fieldProps, value);
  }

  // handle blur action if the fieldProps defines the blur method
  handleBlur = (event) => {
    const fieldDefHandleBlur = this.props.fieldProps.fieldDefHandleBlur;
    if (fieldDefHandleBlur != null) {
      const value = event.target.value;
      if (!this.state.hasError && this.previousBlurValue !== this.state.value) {
        fieldDefHandleBlur(value, this.props.updateFormFieldValidity, this.props.handleFormFieldChange);
      }
      this.previousBlurValue = value;
    }
  }

  resetValue = () => {
    // this.setState({
    //   hasError: false,
    //   errorMessage: null,
    //   value: this.resolveValue()
    // });
    const validationResult = {
      hasError: false,
      errorMessage: null,
      suppressFormErrorDisplay: false
    };
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
  }

  render() {
    const {
      classes,
      fieldProps,
      dataObject,
      dependencyFields,
      validate,
      validationResult,
      fieldDefHandleBlur,
      updateFormFieldValidity,
      handleFormFieldChange,
      ...otherProps
    } = this.props;
    const value = this.resolveValue();
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <RadioGroup
          id={fieldProps.fieldId}
          value={value != null ? value.toString() : null}
          className={classes.radioGroup}
          onChange={this.handleChange}
          {...otherProps}>
          {
            fieldProps.fieldValues.map((fieldValue, idx) => {
              return (
                <FormControlLabel
                  label={fieldValue.label}
                  value={fieldValue.value.toString()}
                  control={<Radio color="primary" disabled={fieldProps.isDisabled} />}
                  key={fieldProps.fieldLabel + '_' + idx} />
              );
            })
          }
        </RadioGroup>
        {
          validationResult && !validationResult.suppressFormErrorDisplay && validationResult.hasError
          ?
          <FormHelperText>{validationResult.errorMessage}</FormHelperText>
          :
          null
        }
      </FormControl>
    );
  }
}

FormRadioField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
  dataObject: PropTypes.object.isRequired,
  dependencyFields: PropTypes.object,
  validate: PropTypes.func,
  validationResult: PropTypes.object,
  fieldDefHandleBlur: PropTypes.func,
  updateFormFieldValidity: PropTypes.func.isRequired,
  handleFormFieldChange: PropTypes.func.isRequired
}

export default withFieldValidations(withStyles(styles)(FormRadioField));
