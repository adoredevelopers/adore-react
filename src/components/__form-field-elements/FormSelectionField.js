import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
/* react-select group related */
// import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import Select, { Creatable } from 'react-select';
import VirtualizedSelect from 'react-virtualized-select';
import createFilterOptions from 'react-select-fast-filter-options';

import withFieldValidations from '../../containers/functional-hocs/withFieldValidations';
import {
  getDataFieldValueFromDataObjectWithFieldPathAndFieldId,
  addValueToSelectionFieldOptions,
  mergeSelectionFieldOptions
} from '../../utils/helpers/commonHelpers';

const styles = theme => ({
  root: {
    width: '100%'
  },
  select: {
    marginTop: '10px',
    minWidth: '100px'
  }
});

class FormSelectionField extends Component {
  componentDidMount() {
    if (this.props.fieldProps.isRequired) {
      let validationResult = this.props.validate();
      validationResult.suppressFormErrorDisplay = true;
      this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    }
  }

  constructor(props) {
    super(props);
    // this.state = {
    //   hasError: false,
    //   errorMessage: null
    // };
    this.previousBlurValue = null;
    this.currentOptions = this.props.fieldProps.fieldValues;
  }

  buildOptions = (value) => {
    let options = this.currentOptions;
    if (value != null && value.length > 0) {
      const fieldId = this.props.fieldProps.fieldId;
      let dependencyFieldsIter = this.props.dependencyFields;
      if (dependencyFieldsIter != null) {
        const pathTokens = this.props.fieldProps.fieldPath.split('.');
        for (let i = 0; i < pathTokens.length; i++) {
          dependencyFieldsIter = dependencyFieldsIter[pathTokens[i]];
          if (dependencyFieldsIter == null) break; // this field is not declared in the dependency field list
        }
        if (dependencyFieldsIter != null && dependencyFieldsIter[fieldId] != null) {
          // there are other field changes trying to update current selection field value, thus dependency actions are triggers below
          options = mergeSelectionFieldOptions(options, dependencyFieldsIter[fieldId].options);
        }
      }
      options = addValueToSelectionFieldOptions(options, value);
    }
    return options;
  }

  resolveValue = () => {
    return getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this.props.dataObject, this.props.fieldProps);
  }

  handleChange = (option) => {
    const fieldProps = this.props.fieldProps;
    let value = null;
    if (option == null || option.length === 0) {
      value = null;
    } else {
      if (this.props.multi) {
        value = option.map((singleOption) => {
          return singleOption.value;
        });
      } else {
        value = option.value;
      }
    }
    let validationResult = this.props.validate(value);
    validationResult.suppressFormErrorDisplay = false;
    // this.setState(validationResult);
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
    this.props.handleFormFieldChange(fieldProps, value);
  }

  // handle blur action if the fieldProps defines the blur method
  handleBlur = (event) => {
    const fieldDefHandleBlur = this.props.fieldProps.fieldDefHandleBlur;
    if (fieldDefHandleBlur != null) {
      const value = event.target.value;
      if (!this.state.hasError && this.previousBlurValue !== this.state.value) {
        fieldDefHandleBlur(value, this.props.updateFormFieldValidity, this.props.handleFormFieldChange);
      }
      this.previousBlurValue = value;
    }
  }

  resetValue = () => {
    // this.setState({
    //   hasError: false,
    //   errorMessage: null,
    //   value: this.resolveValue()
    // });
    const validationResult = {
      hasError: false,
      errorMessage: null,
      suppressFormErrorDisplay: false
    };
    this.props.updateFormFieldValidity(this.props.fieldProps.fieldId, validationResult);
  }

  render() {
    const {
      classes,
      fieldProps,
      dataObject,
      dependencyFields,
      creatable,
      validate,
      validationResult,
      fieldDefHandleBlur,
      updateFormFieldValidity,
      handleFormFieldChange,
      ...otherProps
    } = this.props;
    const value = this.resolveValue();
    const options = this.buildOptions(value);
    return (
      <FormControl
        required={fieldProps.isRequired}
        className={classes.root}
        error={validationResult && !validationResult.suppressFormErrorDisplay ? validationResult.hasError : false}>
        <FormLabel>{fieldProps.fieldLabel}</FormLabel>
        <VirtualizedSelect
          id={fieldProps.fieldId}
          selectComponent={creatable ? Creatable : Select}
          filterOptions={createFilterOptions({options})}
          options={options}
          value={value}
          className={classes.select}
          disabled={fieldProps.isDisabled}
          onChange={this.handleChange}
          {...otherProps} />
          {
            validationResult && !validationResult.suppressFormErrorDisplay && validationResult.hasError
            ?
            <FormHelperText>{validationResult.errorMessage}</FormHelperText>
            :
            null
          }
      </FormControl>
    );
  }
}

FormSelectionField.propTypes = {
  classes: PropTypes.object.isRequired,
  fieldProps: PropTypes.object.isRequired,
  dataObject: PropTypes.object.isRequired,
  dependencyFields: PropTypes.object,
  creatable: PropTypes.bool,
  validate: PropTypes.func,
  validationResult: PropTypes.object,
  fieldDefHandleBlur: PropTypes.func,
  updateFormFieldValidity: PropTypes.func.isRequired,
  handleFormFieldChange: PropTypes.func.isRequired
}

export default withFieldValidations(withStyles(styles)(FormSelectionField));
