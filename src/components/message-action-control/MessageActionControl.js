import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core'
import classnames from 'classnames';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import IconButton from '@material-ui/core/IconButton';
import SendIcon from '@material-ui/icons/Send';

const styles = theme => ({
  root: {
    display: 'flex',
    borderTop: '1px solid lightgray'
  },
  messageInputBox: {
    flex: '1 1 0',
    marginLeft: '10px',
    paddingBottom: '8px'
  },
  messageSendButton: {
    alignSelf: 'flex-end'
  },
  multiLineInput: {
    marginTop: '5px'
  }
});

class MessageActionControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
      inputMultilineHackInitialState: true
    };
  }

  handleMessageInputBoxChange = (event) => {
    this.setState({
      message: event.currentTarget.value,
      inputMultilineHackInitialState: false
    });
  }

  sendMessage = () => {
    this.props.sendMessage(this.props.currentConversationUserId, this.state.message);
    this.setState({
      message: '',
      inputMultilineHackInitialState: true
    });
    this.props.setShouldScrollToMessageBottom(true);
  }

  render() {
    const classes = this.props.classes;
    return (
      <div className={classes.root}>
        <FormControl className={classes.messageInputBox}>
          <Input
            multiline
            autoFocus
            fullWidth
            rows={1}
            rowsMax={8}
            placeholder="write something..."
            value={this.state.message}
            className={classnames(classes.multiLineInput, 'multiLineInput')}
            onChange={this.handleMessageInputBoxChange} />
        </FormControl>
        <IconButton
          color="secondary"
          className={classes.messageSendButton}
          disabled={this.state.message.trim().length === 0}
          onClick={this.sendMessage}>
          <SendIcon />
        </IconButton>
      </div>
    );
  }
}

MessageActionControl.propTypes = {
  classes: PropTypes.object.isRequired,
  currentConversationUserId: PropTypes.string.isRequired,
  sendMessage: PropTypes.func.isRequired,
  setShouldScrollToMessageBottom: PropTypes.func.isRequired
}

export default withStyles(styles)(MessageActionControl);
