import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ImageLoader from 'react-image-loader2';

import navigationLinks from '../../utils/navigationLinks';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';
import { getAgeFromBirthDate, getZodiacFromBirthDate } from '../../utils/helpers/commonHelpers';

import './list-row.css';
const styles = theme => ({
  root: {
    width: '100%'
  },
  listRowButton: {
    display: 'block',
    marginBottom: theme.spacing.unit * 2,
    padding: '0',
    height: '100px',
    width: '100%'
  },
  paper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    height: '100px',
    textAlign: 'left'
  },
  avatarImage: {
    position: 'relative',
    width: '100px',
    height: '100px',
    backgroundColor: '#333'
  },
  image: {
    display: 'block',
    width: '100px',
    height: '100px',
    lineHeight: '100px',
    color: 'ivory',
    textAlign: 'center'
  },
  info: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    margin: theme.spacing.unit,
    flex: '1 1 0'
  },
  name: {
    fontSize: '18px',
    fontWeight: 'bolder',
    textTransform: 'none'
  },
  zodiac: {

  },
  residence: {
    color: 'gray',
    fontSize: '13px',
    textTransform: 'none'
  },
  bio: {
    marginTop: '8px',
    height: '40px',
    fontFamily: 'Roboto, "Open Sans", Arial, "Hiragino Sans GB", 微软雅黑, 华文细黑, STHeiti, "Microsoft YaHei"',
    fontSize: '12px',
    textTransform: 'none',
    overflow: 'hidden'
  }
});

class ListRow extends Component {
  render() {
    const classes = this.props.classes;
    const userInfo = this.props.userInfo;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Link to={navigationLinks('USER_DETAILS_PAGE_ROUTE', userInfo._id)} style={{textDecoration: 'none'}}>
          <Button className={classes.listRowButton}>
            <Paper className={classNames(classes.paper, 'listRow')} elevation={this.props.elevation} square={true}>
              <div className={classes.avatarImage}>
                <ImageLoader className={classes.image} src={userInfo.displayImages[0][appConfig.imageStorage][constants.imageSizes.SMALL]} alt="Avatar" />
              </div>
              <div className={classes.info}>
                <Typography className={classes.name} variant="h5" component="h3">
                  {userInfo.profile.basic.name}, {getAgeFromBirthDate(userInfo.profile.basic.birthday)}, <span className={classes.zodiac}>{getZodiacFromBirthDate(userInfo.profile.basic.birthday)} <span role="img" aria-label="zodiac">♈</span></span>
                </Typography>
                <Typography className={classes.residence} component="span">
                  {userInfo.profile.basic.city}, {userInfo.profile.basic.province}, {userInfo.profile.basic.country}
                </Typography>
                {/*
                  <Typography className={classNames(classes.bio, 'block-with-text')} component="p">
                    {userInfo.introductions.introducingMyself}
                  </Typography>
                */}
                <Typography className={classes.bio} component="p">
                  {userInfo.introductions.introducingMyself}
                </Typography>
              </div>
            </Paper>
          </Button>
        </Link>
      </div>
    );
  }
}

ListRow.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  elevation: PropTypes.number,
  href: PropTypes.string,
  userInfo: PropTypes.object.isRequired
};

export default withStyles(styles)(ListRow);
