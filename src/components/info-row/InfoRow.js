import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';

const styles = theme => ({
  root: {

  },
  infoTitle: {
    fontSize: '22px',
    lineHeight: '40px',
    color: '#d34a5a',
    margin: '0 10px',
    '@media only screen and (min-width: 600px)': {
      margin: '0'
    }
  },
  infoTitleEdit: {
    display: 'flex',
    alignItems: 'center',
    color: '#4a90e2',
    cursor: 'pointer'
  },
  editIcon: {
    marginLeft: '5px',
    width: '18px',
    height: '18px'
  },
  infoText: {
    padding: '10px',
    fontSize: '16px',
    lineHeight: '26px',
    whiteSpace: 'pre-line'
  },
  infoTextEdit: {
    cursor: 'text'
  },
  hintText: {
    padding: '10px',
    fontSize: '16px',
    lineHeight: '26px',
    whiteSpace: 'pre-line'
  },
  hintTextEdit: {
    color: 'grey',
    fontStyle: 'italic',
    cursor: 'text'
  },
  infoEdit: {
    marginBottom: '16px',
    padding: '0 10px'
  },
  editTextArea: {
    lineHeight: '26px',
    width: '100%'
  },
  editTextControls: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: '10px'
  }
});

class InfoRow extends Component {
  handleEditClick = (event) => {
    this.props.onEditClick(this.props.infoField.fieldId);
  };

  handleChange = (event) => {
    this.props.onChange(this.props.infoField.fieldId, event.target.value) //currentTarget is invalid here
  };

  handleCancel = () => {
    this.props.onCancel(this.props.infoField.fieldId);
  };

  handleSubmit = () => {
    this.props.onSubmit(this.props.infoField.fieldId);
  };

  render() {
    const classes = this.props.classes;
    const getInfoTitle = () => {
      return (
        <Typography
          variant="h5"
          component="h3"
          className={this.props.isEdit ? classNames(classes.infoTitle, classes.infoTitleEdit) : classes.infoTitle}
          onClick={this.props.isEdit ? this.handleEditClick : null}>
          {this.props.infoField.fieldLabel}
          {this.props.isEdit ? <EditIcon className={classes.editIcon} /> : null}
        </Typography>
      );
    };
    const self = this;
    const getInfoBody = () => {
      const fieldValueEmpty = this.props.infoField.fieldValue == null || this.props.infoField.fieldValue.trim().length === 0;
      const getInfoBodyStyle = () => {
        if (self.props.infoField.editActive) {
          return {
            display: 'none'
          };
        } else if (this.props.infoField.fieldValue === null || this.props.infoField.fieldValue.trim().length === 0) {
          if (this.props.isEdit) {
            return {
              display: 'none'
            };
          } else {
            return {
              color: 'darkgrey',
              fontSize: '14px',
              fontStyle: 'italic'
            };
          }
        } else {
          return null;
        }
      };
      return (
        <Typography
          component="p"
          paragraph
          style={getInfoBodyStyle()}
          className={this.props.isEdit ? classNames(classes.infoText, classes.infoTextEdit) : classes.infoText}
          onClick={this.props.isEdit ? this.handleEditClick : null}>
          {!fieldValueEmpty ? this.props.infoField.fieldValue : 'Leave blank...'}
        </Typography>
      );
    }
    return (
      <div className={classes.root}>
        {getInfoTitle()}
        {getInfoBody()}
        <Typography
          component="span"
          paragraph
          style={
            this.props.isEdit && !this.props.infoField.editActive && (this.props.infoField.fieldValue == null || this.props.infoField.fieldValue.trim().length === 0)
            ?
            {display: 'block'}
            :
            {display: 'none'}
          }
          className={this.props.isEdit ? classNames(classes.hintText, classes.hintTextEdit) : classes.hintText}
          onClick={this.props.isEdit ? this.handleEditClick : null}>
          {this.props.infoField.fieldValuePlaceholder}
        </Typography>
        <div style={this.props.infoField.editActive ? {display: 'block'} : {display: 'none'}} className={classes.infoEdit}>
          <TextField
            inputRef={this.props.inputRef}
            multiline
            rows={this.props.fieldRows}
            placeholder={this.props.infoField.fieldValuePlaceholder}
            value={this.props.infoField.fieldValue}
            className={classes.editTextArea}
            onChange={this.handleChange} />
          <div className={classes.editTextControls}>
            <Button variant="contained" className={classes.button} onClick={this.handleCancel}>Cancel</Button>
            <Button variant="contained" color="primary" className={classes.button} onClick={this.handleSubmit}>Submit</Button>
          </div>
        </div>
      </div>
    );
  }
}

InfoRow.propTypes = {
  classes: PropTypes.object.isRequired,
  inputRef: PropTypes.func,
  fieldRows: PropTypes.number.isRequired,
  isEdit: PropTypes.bool,
  infoField: PropTypes.object.isRequired,
  onEditClick: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default withStyles(styles)(InfoRow);
