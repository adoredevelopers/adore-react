import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {

  },
  card: {
    marginBottom: theme.spacing.unit * 2,
    maxWidth: '450px'
  },
  image: {
    width: '100%'
  }
});

class SecondaryListRow extends Component {
  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Card className={classes.card} elevation={this.props.elevation} square={true}>
          <CardMedia
            className={classes.cardMedia}
            image="http://placehold.it/350x120"
            title="Placeholder">
            <img className={classes.image} src="http://placehold.it/350x120" alt="Placeholder" />
          </CardMedia>
          <CardContent>
            <Typography variant="h5" component="h2">
              Lizard
            </Typography>
            <Typography component="p">
              Lizards are a widespread group of squamate reptiles, with over
              6,000 species, ranging across all continents except Antarctica
            </Typography>
          </CardContent>
          <CardActions>
            <Button color="primary">Share</Button>
            <Button color="primary">Learn More</Button>
          </CardActions>
        </Card>
      </div>
    );
  }
}

SecondaryListRow.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  elevation: PropTypes.number,
  image: PropTypes.string
};

export default withStyles(styles)(SecondaryListRow);
