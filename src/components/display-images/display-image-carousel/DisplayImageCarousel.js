import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ArrowLeftIcon from '@material-ui/icons/NavigateBefore';
import ArrowRightIcon from '@material-ui/icons/NavigateNext';
import ImageLoader from 'react-image-loader2';

import DisplayImageCarouselFullScreenDialog from './display-image-carousel-full-screen-dialog/DisplayImageCarouselFullScreenDialog';
import appConfig from '../../../config/appConfig';
import constants from '../../../constants/constants';

const styles = theme => ({
  root: {
    position: 'relative',
    height: '100%'
  },
  displayImageCarouselInplace: {
    height: '100%'
  },
  arrowButton: {
    position: 'absolute',
    top: '50%',
    width: '50px',
    height: '60px',
    marginTop: '-30px',
    padding: 0,
    borderRadius: 'unset',
    '@media only screen and (min-width: 600px)': {
      height: '120px',
      marginTop: '-60px'
    }
  },
  leftArrowButton: {
    left: 0,
    color: 'ivory'
  },
  rightArrowButton: {
    right: 0,
    color: 'ivory'
  },
  arrowIcon: {
    width: '50px',
    height: '60px',
    backgroundColor: 'rgba(0,0,0,0.5)',
    fillOpacity: '0.8',
    '@media only screen and (min-width: 600px)': {
      height: '120px'
    }
  },
  leftArrowIcon: {
    borderRadius: '0 8px 8px 0'
  },
  rightArrowIcon: {
    borderRadius: '8px 0 0 8px'
  },
  carousel: {
    height: '100%',
    backgroundColor: '#333'
  },
  carouselImageItem: {
    width: '100%',
  },
  carouselImageItemHidden: {
    display: 'none'
  },
  carouselImage: {
    display: 'block'
  }
});

class DisplayImageCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImageIndex: this.props.currentImageIndex || 0,
      displayImageCarouselFullScreenDialog: {
        open: false
      }
    };
  }

  handleArrowClick = (event, forward) => {
    event.stopPropagation();
    let newIndex = 0;
    if (forward) {
      newIndex = (this.state.currentImageIndex === this.props.displayImages.length - 1) ? 0 : this.state.currentImageIndex + 1;
    } else {
      newIndex = (this.state.currentImageIndex === 0) ? this.props.displayImages.length - 1 : this.state.currentImageIndex - 1;
    }
    this.setState({
      currentImageIndex: newIndex
    });
    this.props.syncCurrentImageIndex && this.props.syncCurrentImageIndex(newIndex);
  };

  handleDisplayImageCarouselFullScreenDialogOpen = (event) => {
    this.setState({
      displayImageCarouselFullScreenDialog: {
        open: true
      }
    });
  };

  handleDisplayImageCarouselFullScreenRequestClose = (event) => {
    this.setState({
      displayImageCarouselFullScreenDialog: {
        open: false
      }
    });
  };

  syncCurrentImageIndex = (newIndex) => {
    this.setState({
      currentImageIndex: newIndex
    });
    this.props.syncCurrentImageIndex && this.props.syncCurrentImageIndex(newIndex);
  };

  render() {
    const classes = this.props.classes;
    const displayImageSize = this.props.isMobile ? window.innerWidth : this.props.displayImageSize;
    const carouselImageItems = this.props.displayImages.map((image, idx) => {
      return (
        <div
          className={
            idx === this.state.currentImageIndex
            ?
            classes.carouselImageItem
            :
            classes.carouselImageItemHidden
          }
          style={this.props.isMobile ? {height: displayImageSize} : null}
          key={idx}>
          <ImageLoader
            className={classes.carouselImage}
            style={{
              width: displayImageSize + 'px',
              height: displayImageSize + 'px',
              lineHeight: displayImageSize + 'px',
              textAlign: 'center',
              color: 'ivory'
            }}
            src={image[appConfig.imageStorage][constants.imageSizes.LARGE]}
            alt={(idx + 1) + ' of ' + this.props.displayImages.length} />
        </div>
      );
    });
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <div className={classes.displayImageCarouselInplace} onClick={this.props.disableFullScreenCarouselClick ? null : this.handleDisplayImageCarouselFullScreenDialogOpen}>
          <IconButton
            color="secondary"
            className={classNames(classes.arrowButton, classes.leftArrowButton)}
            onClick={(event) => this.handleArrowClick(event, false)}>
            <ArrowLeftIcon className={classNames(classes.arrowIcon, classes.leftArrowIcon)} />
          </IconButton>
          <IconButton
            color="secondary"
            className={classNames(classes.arrowButton, classes.rightArrowButton)}
            onClick={(event) => this.handleArrowClick(event, true)}>
            <ArrowRightIcon className={classNames(classes.arrowIcon, classes.rightArrowIcon)} />
          </IconButton>
          <div className={classes.carousel}>
            {carouselImageItems}
          </div>
        </div>
        {
          this.props.disableFullScreenCarouselClick ? '' : (
          <DisplayImageCarouselFullScreenDialog
            displayImages={this.props.displayImages}
            currentImageIndex={this.state.currentImageIndex}
            syncCurrentImageIndex={this.syncCurrentImageIndex}
            fullScreen
            open={this.state.displayImageCarouselFullScreenDialog.open}
            onClose={this.handleDisplayImageCarouselFullScreenRequestClose} />
          )
        }
      </div>
    );
  }
}

DisplayImageCarousel.propTypes = {
  classes: PropTypes.object.isRequired,
  isMobile: PropTypes.bool,
  displayImageSize: PropTypes.number,
  displayImages: PropTypes.array.isRequired,
  currentImageIndex: PropTypes.number.isRequired,
  disableFullScreenCarouselClick: PropTypes.bool,
  syncCurrentImageIndex: PropTypes.func
};

export default withStyles(styles)(DisplayImageCarousel);
