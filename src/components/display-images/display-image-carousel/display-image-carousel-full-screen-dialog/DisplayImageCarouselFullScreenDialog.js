import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import Carousel from './re-carousel/carousel';
import IndicatorDots from './re-carousel/indicator-dots';
import CloseIcon from '@material-ui/icons/Close';
import ImageLoader from 'react-image-loader2';

import appConfig from '../../../../config/appConfig';
import constants from '../../../../constants/constants';

const styles = theme => ({
  dialogContent: {
    position: 'fixed',
    top: '0',
    right: '0',
    bottom: '0',
    left: '0',
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
    overflow: 'hidden'
  },
  closeButton: {
    position: 'fixed',
    top: '0',
    right: '0',
    backgroundColor: 'transparent',
    zIndex: '999'
  },
  closeIcon: {
    width: '30px',
    height: '30px',
    color: 'ivory'
  },
  carouselItem: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center'
  },
  profileImage: {
    width: '100%'
  }
});

class DisplayImageCarouselFullScreenDialog extends Component {
  render() {
    const {
      classes,
      displayImages,
      currentImageIndex,
      onClose,
      syncCurrentImageIndex,
      ...otherProps
    } = this.props;
    const carouselImageItems = displayImages.map((image, idx) => {
      return <div className={classes.carouselItem} key={idx}><ImageLoader className={classes.profileImage} src={image[appConfig.imageStorage][constants.imageSizes.LARGE]} alt={(idx + 1) + ' of ' + displayImages.length} /></div>
    });
    return (
      <Dialog
        {...otherProps}>
        <div className={classes.dialogContent}>
          <div className={classes.closeButton} onClick={onClose}>
            <IconButton color="secondary"><CloseIcon className={classes.closeIcon} /></IconButton>
          </div>
          <Carousel loop widgets={[IndicatorDots]} carouselFrameIndex={currentImageIndex} syncCarouselFrameIndex={syncCurrentImageIndex}>
            {carouselImageItems}
          </Carousel>
        </div>
      </Dialog>
    );
  }
}

DisplayImageCarouselFullScreenDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  displayImages: PropTypes.array.isRequired,
  displayImageIndex: PropTypes.number,
  syncCurrentImageIndex: PropTypes.func,
  open: PropTypes.bool.isRequired,
  fullScreen: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(DisplayImageCarouselFullScreenDialog);
