import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import DisplayImageCarouselContainer from '../../../containers/DisplayImageCarouselContainer';

const styles = theme => ({
  dialogContent: {
    maxWidth: '1024px'
  },
  carouselImages: {
    height: '100%'
  },
  dialogTitleLeft: {
    float: 'left'
  },
  dialogTitleRight: {
    float: 'right'
  }
});

class DisplayImageDialog extends Component {
  constructor(props) {
    super(props);
    this.dialogBottomTextBarHeight = 68; // 68px
    this.dialogMaximumHeight = 1024;
    this.dialogMaximumWidth = this.dialogMaximumHeight - this.dialogBottomTextBarHeight;
    this.state = {
      dialogImageSize: 0
    }
  }

  componentDidMount = () => {
    this.updateViewPortDimensions();
    window.addEventListener('resize', this.updateViewPortDimensions);
  }
  
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateViewPortDimensions);
  }

  resolveDialogdisplayImageStyle = () => {
    let style = {
      width: this.state.displayImageSize + 'px',
      maxWidth: this.dialogMaximumWidth + 'px',
      height: this.state.displayImageSize + 'px',
      maxHeight: this.dialogMaximumHeight + 'px'
    };
    return style;
  }
  
  updateViewPortDimensions = () => {
    // derive dialog size (the image square size) from current window viewport width and height
    let size = window.innerWidth < window.innerHeight ? window.innerWidth : window.innerHeight;
    size *= 0.75;
    size = size > 800 ? 800 : size;
    // update the state
    let newState = this.state;
    newState.displayImageSize = size;
    this.setState(newState);
  }

  render() {
    const {
      classes,
      userName,
      displayImages,
      currentImageIndex,
      syncCurrentImageIndex,
      history,
      location,
      match,
      ...otherProps
    } = this.props;
    return (
      <Dialog
        aria-labelledby="profile image display modal"
        {...otherProps}>
        <div style={this.resolveDialogdisplayImageStyle()} className={classes.dialogContent}>
          <div className={classes.carouselImages}>
            <DisplayImageCarouselContainer
              displayImages={displayImages}
              currentImageIndex={currentImageIndex}
              displayImageSize={this.state.displayImageSize}
              disableFullScreenCarouselClick={true}
              syncCurrentImageIndex={syncCurrentImageIndex} />
          </div>
        </div>
        <DialogTitle>
          <span className={classes.dialogTitleLeft}><strong>{userName}</strong></span>
          <span className={classes.dialogTitleRight}>{currentImageIndex + 1} / {displayImages.length}</span>
        </DialogTitle>
      </Dialog>
    );
  }
}

DisplayImageDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  userName: PropTypes.string,
  displayImages: PropTypes.array.isRequired,
  currentImageIndex: PropTypes.number.isRequired,
  open: PropTypes.bool.isRequired,
  maxWidth: PropTypes.string.isRequired,
  syncCurrentImageIndex: PropTypes.func,
  onClose: PropTypes.func.isRequired
}

export default withStyles(styles)(DisplayImageDialog);
