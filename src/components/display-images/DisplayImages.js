import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ImageLoader from 'react-image-loader2';

import DisplayImageDialog from './display-image-dialog/DisplayImageDialog';
import appConfig from '../../config/appConfig';
import constants from '../../constants/constants';

import './display-images.css';
const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row'
  },
  primaryImageButton: {
    marginRight: '10px',
    padding: '0',
    width: '200px',
    height: '200px',
    borderRadius: '4px',
    backgroundColor: '#333'
  },
  primaryImage: {
    width: '200px',
    height: '200px',
    borderRadius: '4px',
    color: 'ivory',
    lineHeight: '200px'
  },
  secondaryImageGroup: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignContent: 'space-between',
    justifyContent: 'space-between',
    marginRight: '20px'
  },
  secondaryImageGroupZeroCol: {
    width: '0',
    height: '0'
  },
  secondaryImageGroupOneCol: {

  },
  secondaryImageGroupTwoCol: {
    width: '200px',
    height: '200px'
  },
  secondaryImageButton: {
    padding: '0',
    width: '95px',
    height: '95px',
    borderRadius: '4px',
    backgroundColor: '#333'
  },
  secondaryImage: {
    width: '95px',
    height: '95px',
    borderRadius: '4px',
    color: 'ivory',
    lineHeight: '95px'
  },
  secondaryImageCounterOverlay: {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '95px',
    height: '95px',
    lineHeight: '95px',
    borderRadius: '4px',
    color: 'white',
    fontSize: '36px',
    textAlign: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  }
});

class DisplayImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImageIndex: 0,
      displayImageDialog: {
        open: false
      }
    };
  }

  handleDisplayImageClick = (event) => {
    this.setState({
      currentImageIndex: parseInt(event.currentTarget.dataset.index, 10),
      displayImageDialog: {
        open: true
      }
    });
  };

  handleDisplayImageDialogRequestClose = (event) => {
    this.setState({
      currentImageIndex: 0,
      displayImageDialog: {
        open: false
      }
    });
  };

  syncCurrentImageIndex = (newIndex) => {
    this.setState({
      currentImageIndex: newIndex
    });
  };
  
  render() {
    const classes = this.props.classes;
    const routeProps = {
      history: this.props.history,
      location: this.props.location,
      match: this.props.match
    }
    const primaryImageItem = (() => {
      if (this.props.displayImages.length > 0) {
        return (
          <Button className={classes.primaryImageButton} data-index="0" onClick={this.handleDisplayImageClick}>
            <ImageLoader className={classNames(classes.primaryImage, 'hoverImage')} src={this.props.displayImages[0][appConfig.imageStorage][constants.imageSizes.LARGE]} alt={'1 of ' + this.props.displayImages.length} />
          </Button>
        );
      } else {
        return <ImageLoader className={classes.primaryImage} src={constants.DEFAULT_DISPLAY_IMAGE} alt="No Display image" />;
      }
    })();
    const secondaryImageItems = this.props.displayImages.map((image, idx) => {
      const displayThresholdIndex = 4;
      const nonDisplayingImageCount = this.props.displayImages.length - 1 - displayThresholdIndex;
      let nonDisplayingImageCounterOverlay = '';
      if (idx === displayThresholdIndex && nonDisplayingImageCount > 0) {
        nonDisplayingImageCounterOverlay = <div className={classes.secondaryImageCounterOverlay}>+{nonDisplayingImageCount}</div>
      }
      if (idx > 0 && idx <= displayThresholdIndex) {
        return (
          <Button className={classes.secondaryImageButton} key={idx} data-index={idx} onClick={this.handleDisplayImageClick}>
            <ImageLoader className={classNames(classes.secondaryImage, 'hoverImage')} src={image[appConfig.imageStorage][constants.imageSizes.LARGE]} alt={(idx + 2) + ' of ' + this.props.displayImages.length} />
            {nonDisplayingImageCounterOverlay}
          </Button>
        );
      } else {
        return null;
      }
    });
    return (
      <div style={Object.assign({}, this.props.styles)} className={classes.root}>
        {primaryImageItem}
        <div className={
          this.props.displayImages.length <= 1
          ?
          classNames(classes.secondaryImageGroup, classes.secondaryImageGroupZeroCol)
          :
          (
            this.props.displayImages.length > 1 && this.props.displayImages.length <= 3
            ?
            classNames(classes.secondaryImageGroup, classes.secondaryImageGroupOneCol)
            :
            classNames(classes.secondaryImageGroup, classes.secondaryImageGroupTwoCol)
          )
          }>
          {secondaryImageItems}
        </div>
        <DisplayImageDialog
          userName={this.props.userName}
          displayImages={this.props.displayImages}
          currentImageIndex={this.state.currentImageIndex}
          open={this.state.displayImageDialog.open}
          maxWidth="md"
          syncCurrentImageIndex={this.syncCurrentImageIndex}
          onClose={this.handleDisplayImageDialogRequestClose}
          {...routeProps} />
      </div>
    );
  }
}

DisplayImages.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  userName: PropTypes.string,
  displayImages: PropTypes.array.isRequired
};

export default withStyles(styles)(DisplayImages);
