import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import ListRow from '../../components/list-row/ListRow';

const styles = theme => ({
  root: {
    
  },
  noResults: {
    marginBottom: '10px',
    textAlign: 'center',
    fontWeight: 'bold'
  }
});

class UserList extends Component {
  render() {
    const {
      classes,
      users,
      currentPage,
      recordsPerPage
    } = this.props;
    const currentPageUsers = users.length > recordsPerPage ? users.slice(currentPage * recordsPerPage, currentPage * recordsPerPage + recordsPerPage) : users;
    return (
      <div className={classes.root}>
        {
          currentPageUsers.length > 0
          ?
          currentPageUsers.map((user, idx) => {
            return <ListRow elevation={1} userInfo={user} key={'listRow_' + user._id + '_' + idx} />
          })
          :
          <div className={classes.noResults}>No result</div>
        }
      </div>
    );
  }
}

UserList.propTypes = {
  classes: PropTypes.object.isRequired,
  currentPage: PropTypes.number.isRequired,
  recordsPerPage: PropTypes.number.isRequired,
  users: PropTypes.array.isRequired
}

export default withStyles(styles)(UserList);
