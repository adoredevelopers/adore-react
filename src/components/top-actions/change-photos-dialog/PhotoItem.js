import React, { Component } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import ImageLoader from 'react-image-loader2';

import constants from '../../../constants/constants';

const styles = theme => ({
  root: {
    margin: '10px',
    // backgroundColor: '#333',
    cursor: 'move'
  },
  draggingOpacity: {
    opacity: 0.9
  },
  image: {
    verticalAlign: 'bottom'
  },
  badge: {

  },
  badgeRemoveImage: {
    width: '20px',
    height: '20px',
    backgroundColor: 'red',
    borderRadius: '50%',
    textAlign: 'center',
    lineHeight: '20px',
    cursor: 'pointer'
  },
});

class PhotoItem extends Component {
  render() {
    const {
      classes,
      photoItem,
      imageStorage,
      viewPort,
      imageSize,
      handleRemoveImageClick,
      isDragging,
      isOver
    } = this.props;
    const photoItemImageSize = viewPort.width < 600 ? imageSize : '120px';
    const commonStyle = {
      width: photoItemImageSize,
      height: photoItemImageSize,
      lineHeight: photoItemImageSize,
      textAlign: 'center',
      color: 'ivory'
    };
    return (
      <div className={isDragging ? classNames(classes.root, classes.draggingOpacity) : classes.root} style={commonStyle}>
        <Badge
          className={classes.badge}
          badgeContent={
            <span className={classes.badgeRemoveImage} onClick={handleRemoveImageClick}>―</span>
          }>
          <div style={commonStyle}>
            <ImageLoader
              className={classes.image}
              src={photoItem[imageStorage][constants.imageSizes.LARGE]}
              alt="?"
              style={commonStyle} />
              {isOver &&
                <div style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  height: '100%',
                  width: '100%',
                  zIndex: 1,
                  opacity: 0.5,
                  backgroundColor: 'ivory',
                }} />
              }
            </div>
          </Badge>
      </div>
    );
  }
}

PhotoItem.propTypes = {
  classes: PropTypes.object.isRequired,
  photoItem: PropTypes.object.isRequired,
  imageStorage: PropTypes.string.isRequired,
  viewPort: PropTypes.object.isRequired,
  imageSize: PropTypes.string.isRequired,
  handleRemoveImageClick: PropTypes.func.isRequired,
  isDragging: PropTypes.bool,
  isOver: PropTypes.bool
};

export default withStyles(styles)(PhotoItem);
