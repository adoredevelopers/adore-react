import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import AvatarEditor from 'react-avatar-editor';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';

import PhotoItemContainer from '../../../containers/PhotoItemContainer';
import constants from '../../../constants/constants';

import './change-photos-dialog.css';
const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignContent: 'flex-start',
    margin: '-4px',
    paddingTop: '10px'
  },
  containerWithOddNumberItem: {
    justifyContent: 'space-between'
  },
  containerWithSingleItem: {
    justifyContent: 'space-around'
  },
  addImageItem: {
    display: 'flex',
    flexDirection: 'row',
    margin: '10px'
  },
  addImageSymbol: {
    padding: '20px',
    width: '100%',
    height: '100%',
    color: 'gray',
    border: 'dashed 1px black',
    cursor: 'pointer'
  },
  extendedGap: {
    display: 'none'
  },
  chooseImage: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  fileInput: {
    display: 'none'
  },
  button: {
    margin: theme.spacing.unit,
  },
  imageEditor: {
    display: 'flex',
    justifyContent: 'space-around'
  },
  imageEditorControlLabel: {
    lineHeight: '30px'
  },
  imageEditorControlButton: {
    zoom: 0.9
  }
});

const pica = require('pica/dist/pica')();

class ChangePhotosDialog extends Component {
  constructor(props) {
    super(props);
    this.placeholderImage = 'https://moto-vision-5d0bnapfypauqr8mw2qu.netdna-ssl.com/img/p/en-default-thickbox.jpg';
    this.state = {
      viewPort: {
        width: 0,
        height: 0
      },
      imageSize: '0',
      imageEditor: {
        show: false,
        image: this.placeholderImage,
        scale: 1.5,
        scaleDisabled: true,
        rotate: 0,
        rotateDisabled: true,
        uploadButtonDisabled: true
      }
    };
    this.imageEditorDOM = null;
  }

  componentDidMount = () => {
    this.updateViewPortDimensions();
    window.addEventListener('resize', this.updateViewPortDimensions);
  }
  
  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateViewPortDimensions);
  }
  
  updateViewPortDimensions = () => {
    let newState = this.state;
    newState.viewPort.width = window.innerWidth;
    newState.viewPort.height = window.innerHeight;
    newState.imageSize = (newState.viewPort.width - 88) / 2 + 'px';
    this.setState(newState);
  }

  handleAddImageClick = (event) => {
    let imageEditor = Object.assign({}, this.state.imageEditor);
    imageEditor.show = true;
    this.setState({
      imageEditor
    });
  }

  handleRemoveImageClick = (event) => {
    let node = event.currentTarget;
    while (node.getAttribute('class') !== 'photoItem') {
      node = node.parentNode;
    }
    const imageNode = node.querySelector('img');
    const imageSrc = imageNode.currentSrc;
    this.props.removeImage({
      [this.props.imageStorage]: {
        [constants.imageSizes.LARGE]: imageSrc
      }
    });
  }
  
  /* change photos */
  handleCancel = () => {
    this.props.resetImages();
    this.props.onClose();
  }

  handleSubmit = () => {
    this.props.submitImages();
    this.props.onClose();
  }

  /* AvatarEditor functions */
  handleImageEditorFileInputClick = (event) => {
    // clear image file input value so that subsequence onChange event will still fire when pick the same image file
    event.currentTarget.value = null;
  }

  handleImageEditorFileInputChange = (event) => {
    this.setNewImage(event.target.files[0]);
  }

  handleImageEditorDropImage = (event) => {
    event.preventDefault();
    event.stopPropagation();
    this.setNewImage(event.dataTransfer.files[0]);
  }

  setNewImage = (newImage) => {
    if (newImage != null && newImage !== this.placeholderImage) {
      let imageEditor = Object.assign({}, this.state.imageEditor);
      imageEditor.image = newImage;
      imageEditor.scaleDisabled = false;
      imageEditor.rotateDisabled = false;
      imageEditor.uploadButtonDisabled = false;
      this.setState({
        imageEditor
      });
    }
  }

  handleImageEditorScale = (value) => {
    let scale = parseFloat(value);
    let imageEditor = Object.assign({}, this.state.imageEditor);
    imageEditor.scale = scale;
    this.setState({
      imageEditor
    });
  }

  handleImageEditorRotateLeft = (event) => {
    event.preventDefault();
    let imageEditor = Object.assign({}, this.state.imageEditor);
    imageEditor.rotate -= 90;
    this.setState({
      imageEditor
    });
  }

  handleImageEditorRotateRight = (event) => {
    event.preventDefault();
    let imageEditor = Object.assign({}, this.state.imageEditor);
    imageEditor.rotate += 90;
    this.setState({
      imageEditor
    });
  }

  resizeImage = (canvas, size) => {
    const resizeCanvas = document.createElement('canvas');
    resizeCanvas.width = size;
    resizeCanvas.height = size;
    const quality = size < 200 ? 1.0 : 0.8;
    return pica.resize(canvas, resizeCanvas).then(result => pica.toBlob(result, 'image/jpeg', quality));
  }

  buildNewImageItems = (imageBlob, imageSize) => {
    return {
      [imageSize]: imageBlob
    };
  }
  
  applyImage = async () => {
    if (this.imageEditorDOM) {
      this.props.setLoading(true);
      let imageItems = [];
      // This returns a HTMLCanvasElement, it can be made into a data URL or a blob,
      // drawn on another canvas, or added to the DOM.
      const canvas = this.imageEditorDOM.getImage();
      const imageBlob640 = await this.resizeImage(canvas, constants.imageSizes.LARGE);
      imageItems.push(this.buildNewImageItems(imageBlob640, constants.imageSizes.LARGE));
      const imageBlob120 = await this.resizeImage(canvas, constants.imageSizes.SMALL);
      imageItems.push(this.buildNewImageItems(imageBlob120, constants.imageSizes.SMALL));

      // pass resulting resized images to addImage method
      this.props.addImage(imageItems, () => {
        this.props.setLoading(false);
      });
      // If you want the image resized to the canvas size (also a HTMLCanvasElement)
      // const canvasScaled = this.editor.getImageScaledToCanvas();
    }
    this.dismissImageEditor();
  }

  dismissImageEditor = () => {
    this.resetImageEditor();
  }

  resetImageEditor = () => {
    this.setState({
      imageEditor: {
        show: false,
        image: 'https://moto-vision-5d0bnapfypauqr8mw2qu.netdna-ssl.com/img/p/en-default-thickbox.jpg',
        scale: 1.5,
        scaleDisabled: true,
        rotate: 0,
        rotateDisabled: true,
        uploadButtonDisabled: true
      }
    });
  }

  suppressImageEditorActionsForDefaultImage = () => {

  }
  
  logCallback = (event) => {
    console.debug('callback', event)
  }

  render() {
    const {
      classes,
      profileImages,
      imageStorage,
      setImageDragIndex,
      setImageDropIndex,
      addImage,
      removeImage,
      moveImage,
      resetImages,
      setLoading,
      submitImages,
      ...otherProps
    } = this.props;
    return (
      <Dialog
        disableBackdropClick
        disableEscapeKeyDown
        onBackdropClick={this.state.imageEditor.show ? this.dismissImageEditor : this.handleCancel}
        onEscapeKeyDown={this.state.imageEditor.show ? this.dismissImageEditor : this.handleCancel}
        {...otherProps}>
        <DialogTitle>Change Photos</DialogTitle>
        <DialogContent>
          <div
            className={
              this.state.viewPort.width < 600 || profileImages.length === 8 || (profileImages.length + 1) % 4 === 0
              ?
              classNames(classes.container, classes.containerWithOddNumberItem)
              :
              (
                profileImages.length === 0
                ?
                classNames(classes.container, classes.containerWithSingleItem)
                :
                classes.container
              )
            }
            style={this.state.imageEditor.show ? {display: 'none'} : {display: 'flex'}}>
            
            {
              profileImages.map((photoItem, idx) => (
                <PhotoItemContainer
                  photoItem={photoItem}
                  imageStorage={imageStorage}
                  viewPort={this.state.viewPort}
                  imageSize={this.state.imageSize}
                  handleRemoveImageClick={this.handleRemoveImageClick}
                  setImageDragIndex={this.props.setImageDragIndex}
                  setImageDropIndex={this.props.setImageDropIndex}
                  moveImage={this.props.moveImage}
                  key={idx} />
              ), this)
            }

            {
              (() => {
                // show add image button when the total image is less than maximum images allowed
                if (profileImages.length !== constants.MAX_NUMBER_OF_IMAGES) {
                  const imageSize = this.state.viewPort.width < 600 ? this.state.imageSize : '120px';
                  return (
                    <div
                    className={classes.addImageItem}
                    style={{width: imageSize, height: imageSize}}
                    data-dnd-disabled={true}
                    onClick={this.handleAddImageClick}>
                    <AddIcon className={classes.addImageSymbol} data-dnd-disabled={true} />
                  </div>
                  );
                }
              })()
            }
            
          </div>

          <div className={classes.chooseImageEditor} style={this.state.imageEditor.show ? {display: 'block'} : {display: 'none'}}>
            <div className={classes.chooseImage}>
              <input
                id="file"
                type="file"
                multiple={false}
                accept="jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF"
                className={classes.fileInput}
                onClick={this.handleImageEditorFileInputClick}
                onChange={this.handleImageEditorFileInputChange} />
              <label htmlFor="file">
                <Button variant="contained" color="primary" component="span" className={classes.button}>
                  Choose Local Image
                </Button>
              </label>
            </div>
            <div className={classes.imageEditor}>
              <AvatarEditor
                ref={(elem) => {
                  this.imageEditorDOM = elem;
                }}
                image={this.state.imageEditor.image}
                width={this.state.viewPort.width}
                height={this.state.viewPort.width}
                border={this.state.viewPort.width < 600 ? 20 : 60}
                color={[0, 0, 0, 0.25]} // RGBA
                scale={parseFloat(this.state.imageEditor.scale)}
                rotate={parseFloat(this.state.imageEditor.rotate)}
                style={this.state.viewPort.width < 600 ? {width: '100%', height: '100%'} : {width: '50%', height: '50%'}}
                onDropFile={this.handleImageEditorDropImage}
                onImageLoad={this.logCallback.bind(this, 'onImageLoad')}
                onLoadFailure={this.logCallback.bind(this, 'onLoadFailed')}
                onLoadSuccess={this.logCallback.bind(this, 'onLoadSuccess')}
                onImageReady={this.logCallback.bind(this, 'onImageReady')} />
            </div>
            <div>
              <div style={{marginBottom: '10px'}}>
                <span className={classes.imageEditorControlLabel}>Zoom:</span>
                <InputRange
                  name='scale'
                  minValue={1}
                  maxValue={5}
                  step={0.01}
                  value={this.state.imageEditor.scale}
                  disabled={this.state.imageEditor.scaleDisabled}
                  onChange={this.handleImageEditorScale} />
              </div>
              <div style={{display: 'flex', justifyContent: 'space-between'}}>
                <span className={classes.imageEditorControlLabel}>Rotate:</span>
                <div>
                  <Button
                    color="primary"
                    disabled={this.state.imageEditor.rotateDisabled}
                    className={classes.imageEditorControlButton}
                    onClick={this.handleImageEditorRotateLeft}>Left</Button>
                  <Button
                    color="primary"
                    disabled={this.state.imageEditor.rotateDisabled}
                    className={classes.imageEditorControlButton}
                    onClick={this.handleImageEditorRotateRight}>Right</Button>
                </div>
              </div>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <div style={this.state.imageEditor.show ? {display: 'none'} : {display: 'block'}}>
            <Button
              onClick={this.handleCancel}>
              <span>Cancel</span>
            </Button>
            <Button
              color="primary"
              onClick={this.handleSubmit}>
              <span>OK</span>
            </Button>
          </div>
          <div style={this.state.imageEditor.show ? {display: 'block'} : {display: 'none'}}>
            <Button
              onClick={this.dismissImageEditor}>
              <span>Cancel</span>
            </Button>
            <Button
              color="primary"
              disabled={this.state.imageEditor.uploadButtonDisabled}
              onClick={this.applyImage}>
              <span>Add</span>
            </Button>  
          </div>
        </DialogActions>
      </Dialog>
    );
  }
}

ChangePhotosDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  maxWidth: PropTypes.string.isRequired,
  fullScreen: PropTypes.bool.isRequired,
  profileImages: PropTypes.array.isRequired,
  imageStorage: PropTypes.string.isRequired,
  setImageDragIndex: PropTypes.func.isRequired,
  setImageDropIndex: PropTypes.func.isRequired,
  addImage: PropTypes.func.isRequired,
  removeImage: PropTypes.func.isRequired,
  moveImage: PropTypes.func.isRequired,
  resetImages: PropTypes.func.isRequired,
  updateDisplayImages: PropTypes.func,
  submitImages: PropTypes.func,
  setLoading: PropTypes.func,
  onClose: PropTypes.func.isRequired
};

export default withStyles(styles)(ChangePhotosDialog);
