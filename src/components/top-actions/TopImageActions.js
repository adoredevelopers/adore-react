import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';

import ChangePhotosDialogContainer from '../../containers/ChangePhotosDialogContainer';

const styles = theme => ({
  root: {

  },
  changePhotosButton: {
    display: 'none',
    '@media only screen and (min-width: 600px)': {
      display: 'block'
    },
  },
  changePhotosButtonMobile: {
    display: 'block',
    '@media only screen and (min-width: 600px)': {
      display: 'none'
    },
  }
});

class TopImageActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      changePhotosDialog: {
        open: false
      }
    };
  }

  handleChangePhotosButtonClick = (event) => {
    this.setState({
      changePhotosDialog: {
        open: true
      }
    });
  }

  handleRequestClose = (event) => {
    this.setState({
      changePhotosDialog: {
        open: false
      }
    });
  }

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Fab
          color="secondary"
          aria-label="change photos"
          className={classes.changePhotosButton}
          onClick={this.handleChangePhotosButtonClick}>
          <AddAPhotoIcon />
        </Fab>
        <IconButton
          color="secondary"
          aria-label="change photos"
          className={classes.changePhotosButtonMobile}
          onClick={this.handleChangePhotosButtonClick}>
          <AddAPhotoIcon />
        </IconButton>
        <ChangePhotosDialogContainer
          open={this.state.changePhotosDialog.open}
          maxWidth="sm"
          fullScreen={window.innerWidth < 600 ? true : false}
          onClose={this.handleRequestClose} />
      </div>
    );
  }
}

TopImageActions.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object
};

export default withStyles(styles)(TopImageActions);
