import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import Fab from '@material-ui/core/Fab';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import ChatIcon from '@material-ui/icons/Chat';

import navigationLinks from '../../utils/navigationLinks';

const styles = theme => ({
  root: {

  },
  messageInputBox: {
    width: '100%'
  },
  multiLineInput: {

  },
  actionButtons: {
    display: 'flex',
    marginTop: '10px'
  },
  actionButton: {
    marginRight: '10px',
    width: '36px',
    height: '36px'
  },
  gap: {
    flex: '1 1 0'
  },
  messageBoxSendButton: {

  },
  mobileChatButton: {
    width: '50px',
    height: '50px'
  },
  chatIcon: {
    width: '30px',
    height: '30px'
  }
});

class TopMessagingActions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
  }

  handleChange = (event) => {
    this.setState({
      message: event.currentTarget.value
    });
  }

  sendMessage = () => {
    this.props.sendMessage(this.props.userId, this.state.message);
    this.setState({
      message: ''
    });
  }

  getTopMessageActionsContent = (classes) => {
    if (this.props.isMobileView) {
      return (
        <Link to={navigationLinks('USER_MESSAGES_PAGE_ROUTE', this.props.userId)}>
          <IconButton
            color="secondary"
            className={classes.mobileChatButton}>
            <ChatIcon className={classes.chatIcon} />
          </IconButton>
        </Link>
      );
    } else {
      return (
        <div>
          <FormControl className={classes.messageInputBox}>
            <Input
              multiline
              rows={3}
              rowsMax={3}
              className={classes.multiLineInput}
              placeholder="write something..."
              value={this.state.message}
              onChange={this.handleChange} />
          </FormControl>
          <div className={classes.actionButtons}>
            <Fab color="primary" className={classes.actionButton}>
              <AddIcon />
            </Fab>
            <Fab color="secondary" className={classes.actionButton}>
              <EditIcon />
            </Fab>
            <div className={classes.gap} />
            <Button
              variant="contained"
              size="small"
              color="primary"
              className={classes.messageBoxSendButton}
              disabled={this.state.message.trim().length === 0}
              onClick={this.sendMessage}>
              Send
            </Button>
          </div>
        </div>
      );
    }
  }

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        {this.getTopMessageActionsContent(classes)}
      </div>
    );
  }
}

TopMessagingActions.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  isMobileView: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  sendMessage: PropTypes.func.isRequired
};

export default withStyles(styles)(TopMessagingActions);
