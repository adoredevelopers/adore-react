import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  root: {

  },
  card: {
    marginBottom: theme.spacing.unit * 2,
    maxWidth: '450px'
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    })
  },
  expandOpen: {
    transform: 'rotate(180deg)'
  },
  avatar: {
    backgroundColor: red[500]
  },
  image: {
    width: '100%'
  },
  flexGrow: {
    flex: '1 1 0'
  },
});

class ContentSuggestionCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
  }

  handleExpandClick = (event) => {
    this.setState({
      expanded: !this.state.expanded
    });
  };

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <Card elevation={this.props.elevation}>
          <CardHeader
            avatar={<Avatar aria-label="Recipe" className={classes.avatar}>R</Avatar>}
            title="Shrimp and Chorizo Paella"
            subheader="September 14, 2016" />
          <CardMedia
            className={classes.cardMedia}
            image="http://placehold.it/400x198"
            title="Placeholder">
            <img className={classes.image} src="http://placehold.it/400x198" alt="Placeholder" />
          </CardMedia>
          <CardContent>
            <Typography component="p">
              This impressive paella is a perfect party dish and a
              fun meal to cook together with your guests. Add 1 cup
              of frozen peas along with the mussels, if you like.
            </Typography>
          </CardContent>
          <CardActions disableActionSpacing>
            <IconButton>
              <FavoriteIcon />
            </IconButton>
            <IconButton>
              <ShareIcon />
            </IconButton>
            <div className={classes.flexGrow} />
            <IconButton
              className={classNames(classes.expand, {
                [classes.expandOpen]: this.state.expanded,
              })}
              onClick={this.handleExpandClick}>
              <ExpandMoreIcon />
            </IconButton>
          </CardActions>
          <Collapse in={this.state.expanded} transitionDuration="auto" unmountOnExit>
            <CardContent>
              <Typography paragraph variant="body1">Method:</Typography>
              <Typography paragraph>
                Heat 1/2 cup of the broth in a pot until simmering,
                add saffron and set aside for 10 minutes.
              </Typography>
              <Typography paragraph>
                Heat oil in a (14- to 16-inch) paella pan or a large,
                deep skillet over medium-high heat. Add chicken, shrimp
                and chorizo, and cook, stirring occasionally until
                lightly browned, 6 to 8 minutes. Transfer shrimp to a
                large plate and set aside, leaving chicken and chorizo
                in the pan. Add pimentón, bay leaves, garlic, tomatoes,
                onion, salt and pepper, and cook, stirring often until
                thickened and fragrant, about 10 minutes. Add saffron broth
                and remaining 4 1/2 cups chicken broth; bring to a boil.
              </Typography>
              <Typography paragraph>
                Add rice and stir very gently to distribute. Top with
                artichokes and peppers, and cook without stirring, until
                most of the liquid is absorbed, 15 to 18 minutes. Reduce
                heat to medium-low, add reserved shrimp and mussels,
                tucking them down into the rice, and cook again without
                stirring, until mussels have opened and rice is just tender,
                5 to 7 minutes more. (Discard any mussels that don’t open.)
              </Typography>
              <Typography>
                Set aside off of the heat to let rest for 10 minutes, and then serve.
              </Typography>
            </CardContent>
          </Collapse>
        </Card>
      </div>
    );
  }
}

ContentSuggestionCard.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  elevation: PropTypes.number,
  image: PropTypes.string
};

export default withStyles(styles)(ContentSuggestionCard);
