import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import navigationLinks from '../../utils/navigationLinks';
import LogoImage from '../../resources/images/adore_logo@2x.png';

const styles = theme => ({
  root: {

  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  logoImage: {
    marginRight: '10px',
    height: '56px',
    '@media only screen and (min-width: 600px)': {
      height: '64px'
    }
  },
  sectionTitle: {

  },
  barButton: {
    textDecoration: 'none'
  }
});

class RegistrationTopBar extends Component {
  handleLogOutButtonClick = (event) => {
    this.props.performLogOut();
    this.props.history.push(navigationLinks('LOGIN_PAGE_ROUTE'));
  }

  render() {
    const classes = this.props.classes;
    return (
      <AppBar position="static" color="default" className={classes.root}>
        <Toolbar className={classes.toolbar}>
          <img className={classes.logoImage} src={LogoImage} alt="Adore" />
          <Typography className={classes.sectionTitle} variant="h6" color="inherit">
            {this.props.label}
          </Typography>
          {
            this.props.completeProfile
            ?
            (
              <Button size="small" color="secondary" onClick={this.handleLogOutButtonClick}>
                Logout
              </Button>
            )
            :
            (
              <Link to={navigationLinks('LOGIN_PAGE_ROUTE')} className={classes.barButton}>
                <Button size="small" color="secondary">Login</Button>
              </Link>
            )
          }
        </Toolbar>
      </AppBar>
    )
  }
}

RegistrationTopBar.propTypes = {
  classes: PropTypes.object.isRequired,
  label: PropTypes.string,
  completeProfile: PropTypes.bool,
  performLogOut: PropTypes.func.isRequired
};

export default withStyles(styles)(RegistrationTopBar);
