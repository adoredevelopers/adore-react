import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  root: {

  },
  textField: {

  },
  menu: {
    minWidth: '100px'
  }
});

class DataDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      anchorEl: null
    };
  }

  handleClick = (event) => {
    this.setState({
      open: true,
      anchorEl: event.currentTarget
    });
  };

  handleRequestClose = (event) => {
    this.setState({
      open: false,
      anchorEl: null
    });
    if (event.currentTarget.textContent !== '') {
      this.props.updateSelectedData(event.currentTarget.textContent);
    }
  };

  buildMenuItems = () => {
    if (this.props.buildDropdownItems) {
      return this.props.buildDropdownItems();
    } else {
      // build some dummy data here
      let menuItems = [];
      menuItems.push(<MenuItem key="USA" onClick={this.handleRequestClose}>USA</MenuItem>);
      menuItems.push(<MenuItem key="Canada" onClick={this.handleRequestClose}>Canada</MenuItem>);
      return menuItems;
    }
  };

  render() {
    const classes = this.props.classes;
    return (
      <div style={Object.assign({}, this.props.style)} className={classes.root}>
        <TextField
          className={classes.textField}
          value={this.props.selectedData}
          aria-owns="simple-menu"
          aria-haspopup="true"
          onClick={this.handleClick} />
        <Menu
          className={classes.menu}
          anchorEl={this.state.anchorEl}
          open={this.state.open}
          onClose={this.handleRequestClose}>
          {this.buildMenuItems()}
        </Menu>
      </div>
    );
  }
}

DataDropdown.propTypes = {
  classes: PropTypes.object.isRequired,
  style: PropTypes.object,
  buildDropdownItems: PropTypes.func,
  selectedData: PropTypes.string,
  updateSelectedData: PropTypes.func
};

export default withStyles(styles)(DataDropdown);
