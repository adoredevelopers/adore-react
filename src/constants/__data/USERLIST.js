export default USERLIST = [
  { 
    "_id" : ObjectId("550f5c0bd53aac0000ec8734"), 
    "__v" : NumberInt(0), 
    "account_status" : "Active", 
    "account_type" : "Normal", 
    "display_image_urls" : [
      "yolandawang1013@gmail.com/7676cd6626139580979a12ebce3d7291.png", 
      "yolandawang1013@gmail.com/7623fc50fead6d0ce4f987bc9e02d8e3.png", 
      "yolandawang1013@gmail.com/51fc30620b17b484cdfc2d7d18a0ed9f.png", 
      "yolandawang1013@gmail.com/f9f3347dad4465722b1b0a51a9215269.png", 
      "yolandawang1013@gmail.com/upload_21c7a530f910f1d1a4b10dd03c9a3239.jpg", 
      "yolandawang1013@gmail.com/upload_3631a7619bbcc3731d37d8d448e68488.jpg"
    ], 
    "email" : "chaolu.dev@gmail.com", 
    "last_activity" : ISODate("2016-12-28T03:12:30.291-0400"), 
    "last_updated_timestamp" : 1441514306578.0, 
    "loc" : {
      "type" : "Point", 
      "coordinates" : [
        -79.389964, 
        43.652783
      ]
    }, 
    "misc" : {}, 
    "once_connected_up_for_activities" : [
      "1", 
      "2", 
      "3", 
      "4", 
      "5", 
      "6", 
      "7", 
      "8", 
      "9", 
      "10", 
      "11"
    ], 
    "preference" : {
      "basic": {
        "name" : "Chao Lu",
        "gender" : "Male", 
        "birthday" : ISODate("1988-07-04T14:00:00.000-0400"), 
        "c_zodiac" : NumberInt(4), 
        "zodiac" : NumberInt(1), 
        "height" : NumberInt(170), 
        "body_type" : NumberInt(6), 
        "martial_status": NumberInt(0),
        "has_children": false,
        "languages" : [

        ], 
        "residence_place" : {
          "residence_city" : "", 
          "residence_province" : "", 
          "residence_country" : ""
        }, 
        "postal_code": "L4L 7M6",
        "willing_to_relocate": false
      },
      "background": {
        "highest_education": NumberInt(3),
        "occupation" : null, 
        "income": NumberInt(2),
        "religion": NumberInt(1),
        "ethnicities": "Chinese (Han)",
        "birth_place" : null, 
        "residential_status" : "Citizen", 
        "citizenship": "Canada",
        "age_arrived": null
      },
      "lifestyle": {
        "smoking": "Never",
        "drinking": "Socially",
        "interests": []
      }
    },
    "profile" : {
      "basic": {
        "name" : "Chao Lu",
        "gender" : "Male", 
        "birthday" : ISODate("1988-07-04T14:00:00.000-0400"), 
        "c_zodiac" : NumberInt(4), 
        "zodiac" : NumberInt(1), 
        "height" : NumberInt(170), 
        "body_type" : NumberInt(6), 
        "martial_status": NumberInt(0),
        "has_children": false,
        "languages" : [

        ], 
        "residence_place" : {
          "residence_city" : "", 
          "residence_province" : "", 
          "residence_country" : ""
        }, 
        "postal_code": "L4L 7M6",
        "willing_to_relocate": false
      },
      "background": {
        "highest_education": NumberInt(3),
        "bachelor_institution": "McMaster University",
        "graduate_institution": null,
        "occupation" : "Software Engineer", 
        "company": "Perficient Canada LTD.",
        "job_title": "Technical Consultant",
        "income": NumberInt(2),
        "religion": NumberInt(1),
        "ethnicities": "Chinese (Han)",
        "birth_place" : {
          "birth_city" : "太原", 
          "birth_province" : "山西", 
          "birth_country" : "中国"
        }, 
        "residential_status" : "Citizen", 
        "citizenship": "Canada",
        "age_arrived": NumberInt(15)
      },
      "lifestyle": {
        "smoking": "Never",
        "drinking": "Socially",
        "placed_traveled": [
          "New York",
          "Philidelphia",
          "Boston",
          "Chicago",
          "Washingon D.C.",
          "Azores",
          "Panama"
        ],
        "interests": [
          "swimming",
          "kayaking",
          "cycling",
          "snowboarding",
          "hiking",
          "badminton",
          "table-tennis",
          "workout/weight training",
          "guitar",
          "french horn"
        ]
      }
    }, 
    "rand_key" : NumberInt(29259666), 
    "stats" : {
      "number_of_admirers" : NumberInt(3), 
      "number_of_adorers" : NumberInt(0), 
      "number_of_visits" : NumberInt(31)
    }, 
    "creation_date" : ISODate("2015-05-15T22:38:39.982-0400"), 
    "display_image_verified" : true
  }
];
