import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import HeaderContainer from '../../containers/HeaderContainer';
import Footer from '../../components/footer/Footer';
import LogoImage from '../../resources/images/adore_logo@3x.png';
import navigationLinks from '../../utils/navigationLinks';

import './pageTemplate.css';
const styles = theme => ({
  root: {

  },
  pageTemplate: {
    maxWidth: '1024px',
    marginLeft: 'auto',
    marginRight: 'auto'
  }
});

const mapStateToProps = state => ({
  sessionStates: state.sessionStates
});

const mapDispatchToProps = dispatch => ({
});

class PageTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    if (this.props.sessionStates.isLoggedOn) {
      return (
        <Redirect to={navigationLinks('REDIRECT_ROUTE')} />
      );
    } else {
      const classes = this.props.classes;
      return (
        <div className={classes.root}>
          <HeaderContainer appBarPositionFixed={true} hasLeftDrawer={true} />

          <div id="pageTemplate" className={classNames(classes.userAdoresPage, 'page-content')}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <div className={classes.logo}>
                  <img className={classes.logoImage} src={LogoImage} alt="Adore" />
                </div>
              </Grid>
            </Grid>
          </div>
          
          <Footer />
        </div>
      );
    }
  }
}

PageTemplate.propTypes = {
  classes: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(PageTemplate));
