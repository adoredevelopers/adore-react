const constants = {
  thirdPartyServices: {
    geocoding: 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBIYsAC03TvNi1HWZhPxAsTgXT84YLM7JI&address='
  },
  MAX_NUMBER_OF_IMAGES: 8,
  DEFAULT_DISPLAY_IMAGE: 'https://index.co/img/avatar.default.png',
  imageSizes: {
    SMALL: 120,
    LARGE: 640,
  },
  dnd: {
    "itemTypes": {
      "PROFILE_IMAGE": 'profile image'
    }
  }
};

export default constants;
