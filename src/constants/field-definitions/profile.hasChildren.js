export default (fieldType='radioField') => {
  return {
    fieldId: 'hasChildren',
    fieldPath: 'profile.basic',
    fieldLabel: 'Has Children',
    fieldType: fieldType,
    fieldValueType: 'boolean',
    fieldValues: [
      {
        label: 'Yes',
        value: true
      },
      {
        label: 'No',
        value: false
      }
    ],
    isRequired: true
  };
};
