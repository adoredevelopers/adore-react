export default (fieldType='textAreaField') => {
  return {
    fieldId: 'qualitiesIAdore',
    fieldPath: 'introductions',
    fieldLabel: 'Qualities I adore',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
