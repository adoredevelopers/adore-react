export default (fieldType='textField', fieldSubType='dateTextField') => {
  return {
    fieldId: 'birthday',
    fieldPath: 'profile.basic',
    fieldLabel: 'Birthday',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'date',
    isRequired: true
  };
};
