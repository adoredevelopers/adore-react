export default (fieldType='selectionField') => {
  return {
    fieldId: 'ethnicities',
    fieldPath: 'profile.background',
    fieldLabel: 'Ethnicities',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [

    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
