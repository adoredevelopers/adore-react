export default (fieldType='textField', fieldSubType='emailTextField') => {
  return {
    fieldId: 'email',
    fieldPath: '',
    fieldLabel: 'Email',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    isRequired: true,
    isDisabled: false
  };
};
