export default (fieldType='textAreaField') => {
  return {
    fieldId: 'howFriendsDiscribeMe',
    fieldPath: 'introductions',
    fieldLabel: 'How friends discribe me',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
