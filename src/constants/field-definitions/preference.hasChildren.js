export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'hasChildren',
    fieldPath: 'preference',
    fieldLabel: 'Has Children',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'boolean',
    fieldValues: [
      {
        label: 'Yes',
        value: true
      },
      {
        label: 'No',
        value: false
      }
    ],
    isRequired: false
  };
};
