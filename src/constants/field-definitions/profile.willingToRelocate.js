export default (fieldType='radioField') => {
  return {
    fieldId: 'willingToRelocate',
    fieldPath: 'profile.basic',
    fieldLabel: 'Willing To Relocate',
    fieldType: fieldType,
    fieldValueType: 'boolean',
    fieldValues: [
      {
        label: 'Yes',
        value: true
      },
      {
        label: 'No',
        value: false
      }
    ],
    isRequired: false
  };
};
