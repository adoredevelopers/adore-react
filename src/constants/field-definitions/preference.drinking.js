export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'drinking',
    fieldPath: 'preference',
    fieldLabel: 'Drinking',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
      {
          label: 'Never',
          value: 'Never'
      },
      {
          label: 'Socially',
          value: 'Socially'
      },
      {
          label: 'Often',
          value: 'Often'
      }
    ],
    isRequired: false
  };
};
