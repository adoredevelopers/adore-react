export default (fieldType='selectionField') => {
  return {
    fieldId: 'educationInstitute',
    fieldPath: 'profile.background',
    fieldLabel: 'Education Institute',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      
    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
