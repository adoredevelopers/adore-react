export default (fieldType='selectionField') => {
  return {
    fieldId: 'religion',
    fieldPath: 'profile.background',
    fieldLabel: 'Religion',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Non Religious',
        value: 'Non Religious'
      },
      {
        label: 'Atheism',
        value: 'Atheism'
      },
      {
        label: 'Buddhism',
        value: 'Buddhism'
      },
      {
        label: 'Christianity',
        value: 'Christianity'
      },
      {
        label: 'Hinduism',
        value: 'Hinduism'
      },
      {
        label: 'Islam',
        value: 'Islam'
      },
      {
        label: 'Other Religions',
        value: 'Other Religions'
      }
    ],
    isRequired: false
  };
};
