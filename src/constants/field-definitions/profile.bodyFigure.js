export default (fieldType='selectionField') => {
  return {
    fieldId: 'bodyFigure',
    fieldPath: 'profile.basic',
    fieldLabel: 'Body Figure',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Not Disclosed',
        value: 'Not Disclosed'
      },
      {
        label: 'Athletic',
        value: 'Athletic'
      },
      {
        label: 'Big',
        value: 'Big'
      },
      {
        label: 'Built',
        value: 'Built'
      },
      {
        label: 'Culvy',
        value: 'Culvy'
      },
      {
        label: 'Heavy',
        value: 'Heavy'
      },
      {
        label: 'Husky',
        value: 'Husky'
      },
      {
        label: 'Large',
        value: 'Large'
      },
      {
        label: 'Lean',
        value: 'Lean'
      },
      {
        label: 'Muscular',
        value: 'Muscular'
      },
      {
        label: 'Plump',
        value: 'Plump'
      },
      {
        label: 'Ripped',
        value: 'Ripped'
      },
      {
        label: 'Skinny',
        value: 'Skinny'
      },
      {
        label: 'Slender',
        value: 'Slender'
      },
      {
        label: 'Slim',
        value: 'Slim'
      },
      {
        label: 'Small',
        value: 'Small'
      },
      {
        label: 'Thick',
        value: 'Thick'
      },
      {
        label: 'Thin',
        value: 'Thin'
      },
      {
        label: 'Tiny',
        value: 'Tiny'
      }
    ],
    isRequired: false,
    isCreatable: true
  };
};
