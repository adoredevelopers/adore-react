export default (fieldType='textAreaField') => {
  return {
    fieldId: 'peoplesFirstImpressionOfMe',
    fieldPath: 'introductions',
    fieldLabel: 'People\'s first impression of me',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
