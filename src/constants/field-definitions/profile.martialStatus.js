export default (fieldType='selectionField') => {
  return {
    fieldId: 'martialStatus',
    fieldPath: 'profile.basic',
    fieldLabel: 'Martial Status',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Never married',
        value: 'Never married'
      },
      {
        label: 'Divorced',
        value: 'Divorced'
      },
      {
        label: 'Widowed',
        value: 'Widowed'
      },
      {
        label: 'Separated',
        value: 'Separated'
      }
    ],
    isRequired: true
  };
};
