export default (fieldType='selectionField') => {
  return {
    fieldId: 'height',
    fieldPath: 'profile.basic',
    fieldLabel: 'Height (cm)',
    fieldType: fieldType,
    fieldValueType: 'number',
    fieldValues: [
      {
        label: '< 150',
        value: 150
      },
      {
        label: '151',
        value: 151
      },
      {
        label: '152',
        value: 152
      },
      {
        label: '153',
        value: 153
      },
      {
        label: '154',
        value: 154
      },
      {
        label: '155',
        value: 155
      },
      {
        label: '156',
        value: 156
      },
      {
        label: '157',
        value: 157
      },
      {
        label: '158',
        value: 158
      },
      {
        label: '159',
        value: 159
      },
      {
        label: '160',
        value: 160
      },
      {
        label: '161',
        value: 161
      },
      {
        label: '162',
        value: 162
      },
      {
        label: '163',
        value: 163
      },
      {
        label: '164',
        value: 164
      },
      {
        label: '165',
        value: 165
      },
      {
        label: '166',
        value: 166
      },
      {
        label: '167',
        value: 167
      },
      {
        label: '168',
        value: 168
      },
      {
        label: '169',
        value: 169
      },
      {
        label: '170',
        value: 170
      },
      {
        label: '171',
        value: 171
      },
      {
        label: '172',
        value: 172
      },
      {
        label: '173',
        value: 173
      },
      {
        label: '174',
        value: 174
      },
      {
        label: '175',
        value: 175
      },
      {
        label: '176',
        value: 176
      },
      {
        label: '177',
        value: 177
      },
      {
        label: '178',
        value: 178
      },
      {
        label: '179',
        value: 179
      },
      {
        label: '180',
        value: 180
      },
      {
        label: '181',
        value: 181
      },
      {
        label: '182',
        value: 182
      },
      {
        label: '183',
        value: 183
      },
      {
        label: '184',
        value: 184
      },
      {
        label: '185',
        value: 185
      },
      {
        label: '186',
        value: 186
      },
      {
        label: '187',
        value: 187
      },
      {
        label: '188',
        value: 188
      },
      {
        label: '189',
        value: 189
      },
      {
        label: '190',
        value: 190
      },
      {
        label: '191',
        value: 191
      },
      {
        label: '192',
        value: 192
      },
      {
        label: '193',
        value: 193
      },
      {
        label: '194',
        value: 194
      },
      {
        label: '>195',
        value: 195
      }
    ],
    isRequired: true
  };
};
