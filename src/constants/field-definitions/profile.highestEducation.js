export default (fieldType='selectionField') => {
  return {
    fieldId: 'highestEducation',
    fieldPath: 'profile.background',
    fieldLabel: 'Highest Education',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Professional',
        value: 'Professional'
      },
      {
        label: 'Doctorate',
        value: 'Doctorate'
      },
      {
        label: 'Master',
        value: 'Master'
      },
      {
        label: 'Bachelor',
        value: 'Bachelor'
      },
      {
        label: 'Some College',
        value: 'Some College'
      },
      {
        label: 'High School',
        value: 'High School'
      },
      {
        label: 'Other Educations',
        value: 'Other Educations'
      }
    ],
    isRequired: false
  };
};
