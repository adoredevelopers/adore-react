export default (fieldType='textAreaField') => {
  return {
    fieldId: 'whatIEnjoyInLife',
    fieldPath: 'introductions',
    fieldLabel: 'What I enjoy in life',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
