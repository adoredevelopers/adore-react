export default (fieldType='selectionField') => {
  return {
    fieldId: 'company',
    fieldPath: 'profile.background',
    fieldLabel: 'Company',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      
    ],
    isRequired: false,
    isCreatable: true
  };
};
