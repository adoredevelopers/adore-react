export default (fieldType='selectionField') => {
  return {
    fieldId: 'smoking',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Smoking',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Never',
        value: 'Never'
      },
      {
        label: 'Sometimes',
        value: 'Sometimes'
      },
      {
        label: 'Often',
        value: 'Often'
      }
    ],
    isRequired: false
  };
};
