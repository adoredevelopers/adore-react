export default (fieldType='textAreaField') => {
  return {
    fieldId: 'topicsILoveToDiscuss',
    fieldPath: 'introductions',
    fieldLabel: 'Topics I love to discuss',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
