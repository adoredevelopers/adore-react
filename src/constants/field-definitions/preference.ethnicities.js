export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'ethnicities',
    fieldPath: 'preference',
    fieldLabel: 'Ethnicities',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
        
    ],
    isRequired: false
  };
};
