export default (fieldType='selectionField') => {
  return {
    fieldId: 'industry',
    fieldPath: 'profile.background',
    fieldLabel: 'Industry',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [

    ],
    isRequired: false,
    isCreatable: true
  };
};
