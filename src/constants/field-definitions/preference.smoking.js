export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'smoking',
    fieldPath: 'preference',
    fieldLabel: 'Smoking',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Never',
        value: 'Never'
      },
      {
        label:'Sometimes',
        value: 'Sometimes'
      },
      {
        label: 'Often',
        value: 'Often'
      }
    ],
    isRequired: false
  };
};
