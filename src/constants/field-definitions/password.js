export default (fieldType='textField', fieldSubType='passwordTextField') => {
  return {
    fieldId: 'password',
    fieldPath: '',
    fieldLabel: 'Password',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    isRequired: true
  };
};
