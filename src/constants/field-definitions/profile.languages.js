export default (fieldType='selectionField') => {
  return {
    fieldId: 'languages',
    fieldPath: 'profile.basic',
    fieldLabel: 'Languages',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Arabic',
        value: 'Arabic'
      },
      {
        label: 'Awadhi',
        value: 'Awadhi'
      },
      {
        label: 'Azerbaijani',
        value: 'Azerbaijani'
      },
      {
        label: 'Bengali',
        value: 'Bengali'
      },
      {
        label: 'Bhojpuri',
        value: 'Bhojpuri'
      },
      {
        label: 'Burmese',
        value: 'Burmese'
      },
      {
        label: 'Chinese (Mandarin)',
        value: 'Chinese (Mandarin)'
      },
      {
        label: 'Chinese (Cantonese)',
        value: 'Chinese (Cantonese)'
      },
      {
        label: 'Dutch',
        value: 'Dutch'
      },
      {
        label: 'English',
        value: 'English'
      },
      {
        label: 'French',
        value: 'French'
      },
      {
        label: 'German',
        value: 'German'
      },
      {
        label: 'Gujarati',
        value: 'Gujarati'
      },
      {
        label: 'Hausa',
        value: 'Hausa'
      },
      {
        label: 'Hindi',
        value: 'Hindi'
      },
      {
        label: 'Italian',
        value: 'Italian'
      },
      {
        label: 'Japanese',
        value: 'Japanese'
      },
      {
        label: 'Javanese',
        value: 'Javanese'
      },
      {
        label: 'Kannada',
        value: 'Kannada'
      },
      {
        label: 'Korean',
        value: 'Korean'
      },
      {
        label: 'Maithili',
        value: 'Maithili'
      },
      {
        label: 'Malayalam',
        value: 'Malayalam'
      },
      {
        label: 'Marathi',
        value: 'Marathi'
      },
      {
        label: 'Oriya',
        value: 'Oriya'
      },
      {
        label: 'Panjabi',
        value: 'Panjabi'
      },
      {
        label: 'Persian',
        value: 'Persian'
      },
      {
        label: 'Polish',
        value: 'Polish'
      },
      {
        label: 'Portuguese',
        value: 'Portuguese'
      },
      {
        label: 'Romanian',
        value: 'Romanian'
      },
      {
        label: 'Russian',
        value: 'Russian'
      },
      {
        label: 'Serbo-Croatian',
        value: 'Serbo-Croatian'
      },
      {
        label: 'Sindhi',
        value: 'Sindhi'
      },
      {
        label: 'Spanish',
        value: 'Spanish'
      },
      {
        label: 'Sunda',
        value: 'Sunda'
      },
      {
        label: 'Tamil',
        value: 'Tamil'
      },
      {
        label: 'Telugu',
        value: 'Telugu'
      },
      {
        label: 'Thai',
        value: 'Thai'
      },
      {
        label: 'Turkish',
        value: 'Turkish'
      },
      {
        label: 'Ukrainian',
        value: 'Ukrainian'
      },
      {
        label: 'Urdu',
        value: 'Urdu'
      },
      {
        label: 'Vietnamese',
        value: 'Vietnamese'
      },
      {
        label: 'Yoruba',
        value: 'Yoruba'
      },
      {
        label: 'Other Languages',
        value: 'Other Languages'
      }
    ],
    isRequired: true,
    isMulti: true
  };
};
