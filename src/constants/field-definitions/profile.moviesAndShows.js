export default (fieldType='selectionField') => {
  return {
    fieldId: 'moviesAndShows',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Movies and Shows',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      
    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
