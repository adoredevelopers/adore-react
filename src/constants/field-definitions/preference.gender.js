export default (fieldType='checkboxField') => {
  return {
    fieldId: 'gender',
    fieldPath: 'preference',
    fieldLabel: 'Gender',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Male',
        value: false
      },
      {
        label: 'Female',
        value: false
      }
    ],
    isRequired: false
  };
};
