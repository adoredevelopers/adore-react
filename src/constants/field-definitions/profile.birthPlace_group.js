export default (fieldType='selectionField') => {
  return {
    fieldType: 'comboSelectionField',
    itemFields: [
      {
        fieldId: 'country',
        fieldPath: 'profile.background.birthPlace',
        fieldLabel: 'Country/Region',
        fieldType: fieldType,
        fieldValueType: 'string',
        fieldValues: [
    
        ],
        isRequired: false
      },
      {
        fieldId: 'province',
        fieldPath: 'profile.background.birthPlace',
        fieldLabel: 'Province/State',
        fieldType: fieldType,
        fieldValueType: 'string',
        fieldValues: [
    
        ],
        isRequired: false
      },
      {
        fieldId: 'city',
        fieldPath: 'profile.background.birthPlace',
        fieldLabel: 'City',
        fieldType: fieldType,
        fieldValueType: 'string',
        fieldValues: [
    
        ],
        isRequired: false
      }
    ]
  };
};
