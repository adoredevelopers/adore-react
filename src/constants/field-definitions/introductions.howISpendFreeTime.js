export default (fieldType='textAreaField') => {
  return {
    fieldId: 'howISpendFreeTime',
    fieldPath: 'introductions',
    fieldLabel: 'How I spend free time',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
