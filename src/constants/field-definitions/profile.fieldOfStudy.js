export default (fieldType='selectionField') => {
  return {
    fieldId: 'fieldOfStudy',
    fieldPath: 'profile.background',
    fieldLabel: 'Field Of Study',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Arts',
        value: 'Arts'
      },
      {
        label: 'Education',
        value: 'Education'
      },
      {
        label: 'Engineering',
        value: 'Engineering'
      },
      {
        label: 'Information',
        value: 'Information'
      },
      {
        label: 'Management',
        value: 'Management'
      },
      {
        label: 'Science',
        value: 'Science'
      },
      {
        label: 'Medicine',
        value: 'Medicine'
      },
      {
        label: 'Dentistry',
        value: 'Dentistry'
      },
      {
        label: 'Pharmacy',
        value: 'Pharmacy'
      },
      {
        label: 'Law',
        value: 'Law'
      }
    ],
    isRequired: false,
    isCreatable: true
  };
};
