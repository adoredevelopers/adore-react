export default (fieldType='selectionField') => {
  return {
    fieldId: 'music',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Music',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [

    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
