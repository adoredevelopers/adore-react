import axios from 'axios';

import constants from '../constants';
import residenceGroupField from '../../constants/field-definitions/profile.residence_group';

export default (fieldType='textField') => {
  const fieldId = 'postalCode';
  return {
    fieldId,
    fieldPath: 'profile.basic',
    fieldLabel: 'Postal/Zip Code',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldOnBlur: async (values) => {
      console.log();
      console.log(values);
      if (values != null && values.length > 0) {
        try {
          const response = await axios.get(constants.thirdPartyServices.geocoding + values[fieldId]);
          console.debug(response);
          if (response.data.results.length > 0) {
            let location = {};
            const addressComponents = response.data.results[0].address_components;
            addressComponents.forEach(function(addressComponent, idx) {
              const types = addressComponent.types;
              if (types.indexOf('country') !== -1) {
                location.country = addressComponent.long_name;
                values.country = {
                  label: addressComponent.long_name,
                  value: addressComponent.long_name
                };
              } else if (types.indexOf('administrative_area_level_1') !== -1) {
                location.province = addressComponent.short_name;
                values.province = {
                  label: addressComponent.short_name,
                  value: addressComponent.short_name
                };
              } else if (types.indexOf('locality') !== -1) {
                location.city = addressComponent.short_name;
                values.city = {
                  label: addressComponent.short_name,
                  value: addressComponent.short_name
                };
              } else {}
            });
            console.debug('location: ');
            console.debug(location);
          }
        } catch (error) {
          console.debug(error);
        }
      }
    },
    isRequired: true
  };
};
