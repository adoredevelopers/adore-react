export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'qualities',
    fieldPath: 'preference',
    fieldLabel: 'Qualities',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [

    ],
    isRequired: false
  };
};
