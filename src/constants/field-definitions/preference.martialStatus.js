export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'martialStatus',
    fieldPath: 'preference',
    fieldLabel: 'Martial Status',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Never married',
        value: 'Never married'
      },
      {
        label: 'Divorced',
        value: 'Divorced'
      },
      {
        label: 'Widowed',
        value: 'Widowed'
      },
      {
        label: 'Separated',
        value: 'Separated'
      }
    ],
    isRequired: false
  };
};
