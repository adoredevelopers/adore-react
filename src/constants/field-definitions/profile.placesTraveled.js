export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'placesTraveled',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Places Traveled',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
        
    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
