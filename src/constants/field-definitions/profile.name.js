export default (fieldType='textField') => {
  return {
    fieldId: 'name',
    fieldPath: 'profile.basic',
    fieldLabel: 'Name',
    fieldType: fieldType,
    fieldValueType: 'string',
    isRequired: true
  };
};
