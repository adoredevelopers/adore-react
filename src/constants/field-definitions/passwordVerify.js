export default (fieldType='textField', fieldSubType='passwordTextField') => {
  return {
    fieldId: 'passwordVerify',
    fieldPath: '',
    fieldLabel: 'Password Verify',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    isRequired: true
  };
};
