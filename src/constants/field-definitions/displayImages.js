export default (fieldType='imageField') => {
  return {
    fieldId: 'displayImages',
    fieldPath: '',
    fieldLabel: 'Display Images',
    fieldType: fieldType,
    fieldValueType: 'string',
    isRequired: true
  };
};
