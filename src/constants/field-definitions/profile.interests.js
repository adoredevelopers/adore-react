export default (fieldType='selectionField') => {
  return {
    fieldId: 'interests',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Interests',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
        {
          label: 'Cooking',
          value: 'Cooking'
        },
        {
          label: 'Exercise',
          value: 'Exercise'
        },
        {
          label: 'Food',
          value: 'Food'
        },
        {
          label: 'Movies',
          value: 'Movies'
        },
        {
          label: 'Music',
          value: 'Music'
        },
        {
          label: 'Outdoor',
          value: 'Outdoor'
        },
        {
          label: 'Pets',
          value: 'Pets'
        },
        {
          label: 'Photography',
          value: 'Photography'
        },
        {
          label: 'Politics',
          value: 'Politics'
        },
        {
          label: 'Reading',
          value: 'Reading'
        },
        {
          label: 'Sports',
          value: 'Sports'
        },
        {
          label: 'Traveling',
          value: 'Traveling'
        }
    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
