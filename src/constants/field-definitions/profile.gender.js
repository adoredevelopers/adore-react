export default (fieldType='radioField') => {
  return {
    fieldId: 'gender',
    fieldPath: 'profile.basic',
    fieldLabel: 'Gender',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Male',
        value: 'Male'
      },
      {
        label: 'Female',
        value: 'Female'
      }
    ],
    isRequired: true
  };
};
