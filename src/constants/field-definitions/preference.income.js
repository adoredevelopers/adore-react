export default (fieldType='selectionField') => {
  return {
    fieldId: 'income',
    fieldPath: 'preference',
    fieldLabel: 'Income (USD/year)',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Do Not Care',
        value: ''
      },
      {
        label: 'Less than $50K',
        value: 'Less than $50K'
      },
      {
        label: '$50K - 75K',
        value: '$50K - 75K'
      },
      {
        label: '$75K - 100K',
        value: '$75K - 100K'
      },
      {
        label: '$100K - 150K',
        value: '$100K - 150K'
      },
      {
        label: '$150K - 300K',
        value: '$150K - 300K'
      },
      {
        label: '$300K or more',
        value: '$300K or more'
      }
    ],
    isRequired: false
  };
};
