export default (fieldType='textAreaField') => {
  return {
    fieldId: 'introducingMyself',
    fieldPath: 'introductions',
    fieldLabel: 'Introducing myself',
    fieldType: fieldType,
    fieldRows: 6,
    fieldValueType: 'string',
    fieldValuePlaceholder: 'placeholder text',
    isRequired: false
  };
};
