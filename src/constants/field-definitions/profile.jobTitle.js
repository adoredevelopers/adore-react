export default (fieldType='selectionField') => {
  return {
    fieldId: 'jobTitle',
    fieldPath: 'profile.background',
    fieldLabel: 'Job Title',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [

    ],
    isRequired: false,
    isCreatable: true
  };
};
