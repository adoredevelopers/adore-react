export default (fieldType='comboSelectionField') => {
  const itemFieldType = 'selectionField';
  return {
    fieldType: fieldType,
    itemFields: [
      {
        fieldId: 'country',
        fieldPath: 'profile.basic',
        fieldLabel: 'Country/Region',
        fieldType: itemFieldType,
        fieldValueType: 'string',
        fieldValues: [],
        isRequired: false,
        isDisabled: false,
        isCreatable: true
      },
      {
        fieldId: 'province',
        fieldPath: 'profile.basic',
        fieldLabel: 'Province/State',
        fieldType: itemFieldType,
        fieldValueType: 'string',
        fieldValues: [],
        isRequired: false,
        isDisabled: false,
        isCreatable: true
      },
      {
        fieldId: 'city',
        fieldPath: 'profile.basic',
        fieldLabel: 'City',
        fieldType: itemFieldType,
        fieldValueType: 'string',
        fieldValues: [],
        isRequired: false,
        isDisabled: false,
        isCreatable: true
      }
    ]
  };
};
