export default (fieldType='selectionField', fieldSubType='multiSelectionField') => {
  return {
    fieldId: 'education',
    fieldPath: 'preference',
    fieldLabel: 'Education',
    fieldType: fieldType,
    fieldSubType: fieldSubType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Professional',
        value: 'Professional'
      },
      {
        label: 'Doctorate',
        value: 'Doctorate'
      },
      {
        label: 'Master',
        value: 'Master'
      },
      {
        label: 'Bachelor',
        value: 'Bachelor'
      },
      {
        label: 'Some College',
        value: 'Some College'
      },
      {
        label: 'High School',
        value: 'High School'
      },
      {
        label: 'Other Educations',
        value: 'Other Educations'
      }
    ],
    isRequired: false
  };
};
