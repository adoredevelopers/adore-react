export default (fieldType='selectionField') => {
  return {
    fieldId: 'books',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Books',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      
    ],
    isRequired: false,
    isMulti: true,
    isCreatable: true
  };
};
