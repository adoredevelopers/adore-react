export default (fieldType='selectionField') => {
  return {
    fieldId: 'drinking',
    fieldPath: 'profile.lifestyle',
    fieldLabel: 'Drinking',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
          label: 'Never',
          value: 'Never'
      },
      {
          label: 'Socially',
          value: 'Socially'
      },
      {
          label: 'Often',
          value: 'Often'
      }
    ],
    isRequired: false
  };
};
