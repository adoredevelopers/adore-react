export default (fieldType='selectionField') => {
  return {
    fieldId: 'residentialStatus',
    fieldPath: 'profile.basic',
    fieldLabel: 'Residential Status',
    fieldType: fieldType,
    fieldValueType: 'string',
    fieldValues: [
      {
        label: 'Student Visa',
        value: 'Student Visa'
      },
      {
        label: 'Work Visa',
        value: 'Work Visa'
      },
      {
        label: 'Permanent Resident',
        value: 'Permanent Resident'
      },
      {
        label: 'Citizen',
        value: 'Citizen'
      }
    ],
    isRequired: false
  };
};
