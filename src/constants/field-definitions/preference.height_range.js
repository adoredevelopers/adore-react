export default (fieldType='textField', fieldSubType='numberTextField') => {
  return {
    fieldType: 'comboTextField',
    itemFields: [
      {
        fieldId: 'minHeight',
        fieldPath: 'preference',
        fieldLabel: 'Min Height (cm)',
        fieldType: fieldType,
        fieldSubType: fieldSubType,
        fieldValueType: 'number',
        isRequired: false
      },
      {
        fieldId: 'maxHeight',
        fieldPath: 'preference',
        fieldLabel: 'Max Height (cm)',
        fieldType: fieldType,
        fieldSubType: fieldSubType,
        fieldValueType: 'number',
        isRequired: false
      }
    ]
  };
};
