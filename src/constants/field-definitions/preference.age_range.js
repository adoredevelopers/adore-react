export default (fieldType='textField', fieldSubType='numberTextField') => {
  return {
    fieldType: 'comboTextField',
    itemFields: [
      {
        fieldId: 'minAge',
        fieldPath: 'preference',
        fieldLabel: 'Min Age',
        fieldType: fieldType,
        fieldSubType: fieldSubType,
        fieldValueType: 'number',
        isRequired: false
      },
      {
        fieldId: 'maxAge',
        fieldPath: 'preference',
        fieldLabel: 'Max Age',
        fieldType: fieldType,
        fieldSubType: fieldSubType,
        fieldValueType: 'number',
        isRequired: false
      }
    ]
  };
};;
