import introducingMyselfField from '../field-definitions/introductions.introducingMyself';
import howISpendFreeTimeField from '../field-definitions/introductions.howISpendFreeTime';
import peoplesFirstImpressionOfMeField from '../field-definitions/introductions.peoplesFirstImpressionOfMe';
import howFriendsDiscribeMeField from '../field-definitions/introductions.howFriendsDiscribeMe';
import whatIEnjoyInLifeField from '../field-definitions/introductions.whatIEnjoyInLife';
import topicsILoveToDiscussField from '../field-definitions/introductions.topicsILoveToDiscuss';
import qualitiesIAdoreField from '../field-definitions/introductions.qualitiesIAdore';

const INTRODUCTIONS_SECTION_FIELDS = {
  introducingMyself: introducingMyselfField(),
  howISpendFreeTime: howISpendFreeTimeField(),
  peoplesFirstImpressionOfMe: peoplesFirstImpressionOfMeField(),
  howFriendsDiscribeMe: howFriendsDiscribeMeField(),
  whatIEnjoyInLife: whatIEnjoyInLifeField(),
  topicsILoveToDiscuss: topicsILoveToDiscussField(),
  qualitiesIAdore: qualitiesIAdoreField()
};

export default INTRODUCTIONS_SECTION_FIELDS;
