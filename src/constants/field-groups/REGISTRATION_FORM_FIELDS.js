/* complete your profile */
import displayImagesField from '../../constants/field-definitions/displayImages';
import emailField from '../../constants/field-definitions/email';
import passwordField from '../../constants/field-definitions/password';
import passwordVerifyField from '../../constants/field-definitions/passwordVerify';
/* basic */
import nameField from '../../constants/field-definitions/profile.name';
import genderField from '../../constants/field-definitions/profile.gender';
import birthdayField from '../../constants/field-definitions/profile.birthday';
import heightField from '../../constants/field-definitions/profile.height';
import bodyFigureField from '../field-definitions/profile.bodyFigure';
import martialStatusField from '../../constants/field-definitions/profile.martialStatus';
import languagesField from '../../constants/field-definitions/profile.languages';
import postalCodeField from '../../constants/field-definitions/profile.postalCode';
import residenceGroupField from '../../constants/field-definitions/profile.residence_group';
import residentialStatusField from '../../constants/field-definitions/profile.residentialStatus';
import willingToRelocateField from '../../constants/field-definitions/profile.willingToRelocate';
/* background */
import highestEducationField from '../../constants/field-definitions/profile.highestEducation';
import educationInstituteField from '../../constants/field-definitions/profile.educationInstitute';
import fieldOfStudyField from '../../constants/field-definitions/profile.fieldOfStudy';
import industryField from '../../constants/field-definitions/profile.industry';
import companyField from '../../constants/field-definitions/profile.company';
import jobTitleField from '../../constants/field-definitions/profile.jobTitle';
import incomeField from '../../constants/field-definitions/profile.income';
import religionField from '../../constants/field-definitions/profile.religion';
import ethnicitiesField from '../../constants/field-definitions/profile.ethnicities';
/* lifestyle */
import smokingField from '../../constants/field-definitions/profile.smoking';
import drinkingField from '../../constants/field-definitions/profile.drinking';
import interestsField from '../../constants/field-definitions/profile.interests';
import booksField from '../../constants/field-definitions/profile.books';
import musicField from '../../constants/field-definitions/profile.music';
import moviesAndShowsField from '../../constants/field-definitions/profile.moviesAndShows';

const REGISTRATION_FORM_FIELDS = [
  {
    sectionLabel: 'Complete Profile',
    sectionFields: [
      displayImagesField(),
      emailField(),
      passwordField(),
      passwordVerifyField()
    ]
  },
  {
    sectionLabel: 'Basic',
    sectionFields: [
      nameField(),
      genderField(),
      birthdayField(),
      heightField(),
      bodyFigureField(),
      martialStatusField(),
      languagesField(),
      postalCodeField(),
      residenceGroupField(),
      residentialStatusField(),
      willingToRelocateField()
    ]
  },
  {
    sectionLabel: 'Background',
    sectionFields: [
      highestEducationField(),
      educationInstituteField(),
      fieldOfStudyField(),
      industryField(),
      companyField(),
      jobTitleField(),
      incomeField(),
      religionField(),
      ethnicitiesField(),
    ],
  },
  {
    sectionLabel: 'Lifestyle',
    sectionFields: [
      smokingField(),
      drinkingField(),
      interestsField(),
      booksField(),
      musicField(),
      moviesAndShowsField()
    ]
  }
];

const getMergedSectionFields = () => {
  let mergedSectionFields = [];
  for (let section of REGISTRATION_FORM_FIELDS) {
    mergedSectionFields = mergedSectionFields.concat(section.sectionFields);
  }
  return mergedSectionFields;
};

export { REGISTRATION_FORM_FIELDS, getMergedSectionFields }
