import genderField from '../field-definitions/preference.gender';
import ageRangeField from '../field-definitions/preference.age_range';
import heightRangeField from '../field-definitions/preference.height_range';
import martialStatusField from '../field-definitions/preference.martialStatus';
import hasChildrenField from '../field-definitions/preference.hasChildren';
import educationField from '../field-definitions/preference.education';
import incomeField from '../field-definitions/preference.income';
import religionField from '../field-definitions/preference.religion';
import ethnicitiesField from '../field-definitions/preference.ethnicities';
import smokingField from '../field-definitions/preference.smoking';
import drinkingField from '../field-definitions/preference.drinking';
import qualitiesField from '../field-definitions/preference.qualities';

const PREFERENCEINFO_EDIT_DIALOG_FORM_FIELDS = [
  {
    sectionLabel: 'Looking for in a match',
    sectionFields: [
      genderField(),
      ageRangeField(),
      heightRangeField(),
      martialStatusField(),
      hasChildrenField(),
    
      educationField(),
      incomeField(),
      religionField(),
      ethnicitiesField(),
      
      smokingField(),
      drinkingField(),
      qualitiesField()
    ]
  }
];

export default PREFERENCEINFO_EDIT_DIALOG_FORM_FIELDS;
