'use strict'

import { getMergedSectionFields } from '../constants/field-groups/REGISTRATION_FORM_FIELDS';
import { getDataFieldValueFromDataObjectWithFieldPathAndFieldId } from '../utils/helpers/commonHelpers';

class UserProfile {
  constructor() {
    this._id = '';

    this.displayImages = [];
    this.email = '';
    this.password = '';

    this.profile = {
      basic: {},
      background: {},
      lifestyle: {}
    };
    this.introductions = {};
    this.preference = {};

    this.stats = {};
    this.lastActiveAt = '';
  }

  initFromDatabase(user) {
    if (user._id != null) this.setId(user._id);
    if (user.displayImages != null) this.setDisplayImages(user.displayImages);
    if (user.email != null) this.setEmail(user.email);
    if (user.profile != null && user.profile.basic != null) this.setName(user.profile.basic.name);
    if (user.profile != null && user.profile.basic != null) this.setGender(user.profile.basic.gender);
    if (user.profile != null && user.profile.basic != null) this.setBirthday(user.profile.basic.birthday);
    if (user.profile != null && user.profile.basic != null) this.setHeight(user.profile.basic.height);
    if (user.profile != null && user.profile.basic != null) this.setBodyFigure(user.profile.basic.bodyFigure);
    if (user.profile != null && user.profile.basic != null) this.setMartialStatus(user.profile.basic.martialStatus);
    if (user.profile != null && user.profile.basic != null) this.setLanguages(user.profile.basic.languages);
    if (user.profile != null && user.profile.basic != null) this.setPostalCode(user.profile.basic.postalCode);
    if (user.profile != null && user.profile.basic != null) this.setCountry(user.profile.basic.country);
    if (user.profile != null && user.profile.basic != null) this.setProvince(user.profile.basic.province);
    if (user.profile != null && user.profile.basic != null) this.setCity(user.profile.basic.city);
    if (user.profile != null && user.profile.basic != null) this.setResidentialStatus(user.profile.basic.residentialStatus);
    if (user.profile != null && user.profile.basic != null) this.setWillingToRelocate(user.profile.basic.willingToRelocate);
    if (user.profile != null && user.profile.background != null) this.setHighestEducation(user.profile.background.highestEducation);
    if (user.profile != null && user.profile.background != null) this.setEducationInstitute(user.profile.background.educationInstitute);
    if (user.profile != null && user.profile.background != null) this.setFieldOfStudy(user.profile.background.fieldOfStudy);
    if (user.profile != null && user.profile.background != null) this.setIndustry(user.profile.background.industry);
    if (user.profile != null && user.profile.background != null) this.setCompany(user.profile.background.company);
    if (user.profile != null && user.profile.background != null) this.setJobTitle(user.profile.background.jobTitle);
    if (user.profile != null && user.profile.background != null) this.setIncome(user.profile.background.income);
    if (user.profile != null && user.profile.background != null) this.setReligion(user.profile.background.religion);
    if (user.profile != null && user.profile.background != null) this.setEthnicities(user.profile.background.ethnicities);
    if (user.profile != null && user.profile.lifestyle != null) this.setSmoking(user.profile.lifestyle.smoking);
    if (user.profile != null && user.profile.lifestyle != null) this.setDrinking(user.profile.lifestyle.drinking);
    if (user.profile != null && user.profile.lifestyle != null) this.setInterests(user.profile.lifestyle.interests);
    if (user.profile != null && user.profile.lifestyle != null) this.setBooks(user.profile.lifestyle.books);
    if (user.profile != null && user.profile.lifestyle != null) this.setMusic(user.profile.lifestyle.music);
    if (user.profile != null && user.profile.lifestyle != null) this.setMoviesAndShows(user.profile.lifestyle.moviesAndShows);
    if (user.introductions != null) this.setIntroducingMyself(user.introductions.introducingMyself);
    if (user.introductions != null) this.setHowISpendFreeTime(user.introductions.howISpendFreeTime);
    if (user.introductions != null) this.setPeoplesFirstImpressionOfMe(user.introductions.peoplesFirstImpressionOfMe);
    if (user.introductions != null) this.setHowFriendsDiscribeMe(user.introductions.howFriendsDiscribeMe);
    if (user.introductions != null) this.setTopicsILoveToDiscuss(user.introductions.topicsILoveToDiscuss)
    if (user.introductions != null) this.setWhatIEnjoyInLife(user.introductions.whatIEnjoyInLife);
    if (user.introductions != null) this.setQualitiesIAdore(user.introductions.qualitiesIAdore);
    if (user.preference != null) this.setPreferredGender(user.preference.gender);
    if (user.preference != null) this.setPreferredMinAge(user.preference.minAge);
    if (user.preference != null) this.setPreferredMaxAge(user.preference.maxAge);
    if (user.preference != null) this.setPreferredMinHeight(user.preference.minHeight);
    if (user.preference != null) this.setPreferredMaxHeight(user.preference.maxHeight);
    if (user.preference != null) this.setPreferredMartialStatus(user.preference.martialStatus);
    if (user.preference != null) this.setPreferredLanguages(user.preference.languages);
    if (user.preference != null) this.setPreferredEducation(user.preference.education);
    if (user.preference != null) this.setPreferredIncome(user.preference.income);
    if (user.preference != null) this.setPreferredReligions(user.preference.religions);
    if (user.preference != null) this.setPreferredEthnicities(user.preference.ethnicities);
    if (user.preference != null) this.setPreferredSmoking(user.preference.smokeing);
    if (user.preference != null) this.setPreferredDrinking(user.preference.drinking);
    if (user.stats != null) this.setAdmirers(user.stats.admirers);
    if (user.stats != null) this.setAdorers(user.stats.adorers);
    if (user.stats != null) this.setVisits(user.stats.visits);
    if (user.lastActiveAt != null) this.setLastActiveAt(user.lastActiveAt);
  }

  initFromApplicationState(user) {
    return this.initFromDatabase(user);
  }

  initFromRegistrationForm(values) {
    this.setDisplayImages(values.displayImage);
    this.setEmail(values.email);
    this.setPassword(values.password);
    this.setName(values.name);
    this.setGender(values.gender);
    this.setBirthday(values.birthday);
    this.setHeight(values.height);
    this.setBodyFigure(values.bodyFigure);
    this.setMartialStatus(values.martialStatus);
    this.setLanguages(values.languages);
    this.setPostalCode(values.postalCode);
    this.setCountry(values.country);
    this.setProvince(values.province);
    this.setCity(values.city);
    this.setResidentialStatus(values.residentialStatus);
    this.setWillingToRelocate(values.willingToRelocate);
    this.setHighestEducation(values.highestEducation);
    this.setEducationInstitute(values.educationInstitute);
    this.setFieldOfStudy(values.fieldOfStudy);
    this.setIndustry(values.industry);
    this.setCompany(values.company);
    this.setJobTitle(values.jobTitle);
    this.setIncome(values.income);
    this.setReligion(values.religion);
    this.setEthnicities(values.ethnicities);
    this.setSmoking(values.smoking);
    this.setDrinking(values.drinking);
    this.setInterests(values.interests);
    this.setBooks(values.books);
    this.setMusic(values.music);
    this.setMoviesAndShows(values.moviesAndShows);
  }

  getAllFields(submission) {
    const fields = {
      _id: this._id,
      email: this.email,
      displayImages: this.displayImages,
      profile: this.profile,
      introductions: this.introductions,
      preference: this.preference,
      stats: this.stats,
      lastActiveAt: this.lastActiveAt
    };
    if (submission) {
      delete fields._id;
      delete fields.displayImages;
      // to be removed if we limit to OAuth only
      if (this.password) {
        fields.password = this.password;
      }
    }
    return fields;
  }

  getRegistrationFormFields() {
    const registrationFields = getMergedSectionFields();
    let fields = {};
    for (let i = 0; i < registrationFields.length; i++) {
      const fieldProps = registrationFields[i];
      if (fieldProps.fieldType === 'comboTextField' || fieldProps.fieldType === 'comboSelectionField') {
        fieldProps.itemFields.forEach((itemFieldProps) => {
          fields = this.resolveFormField(fields, itemFieldProps);
        });
      } else {
        fields = this.resolveFormField(fields, fieldProps);
      }
    }
    return fields;
  }

  resolveFormField(fields, fieldProps) {
    let fieldValue = getDataFieldValueFromDataObjectWithFieldPathAndFieldId(this, fieldProps);
    if (fieldValue == null) {
      fields[fieldProps.fieldId] = '';
      return fields;
    }
    if (fieldProps.fieldId === 'displayImages') {
      fieldValue = JSON.stringify(fieldValue);
    }
    if (fieldProps.fieldId === 'birthday') {
      fieldValue = fieldValue.substring(0, 10); // only needs "yyyy-MM-dd"
    }
    if (fieldProps.fieldType === 'selectionField') {
      if (fieldProps.isMulti) {
        if (fieldValue.length > 0) {
          fieldValue = fieldValue.map((eachValue) => ({
            label: eachValue,
            value: eachValue
          }));
        } else {
          fieldValue = [];
        }
      } else {
        if (fieldValue.toString().trim().length > 0) {
          fieldValue = {
            label: fieldValue,
            value: fieldValue
          };
        } else {
          fieldValue = '';
        }
      }
    }
    fields[fieldProps.fieldId] = fieldValue;
    return fields;
  }

  // getRegistrationFormValuesFlat() {
  //   return {
  //     email: this.email,
  //     password: this.password,
  //     // profile.basic
  //     name: this.profile.basic.name,
  //     gender: this.profile.basic.gender,
  //     birthday: this.profile.basic.birthday,
  //     height: this.profile.basic.height,
  //     bodyFigure: this.profile.basic.bodyFigure,
  //     martialStatus: this.profile.basic.martialStatus,
  //     languages: this.profile.basic.languages,
  //     postalCode: this.profile.basic.postalCode,
  //     country: this.profile.basic.country,
  //     province: this.profile.basic.province,
  //     city: this.profile.basic.city,
  //     residentialStatus: this.profile.basic.residentialStatus,
  //     willingToRelocate: this.profile.basic.willingToRelocate,
  //     // profile.background
  //     highestEducation: this.profile.background.highestEducation,
  //     educationInstitute: this.profile.background.educationInstitute,
  //     fieldOfStudy: this.profile.background.fieldOfStudy,
  //     jobTitle: this.profile.background.jobTitle,
  //     industry: this.profile.background.industry,
  //     company: this.profile.background.company,
  //     income: this.profile.background.income,
  //     religion: this.profile.background.religion,
  //     ethnicities: this.profile.background.ethnicities,
  //     // profile.lifestyle
  //     smoking: this.profile.lifestyle.smoking,
  //     drinking: this.profile.lifestyle.drinking,
  //     interests: this.profile.lifestyle.interests,
  //     books: this.profile.lifestyle.books,
  //     music: this.profile.lifestyle.music,
  //     moviesAndShows: this.profile.lifestyle.moviesAndShows
  //   }
  // }

  getId() {
    return this._id;
  }

  setId(id) {
    this._id = id;
  }

  getEmail() {
    return this.email;
  }

  setEmail(email) {
    this.email = email;
  }

  getPassword() {
    return this.password;
  }

  setPassword(password) {
    this.password = password;
  }

  getDisplayImages() {
    return this.displayImages;
  }

  setDisplayImages(displayImages) {
    this.displayImages = displayImages;
  }

  /* PROFILE */

  getProfile() {
    return this.profile;
  }

  // profile.basic
  getName() {
    return this.profile.basic.name || '';
  }

  setName(name) {
    this.profile.basic.name = name;
  }

  getGender() {
    return this.profile.basic.gender || '';
  }

  setGender(gender) {
    this.profile.basic.gender = gender;
  }

  getBirthday() {
    return this.profile.basic.birthday || '';
  }

  setBirthday(birthday) {
    this.profile.basic.birthday = birthday;
  }

  getHeight() {
    return this.profile.basic.height || '';
  }

  setHeight(height) {
    this.profile.basic.height = height;
  }

  getBodyFigure() {
    return this.profile.basic.bodyFigure || '';
  }

  setBodyFigure(bodyFigure) {
    this.profile.basic.bodyFigure = bodyFigure;
  }

  getMartialStatus() {
    return this.profile.basic.martialStatus || '';
  }

  setMartialStatus(martialStatus) {
    this.profile.basic.martialStatus = martialStatus;
  }

  getLanguages() {
    return this.profile.basic.languages || '';
  }

  setLanguages(languages) {
    this.profile.basic.languages = languages;
  }

  getPostalCode() {
    return this.profile.basic.postalCode || '';
  }

  setPostalCode(postalCode) {
    this.profile.basic.postalCode = postalCode;
  }

  getCountry() {
    return this.profile.basic.country || '';
  }

  setCountry(country) {
    this.profile.basic.country = country;
  }

  getProvince() {
    return this.profile.basic.province || '';
  }

  setProvince(province) {
    this.profile.basic.province = province;
  }

  getCity() {
    return this.profile.basic.city || '';
  }

  setCity(city) {
    this.profile.basic.city = city;
  }

  getResidentialStatus() {
    return this.profile.basic.residentialStatus || '';
  }

  setResidentialStatus(residentialStatus) {
    this.profile.basic.residentialStatus = residentialStatus;
  }

  getWillingToRelocate() {
    return this.profile.basic.willingToRelocate || '';
  }

  setWillingToRelocate(willingToRelocate) {
    this.profile.basic.willingToRelocate = (willingToRelocate == true).toString();
  }
  
  // profile.background
  getHighestEducation() {
    return this.profile.background.highestEducation || '';
  }

  setHighestEducation(highestEducation) {
    this.profile.background.highestEducation = highestEducation;
  }

  getEducationInstitute() {
    return this.profile.background.educationInstitute || '';
  }

  setEducationInstitute(educationInstitute) {
    this.profile.background.educationInstitute = educationInstitute;
  }

  getFieldOfStudy() {
    return this.profile.background.fieldOfStudy || '';
  }

  setFieldOfStudy(fieldOfStudy) {
    this.profile.background.fieldOfStudy = fieldOfStudy;
  }

  getIndustry() {
    return this.profile.background.industry || '';
  }

  setIndustry(industry) {
    this.profile.background.industry = industry;
  }

  getCompany() {
    return this.profile.background.company || '';
  }

  setCompany(company) {
    this.profile.background.company = company;
  }

  getJobTitle() {
    return this.profile.background.jobTitle || '';
  }

  setJobTitle(jobTitle) {
    this.profile.background.jobTitle = jobTitle;
  }

  getIncome() {
    return this.profile.background.income || '';
  }

  setIncome(income) {
    this.profile.background.income = income;
  }

  getReligion() {
    return this.profile.background.religion || '';
  }

  setReligion(religion) {
    this.profile.background.religion = religion;
  }

  getEthnicities() {
    return this.profile.background.ethnicities || '';
  }

  setEthnicities(ethnicities) {
    this.profile.background.ethnicities = ethnicities;
  }

  // profile.lifestyle
  getSmoking() {
    return this.profile.lifestyle.smoking || '';
  }

  setSmoking(smoking) {
    this.profile.lifestyle.smoking = smoking;
  }

  getDrinking() {
    return this.profile.lifestyle.drinking || '';
  }

  setDrinking(drinking) {
    this.profile.lifestyle.drinking = drinking;
  }

  getInterests() {
    return this.profile.lifestyle.interests || '';
  }

  setInterests(interests) {
    this.profile.lifestyle.interests = interests;
  }

  getBooks() {
    return this.profile.lifestyle.books || '';
  }

  setBooks(books) {
    this.profile.lifestyle.books = books;
  }

  getMusic() {
    return this.profile.lifestyle.music || '';
  }

  setMusic(music) {
    this.profile.lifestyle.music = music;
  }

  getMoviesAndShows() {
    return this.profile.lifestyle.moviesAndShows || '';
  }

  setMoviesAndShows(moviesAndShows) {
    this.profile.lifestyle.moviesAndShows = moviesAndShows;
  }

  /* INTRODUCTIONS */

  getIntroductions() {
    return this.introductions;
  }

  setIntroducingMyself(introducingMyself) {
    this.introductions.introducingMyself = introducingMyself;
  }

  setHowISpendFreeTime(howISpendFreeTime) {
    this.introductions.howISpendFreeTime = howISpendFreeTime;
  }

  setPeoplesFirstImpressionOfMe(peoplesFirstImpressionOfMe) {
    this.introductions.peoplesFirstImpressionOfMe = peoplesFirstImpressionOfMe;
  }

  setHowFriendsDiscribeMe(howFriendsDiscribeMe) {
    this.introductions.howFriendsDiscribeMe = howFriendsDiscribeMe;
  }

  setTopicsILoveToDiscuss(topicsILoveToDiscuss) {
    this.introductions.topicsILoveToDiscuss = topicsILoveToDiscuss;
  }

  setWhatIEnjoyInLife(whatIEnjoyInLife) {
    this.introductions.whatIEnjoyInLife = whatIEnjoyInLife;
  }

  setQualitiesIAdore(qualitiesIAdore) {
    this.introductions.qualitiesIAdore = qualitiesIAdore;
  }

  /* PREFERENCE */

  getPreference() {
    return this.preference;
  }

  setPreferredGender(gender) {
    this.preference.gender = gender;
  }

  setPreferredMinAge(minAge) {
    this.preference.minAge = minAge;
  }

  setPreferredMaxAge(maxAge) {
    this.preference.maxAge = maxAge;
  }

  setPreferredMinHeight(minHeight) {
    this.preference.minHeight = minHeight;
  }

  setPreferredMaxHeight(maxHeight) {
    this.preference.maxHeight = maxHeight;
  }

  setPreferredMartialStatus(martialStatus) {
    this.preference.martialStatus = martialStatus;
  }

  setPreferredLanguages(languages) {
    this.preference.languages = languages;
  }

  setPreferredEducation(education) {
    this.preference.education = education;
  }

  setPreferredIncome(income) {
    this.preference.income = income;
  }

  setPreferredReligions(religions) {
    this.preference.religions = religions;
  }

  setPreferredEthnicities(ethnicities) {
    this.preference.ethnicities = ethnicities;
  }

  setPreferredSmoking(smoking) {
    this.preference.smoking = smoking;
  }

  setPreferredDrinking(drinking) {
    this.preference.drinking = drinking;
  }

  /* STATS */

  getStats() {
    return this.stats;
  }

  setAdmirers(admirers) {
    this.stats.admirers = admirers;
  }

  setAdorers(adorers) {
    this.stats.adorers = adorers;
  }

  setVisits(visits) {
    this.stats.visits = visits;
  }

  getLastActiveAt() {
    return this.lastActiveAt;
  }

  setLastActiveAt(lastActiveAt) {
    this.lastActiveAt = lastActiveAt;
  }
}

export default UserProfile;
