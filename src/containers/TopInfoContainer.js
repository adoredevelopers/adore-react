import React, { Component } from 'react';
import { connect } from 'react-redux';

import TopInfo from '../components/top-info/TopInfo';

class TopInfoContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  /* REPLACE THIS LOGIC! leverage api to get the user if not found in redux store   */
  resolveComponentProps = () => {
    let userInfo = null;
    if (this.userId == null) {
      userInfo = this.props.profileInfo;
    } else {
      // try to resolve from user list
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userInfo = this.props.userList.users[i];
          break;
        }
      }
      // not found in user list, then check conversation message users
      const conversationKeys = Object.keys(this.props.conversations);
      for (let i = 0; i < conversationKeys.length; i++) {
        const conversation = this.props.conversations[conversationKeys[i]];
        if (this.userId === conversation.messageUser._id) {
          userInfo = conversation.messageUser;
          break;
        }
      }
    }
    return {
      userInfo
    };
  }

  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    return (
      <TopInfo {...this.resolveComponentProps()} {...otherProps} />
    );
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(TopInfoContainer);
