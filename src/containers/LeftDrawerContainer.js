import LeftDrawer from '../components/left-drawer/LeftDrawer';
import withSignOut from './functional-hocs/withSignOut';

export default withSignOut(LeftDrawer);
