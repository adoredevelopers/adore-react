import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragSource, DropTarget } from 'react-dnd';

import PhotoItem from '../components/top-actions/change-photos-dialog/PhotoItem';
import constants from '../constants/constants';

const dragSource = {
  beginDrag(props) {
    console.debug('drag: ');
    console.debug(props);
    props.setImageDragIndex(props.photoItem);
    return {
      dragIndex: props.index
    };
  }
};

const collectDrag = (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  connectDragPreview: connect.dragPreview(),
  isDragging: monitor.isDragging()
});

const dropTarget = {
  drop(props) {
    console.debug('drop: ');
    console.debug(props);
    props.setImageDropIndex(props.photoItem);
    props.moveImage();
  }
};

const collectDrop = (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
  isOver: monitor.isOver()
});

class PhotoItemContainer extends Component {
  componentDidMount() {
    this.setPreviewImage();
  }

  componentDidUpdate() {
    this.setPreviewImage();
  }

  setPreviewImage() {
    const img = new Image(120, 120);
    img.src = this.props.photoItem[this.props.imageStorage][constants.imageSizes.SMALL];
    img.onload = () => this.props.connectDragPreview(img);
  }

  render() {
    const {
      connectDragSource,
      connectDragPreview,
      isDragging,
      connectDropTarget,
      isOver,
      setImageDragIndex,
      setImageDropIndex,
      moveImage,
      ...otherProps
    } = this.props;
    return connectDropTarget(
      connectDragSource(
        <div className="photoItem">
          <PhotoItem isDragging={isDragging} isOver={isOver} {...otherProps} />
        </div>
      )
    );
  }
}

PhotoItemContainer.propTypes = {
  connectDragSource: PropTypes.func.isRequired,
  connectDragPreview: PropTypes.func.isRequired,
  isDragging: PropTypes.bool.isRequired,
  connectDropTarget: PropTypes.func.isRequired,
  isOver: PropTypes.bool.isRequired,
  setImageDragIndex: PropTypes.func.isRequired,
  setImageDropIndex: PropTypes.func.isRequired,
  moveImage: PropTypes.func.isRequired
};

export default DropTarget(constants.dnd.itemTypes.PROFILE_IMAGE, dropTarget, collectDrop)(DragSource(constants.dnd.itemTypes.PROFILE_IMAGE, dragSource, collectDrag)(PhotoItemContainer));
