import { connect } from 'react-redux';

import ConversationListControl from '../components/conversation-list-control/ConversationListControl';

const mapStateToProps = state => ({
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(ConversationListControl);
