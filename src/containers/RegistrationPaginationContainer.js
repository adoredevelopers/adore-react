import { connect } from 'react-redux';

import Pagination from '../components/pagination/Pagination';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Pagination);
