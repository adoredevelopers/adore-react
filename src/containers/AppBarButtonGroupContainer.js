import AppBarButtonGroup from '../components/header/app-bar-button-group/AppBarButtonGroup';
import withSignOut from './functional-hocs/withSignOut';

export default withSignOut(AppBarButtonGroup);
