import { connect } from 'react-redux';

import FiltersDialog from '../components/filter-sort-toolbar/filters-dialog/FiltersDialog';
import { setUsersFiltersAction, filterUserListAction } from '../actions';

const mapStateToProps = state => ({
  selfUser: state.profileInfo,
  sortBy: state.userList.sortBy,
  filters: state.userList.filters
});

const mapDispatchToProps = dispatch => ({
  setUsersFilters: (filters) => {
    dispatch(setUsersFiltersAction(filters))
  },
  filterUserList: (filters) => {
    dispatch(filterUserListAction(filters));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(FiltersDialog);
