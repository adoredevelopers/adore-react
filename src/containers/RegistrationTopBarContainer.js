import { withRouter } from 'react-router-dom'

import RegistrationTopBar from '../components/registration-top-bar/RegistrationTopBar';
import withSignOut from './functional-hocs/withSignOut';

export default withRouter(withSignOut(RegistrationTopBar));
