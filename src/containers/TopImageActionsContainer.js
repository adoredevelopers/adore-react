import { connect } from 'react-redux';

import TopImageActions from '../components/top-actions/TopImageActions';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(TopImageActions);
