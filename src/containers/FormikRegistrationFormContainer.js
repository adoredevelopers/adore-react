import { connect } from 'react-redux';

import FormikRegistrationForm from '../components/registration-form/FormikRegistrationForm';
import withUpdateProfile from '../containers/functional-hocs/withUpdateProfile';

const mapStateToProps = state => {
  return {
    profileInfo: state.profileInfo
  };
};

const mapDispatchToProps = dispatch => ({
});

export default withUpdateProfile(connect(mapStateToProps, mapDispatchToProps)(FormikRegistrationForm));
