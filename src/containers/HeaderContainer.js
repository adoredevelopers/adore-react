import { connect } from 'react-redux';

import Header from '../components/header/Header';

const mapStateToProps = state => {
  let name = null, displayImage = null;
  if (state.profileInfo != null)  {
    if (state.profileInfo.profile != null && state.profileInfo.profile.basic != null) {
      name = state.profileInfo.profile.basic.name;
    }
    if (state.profileInfo.displayImages != null && state.profileInfo.displayImages.length > 0) {
      displayImage = state.profileInfo.displayImages[0];
    }
  }
  return {
    userInfo: {
      name,
      displayImage
    }
  };
};

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
