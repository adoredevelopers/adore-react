import { DragDropContext } from 'react-dnd';
// import HTML5Backend from 'react-dnd-html5-backend';
// import { default as TouchBackend } from 'react-dnd-touch-backend';
import { default as MultiBackend } from 'react-dnd-multi-backend';
import { default as HTML5toTouch } from 'react-dnd-multi-backend/lib/HTML5toTouch';

import ChangePhotosDialog from '../components/top-actions/change-photos-dialog/ChangePhotosDialog';
import withImageUpload from '../containers/functional-hocs/withImageUpload';
import withLoadingSpinner from '../containers/functional-hocs/withLoadingSpinner';

// export default DragDropContext(HTML5Backend)(withLoadingSpinner(withImageUpload(ChangePhotosDialog)));
export default DragDropContext(MultiBackend(HTML5toTouch))(withLoadingSpinner(withImageUpload(ChangePhotosDialog)));
