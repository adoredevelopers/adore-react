import { connect } from 'react-redux';

import TopMessagingActions from '../components/top-actions/TopMessagingActions';
import withSendMessage from './functional-hocs/withSendMessage';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default withSendMessage(connect(mapStateToProps, mapDispatchToProps)(TopMessagingActions));
