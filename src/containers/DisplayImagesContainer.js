import React, { Component } from 'react';
import { connect } from 'react-redux';

import DisplayImages from '../components/display-images/DisplayImages';

class DisplayImagesContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  resolveComponentProps = () => {
    let userProfile = null;
    if (this.userId == null) {
      userProfile = this.props.profileInfo;
    } else {
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userProfile = this.props.userList.users[i];
          break;
        }
      }
      // In case browsing details page from chat view,
      // we might not be able to resolve userProfile from the cached userList from browseing,
      // we then check conversation message users instead.
      if (!userProfile) {
        const conversationKeys = Object.keys(this.props.conversations);
        for (let i = 0; i < conversationKeys.length; i++) {
          const conversation = this.props.conversations[conversationKeys[i]];
          if (this.userId === conversation.messageUser._id) {
            userProfile = conversation.messageUser;
            break;
          }
        }
      }
    }
    let resolvedProps = {};
    if (userProfile != null) {
      resolvedProps = {
        userName: userProfile.profile ? userProfile.profile.basic.name : '',
        displayImages: userProfile.displayImages
      };
    } else {
      resolvedProps = {
        userName: '',
        displayImages: []
      };
    }
    return resolvedProps;
  }

  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    return (
      <DisplayImages {...this.resolveComponentProps()} {...otherProps} />
    );
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(DisplayImagesContainer);
