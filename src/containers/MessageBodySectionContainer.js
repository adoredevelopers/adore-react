import { connect } from 'react-redux';

import MessageBodySection from '../components/message-body-section/MessageBodySection';
import withFetchMoreMessages from '../containers/functional-hocs/withFetchMoreMessages';

const mapStateToProps = state => ({
  selfUserId: state.sessionStates.selfUserId,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default withFetchMoreMessages(connect(mapStateToProps, mapDispatchToProps)(MessageBodySection));
