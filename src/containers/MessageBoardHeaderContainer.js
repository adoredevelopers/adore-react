import { connect } from 'react-redux';

import MessageBoardHeader from '../components/message-board-header/MessageBoardHeader';

const mapStateToProps = state => ({
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(MessageBoardHeader);
