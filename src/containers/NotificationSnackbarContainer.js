import { connect } from 'react-redux';

import NotificationSnackbar from '../components/footer/notification-snackbar/NotificationSnackbar';
import withNotifications from '../containers/functional-hocs/withNotifications';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default withNotifications(connect(mapStateToProps, mapDispatchToProps)(NotificationSnackbar));
