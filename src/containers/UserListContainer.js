import { connect } from 'react-redux';

import UserList from '../components/user-list/UserList';
import withLoadingSpinner from '../containers/functional-hocs/withLoadingSpinner';

const mapStateToProps = state => ({
  isLoading: state.userList.isFetching,
  currentPage: state.userList.currentPage,
  recordsPerPage: state.userList.limit,
  users: state.userList.users
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(withLoadingSpinner(UserList));
