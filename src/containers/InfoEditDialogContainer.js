import { connect } from 'react-redux';

import InfoEditDialog from '../components/__info-edit-dialog/InfoEditDialog';
import withUpdateProfile from '../containers/functional-hocs/withUpdateProfile';

const mapStateToProps = state => {
  return {
    profileInfo: state.profileInfo
  };
};

const mapDispatchToProps = dispatch => ({
});

export default withUpdateProfile(connect(mapStateToProps, mapDispatchToProps)(InfoEditDialog));
