import React, { Component } from 'react';
import { connect } from 'react-redux';

import TopBarMobile from '../components/top-bar-mobile/TopBarMobile';

class TopBarMobileContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  resolveComponentProps = () => {
    if (this.props.numberOfDisplayImages != null) {
      // return if explicitly passed in
      return {
        numberOfDisplayImages: this.props.numberOfDisplayImages
      };
    }
    let userProfile = null;
    if (this.userId == null) {
      userProfile = this.props.profileInfo;
    } else {
      // try to resolve from user list
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userProfile = this.props.userList.users[i];
          break;
        }
      }
      // not found in user list, then check conversation message users
      const conversationKeys = Object.keys(this.props.conversations);
      for (let i = 0; i < conversationKeys.length; i++) {
        const conversation = this.props.conversations[conversationKeys[i]];
        if (this.userId === conversation.messageUser._id) {
          userProfile = conversation.messageUser;
          break;
        }
      }
    }
    return {
      numberOfDisplayImages: userProfile.displayImages.length
    };
  }

  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    console.debug(profileInfo);
    console.debug(userList);
    console.debug(conversations);
    return (
      <TopBarMobile {...this.resolveComponentProps()} {...otherProps} />
    );
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(TopBarMobileContainer);
