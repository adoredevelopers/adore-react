import { connect } from 'react-redux';

import DetailedInfoSection from '../components/detailed-info-section/DetailedInfoSection';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailedInfoSection);
