import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import BasicInfoList from '../components/basic-info-list/BasicInfoList';
import highestEducation from '../constants/field-definitions/profile.highestEducation';
import { getZodiacFromBirthDate, getChineseZodiacFromBirthDate } from '../utils/helpers/commonHelpers';

const buildPersonalBasicInfo = (profileInfo) => {
  //'女, 华人, 160cm, 匀称身材, 从未结婚, 没有孩子'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null && profileInfo.profile.basic != null) {
    let personalBasicInfoArray = [];
    const basicInfo = profileInfo.profile.basic;
    if (basicInfo.gender != null) {
      personalBasicInfoArray.push(basicInfo.gender);
    }
    if (basicInfo.ethnicities != null) {
      personalBasicInfoArray.push(basicInfo.ethnicities);
    }
    if (basicInfo.height != null) {
      personalBasicInfoArray.push(basicInfo.height + 'cm');
    }
    if (basicInfo.bodyType != null) {
      personalBasicInfoArray.push(basicInfo.bodyType + ' build');
    }
    if (basicInfo.martialStatus != null) {
      personalBasicInfoArray.push(basicInfo.martialStatus);
    }
    if (basicInfo.hasChildren != null) {
      personalBasicInfoArray.push(basicInfo.hasChildren ? 'no children' : 'has children');
    }
    infoText = personalBasicInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildPersonalExtraInfo = (profileInfo) => {
  //'射手座, 属马'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null && profileInfo.profile.basic != null) {
    let personalExtraInfoArray = [];
    const basicInfo = profileInfo.profile.basic;
    const birthday = basicInfo != null ? basicInfo.birthday : null;
    if (birthday != null) {
      personalExtraInfoArray.push('Born in the year of ' + getChineseZodiacFromBirthDate(birthday));
      personalExtraInfoArray.push('Horoscope ' + getZodiacFromBirthDate(birthday));
    }
    infoText = personalExtraInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildEducationAndWorkInfo = (profileInfo) => {
  //'研究生毕业于University of Toronto, 硕士, 从事教育工作'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null && profileInfo.profile.background != null) {
    let educationAndWorkInfoArray = [];
    const backgroundInfo = profileInfo.profile.background;
    const educationArray = highestEducation().fieldValues.map((fieldValue) => {
      return fieldValue.value;
    });
    if (backgroundInfo.highestEducationFields != null) {
      educationAndWorkInfoArray.push(educationArray[backgroundInfo.highestEducation]);
    }
    const bachelorIndex = educationArray.indexOf('Bachelor');
    if (bachelorIndex !== -1) {
      if (backgroundInfo.highestEducationFields >= bachelorIndex) {
        if (backgroundInfo.bachelorInstitution != null) {
          educationAndWorkInfoArray.push('本科毕业于' + backgroundInfo.bachelorInstitution);
        }
        if (backgroundInfo.highestEducationFields > bachelorIndex) {
          if (backgroundInfo.graduateInstitution != null) {
            educationAndWorkInfoArray.push('研究生毕业于' + backgroundInfo.graduateInstitution);
          }
        }
      }
    }
    if (backgroundInfo.occupation != null) {
      educationAndWorkInfoArray.push('从事' + backgroundInfo.occupation + '工作');
    }
    infoText += educationAndWorkInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildOriginAndResidenceInfo = (profileInfo) => {
  //'出生在中国, 加拿大永久居民'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null) {
    let originAndResidenceInfoArray = [];
    const basicInfo = profileInfo.profile.basic;
    const backgroundInfo = profileInfo.profile.background;
    if (backgroundInfo != null) {
      if (backgroundInfo.birthPlace != null) {
        const country = backgroundInfo.birthPlace.country;
        // const province = backgroundInfo.birthPlace.province;
        // const city = backgroundInfo.birthPlace.city;
        if (country != null && country.trim().length > 0) {
          originAndResidenceInfoArray.push('出生在' + country);
        }
      }
    }
    if (basicInfo != null) {   
      if (basicInfo.residence != null) {
        const country = basicInfo.residence.country;
        // const province = basicInfo.residence.province;
        // const city = basicInfo.residence.city;
        const residentialStatus = basicInfo.residentialStatus;
        if (country != null && country.trim().length > 0 && residentialStatus != null && residentialStatus.trim().length > 0) {
          originAndResidenceInfoArray.push(residentialStatus + ' of ' + country);
        }
      }
    }
    infoText += originAndResidenceInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildLanguageInfo = (profileInfo) => {
  //'会说英语'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null && profileInfo.profile.basic != null) {
    let languageInfoArray = [];
    if (profileInfo.profile.basic.languages.length > 0) {
      languageInfoArray.push('Speaks ' + profileInfo.profile.basic.languages.join(', '));
    }
    infoText += languageInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    }
  }
  return info;
};

const buildLifestyleInfo = (profileInfo) => {
  //'不抽烟, 不饮酒'
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.profile != null && profileInfo.profile.lifestyle != null) {
    let lifestyleInfoArray = [];
    const lifestyleInfo = profileInfo.profile.lifestyle;
    if (lifestyleInfo.smoking != null) {
      lifestyleInfoArray.push(lifestyleInfo.smoking + ' Smoke');
    }
    if (lifestyleInfo.drinking != null) {
      lifestyleInfoArray.push('Drinking ' + lifestyleInfo.drinking);
    }
    lifestyleInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    }
  }
  return info;
};

const buildActivityInfo = (profileInfo) => {
  //'上次在线是今天'
  let info = null;
  let infoText = '';
  if (profileInfo.lastActiveAt != null) {
    let seconds = moment().diff(moment(profileInfo.lastActiveAt), 'seconds');
    if (seconds < 300) { // 5 minutes
      infoText += 'online now';
    } else if (seconds < 900) { // 15 minutes
      infoText += 'active 15 minutes ago';
    } else if (seconds < 1800) { // 30 minutes
      infoText += 'active 30 minutes ago';
    } else if (seconds < 3600) { // 1 hour
      infoText += 'active 1 hour ago';
    } else if (seconds < 7200) { // 2 hours
      infoText += 'active 2 hours ago';
    } else if (seconds < 14400) { // 4 hours
      infoText += 'active 4 hours ago';
    } else if (seconds < 21600) { // 6 hours
      infoText += 'active 6 hours ago';
    } else if (seconds < 43200) { // 12 hours
      infoText += 'active 12 hours ago';
    } else if (seconds < 86400) { // 1 day
      infoText += 'active 1 day ago';
    } else if (seconds < 604800) { // 1 week
      infoText += 'active a week ago';
    } else { // 1 week maximum to display
      infoText += 'active a week ago';
    }
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    }
  }
  return info;
};

class BasicInfoListContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  resolveComponentProps = () => {
    let userProfile = null;
    if (this.userId == null) {
      userProfile = this.props.profileInfo;
    } else {
      // try to resolve from user list
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userProfile = this.props.userList.users[i];
          break;
        }
      }
      // not found in user list, then check conversation message users
      if (userProfile == null) {
        const conversationKeys = Object.keys(this.props.conversations);
        for (let i = 0; i < conversationKeys.length; i++) {
          const conversation = this.props.conversations[conversationKeys[i]];
          if (this.userId === conversation.messageUser._id) {
            userProfile = conversation.messageUser;
            break;
          }
        }
      }
      // not resolved to any saved user
      // maybe make an API call
      // or return 404 page
    }
    // return infoData base on the resolved user profie
    return {
      infoData: {
        personalBasicInfo: buildPersonalBasicInfo(userProfile),
        personalExtraInfo: buildPersonalExtraInfo(userProfile),
        educationAndWorkInfo: buildEducationAndWorkInfo(userProfile),
        originAndResidenceInfo: buildOriginAndResidenceInfo(userProfile),
        languageInfo: buildLanguageInfo(userProfile),
        lifestyleInfo: buildLifestyleInfo(userProfile),
        activityInfo: buildActivityInfo(userProfile)
      }
    };
  }

  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    return (
      <BasicInfoList {...this.resolveComponentProps()} {...otherProps} />
    );
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(BasicInfoListContainer);
