import Pagination from '../components/pagination/Pagination';
import withFetchUserList from '../containers/functional-hocs/withFetchUserList';

export default withFetchUserList(Pagination);
