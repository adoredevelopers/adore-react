import React, { Component } from 'react';
import { connect } from 'react-redux';

import IntroductionsSection from '../components/introductions-section/IntroductionsSection';
import withUpdateIntroductions from '../containers/functional-hocs/withUpdateIntroductions';
import INTRODUCTIONS_SECTION_FIELDS from '../constants/field-groups/INTRODUCTIONS_SECTION_FIELDS';

class IntroductionsSectionContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  resolveComponentProps = () => {
    let userProfile = null;
    if (this.userId == null) {
      userProfile = this.props.profileInfo;
    } else {
      // try to resolve from user list
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userProfile = this.props.userList.users[i];
          break;
        }
      }
      // not found in user list, then check conversation message users
      if (userProfile == null) {
        const conversationKeys = Object.keys(this.props.conversations);
        for (let i = 0; i < conversationKeys.length; i++) {
          const conversation = this.props.conversations[conversationKeys[i]];
          if (this.userId === conversation.messageUser._id) {
            userProfile = conversation.messageUser;
            break;
          }
        }
      }
      // not resolved to any saved user
      // maybe make an API call
      // or return 404 page
    }
    // return introductionsDelta base on the resolved user profie
    let introductionsFields = {};
    Object.keys(INTRODUCTIONS_SECTION_FIELDS).forEach((fieldId, idx) => {
      const field = INTRODUCTIONS_SECTION_FIELDS[fieldId];
      const fieldValue = userProfile.introductions[fieldId];
      introductionsFields[fieldId] = {
        editActive: false,
        fieldId: fieldId,
        fieldLabel: field.fieldLabel,
        fieldValuePlaceholder: field.fieldValuePlaceholder,
        fieldValue: fieldValue ? fieldValue : ''
      };
    });
    // overriding with explicitly passed in prop
    if (this.props.introductions != null) {
      introductionsFields = this.props.introductions
    } 
    return {
      introductionsFields,
      profileInfo: userProfile
    };
  }
  
  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    const resolvedComponentProps = this.resolveComponentProps();
    if (this.userId == null) {
      const IntroductionsSectionWithUpdateIntroductions = withUpdateIntroductions(IntroductionsSection);
      return <IntroductionsSectionWithUpdateIntroductions {...resolvedComponentProps} {...otherProps} />;
    } else {
      return <IntroductionsSection {...resolvedComponentProps} {...otherProps} />
    }
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(IntroductionsSectionContainer);
