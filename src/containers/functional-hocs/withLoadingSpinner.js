import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';

function withLoadingSpinner(WrappedComponent) {
  const styles = {
    root: {
      backgroundColor: 'transparent'
    },
    paper: {
      backgroundColor: 'rgba(0, 0, 0, 0.2)',
      width: '100%',
      height: '100%'
    },
    spinner: {
      margin: 'auto',
      zIndex: '10000'
    }
  };

  class LoadingSpinnerHOC extends Component {
    constructor(props) {
      super(props);
      this.state = {
        isLoading: false
      };
    }

    setLoading = (isLoading) => {
      this.setState({
        isLoading
      });
    }

    render() {
      const {
        classes,
        isLoading,
        ...otherProps
      } = this.props;
      return (
        <div>
          <Dialog
            fullScreen
            open={isLoading != null ? isLoading : this.state.isLoading}
            classes={{
              root: classes.root,
              paper: classes.paper
            }}>
            <CircularProgress size={50} color="secondary" className={classes.spinner} />
          </Dialog>
          <WrappedComponent setLoading={this.setLoading} {...otherProps} />
        </div>
      );
    }
  }
  
  return withStyles(styles)(LoadingSpinnerHOC);
}

export default withLoadingSpinner;
