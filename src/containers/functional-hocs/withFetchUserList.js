import React, { Component } from 'react';
import { connect } from 'react-redux';

import { buildFiltersQuery } from '../../utils/helpers/commonHelpers';

import {
  setFetchingUserListAction,
  setUserListAction,
  resetUserListCurrentPageAction,
  decrementCurrentPageAction,
  incrementCurrentPageAction,
  appendMoreUsersAction
} from '../../actions';
import fetchUserList from '../../utils/services/fetchUserList';

function withFetchUserList(WrappedComponent) {
  class FetchUserListHOC extends Component {
    handleQueryChange = (query) => {
      this.props.setFetching(true);
      fetchUserList(query).then((users) => {
        this.props.setFetching(false);
        this.props.setUserList(users.data, users.total, users.limit, users.limit);
        this.props.resetUserListCurrentPage();
      });
    }
    
    goToPreviousPage = () => {
      this.props.decrementCurrentPage();
      this.scrollToTop();
    }

    goToNextPage = () => {
      var self = this;
      if (self.props.currentPage === (self.props.fetchedPages - 1)) { // at the end of fetched pages, needs to fetch more, the pagination component disables the button when reaching the max page
        this.fetchMoreUsers(this.scrollToTop);
      }
      this.props.incrementCurrentPage();
    }

    fetchMoreUsers = (callback) => {
      this.props.setFetching(true);
      const query = {
        _id: {
          $ne: this.props.selfUserId
        },
        ...buildFiltersQuery(this.props.filters),
        $sort: this.getQuerySort(this.props.sortBy),
        $skip: this.props.skip
      };
      fetchUserList(query, users => {
        this.props.appendMoreUsers(users.data, users.total, users.skip + users.limit, users.limit);
        this.props.setFetching(false);
        callback && callback();
      });
    }

    getQuerySort = (sortBy) => {
      let sort = {};
      switch (sortBy) {
        case 0:
          sort = {
            updatedAt: -1
          };
          break;
        case 1:
          // special handling due to custom aggregation at backend
          sort = {
            distance: true,
            skip: this.props.skip
          };
          break;
        case 2:
          sort = {
            //todo
          };
          break;
        case 3:
          sort = {
            createdAt: 1
          };
          break;
        default:
          sort = {
            updatedAt: -1
          };
      }
      return sort;
    }

    scrollToTop = () => {
      window.scrollTo(0, 0);
    }

    render() {
      const {
        totalPages,
        currentPage,
        fetchedPages,
        skip,
        limit,
        total,
        sortBy,
        setUserList,
        resetUserListCurrentPage,
        decrementCurrentPage,
        incrementCurrentPage,
        setFetching,
        handleQueryChange,
        handleSortChange,
        appendMoreUsers,
        ...otherProps
      } = this.props;
      return (
        <WrappedComponent
          steps={totalPages}
          activeStep={currentPage}
          skip={skip}
          limit={limit}
          getQuerySort={this.getQuerySort}
          handleQueryChange={this.handleQueryChange}
          handleBack={this.goToPreviousPage}
          handleNext={this.goToNextPage}
          {...otherProps} />
      );
    }
  }

  const mapStateToProps = state => ({
    selfUserId: state.sessionStates.selfUserId,
    isFetching: state.userList.isFetching,
    totalPages: state.userList.total === 0 ? 0 : Math.ceil(state.userList.total / state.userList.limit),
    currentPage: state.userList.currentPage,
    fetchedPages: Math.ceil(state.userList.users.length / state.userList.limit),
    skip: state.userList.skip,
    limit: state.userList.limit,
    total: state.userList.total,
    sortBy: state.userList.sortBy,
    filters: state.userList.filters
  });
  
  const mapDispatchToProps = dispatch => ({
    setFetching: (isFetching) => {
      dispatch(setFetchingUserListAction(isFetching));
    },
    setUserList: (users, total, skip, limit) => {
      dispatch(setUserListAction(users, total, skip, limit));
    },
    resetUserListCurrentPage: () => {
      dispatch(resetUserListCurrentPageAction());
    },
    decrementCurrentPage: () => {
      dispatch(decrementCurrentPageAction());
    },
    incrementCurrentPage: () => {
      dispatch(incrementCurrentPageAction());
    },
    appendMoreUsers: (users, total, skip, limit) => {
      dispatch(appendMoreUsersAction(users, total, skip, limit));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(FetchUserListHOC);
}

export default withFetchUserList;
