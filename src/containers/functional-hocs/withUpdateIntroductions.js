import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setProfileInfoAction } from '../../actions';

import APIClient from '../../utils/services/APIClient';
const api = new APIClient();

function withUpdateIntroductions(WrappedComponent) {
  class UpdateIntroductionsHOC extends Component {
    constructor(props) {
      super(props);
      this.state = {
        introductionsFields: this.props.introductionsFields
      };
    }

    handleSectionFieldClick = (fieldId, fieldDOM) => {
      const introductions = this.props.profileInfo.introductions;
      const introductionsFields = Object.assign({}, this.state.introductionsFields);
      Object.keys(introductionsFields).forEach((key, idx) => {
        if (key !== fieldId && introductionsFields[key].editActive) {
          introductionsFields[key].editActive = false;
          introductionsFields[key].fieldValue = introductions[key];
        }
      });
      introductionsFields[fieldId].editActive = !introductionsFields[fieldId].editActive;
      if (!introductionsFields[fieldId].editActive) {
        introductionsFields[fieldId].fieldValue = introductions[fieldId];
      }
      this.setState({
        introductionsFields
      }, () => {
        fieldDOM.focus();
      });
    }
  
    handleSectionFieldChange = (fieldId, fieldValue) => {
      const introductionsFields = Object.assign({}, this.state.introductionsFields);
      introductionsFields[fieldId].fieldValue = fieldValue;
      this.setState({
        introductionsFields
      });
    }

    resetSectionField = (fieldId) => {
      const introductions = this.props.profileInfo.introductions;
      const introductionsFields = Object.assign({}, this.state.introductionsFields);
      introductionsFields[fieldId].fieldValue = introductions[fieldId];
      introductionsFields[fieldId].editActive = false;
      this.setState({
        introductionsFields
      });
    }

    submitServiceRequest = async (fieldId) => {
      const id = this.props.profileInfo._id;
      let preparedData = {
        introductions: {}
      };
      const introductionsFields = Object.assign({}, this.state.introductionsFields);
      Object.keys(introductionsFields).forEach((key, idx) => {
        let fieldValue = this.state.introductionsFields[key].fieldValue;
        fieldValue = fieldValue ? fieldValue.replace(/^\s\n+|\s\n+$/g, '') : '';
        // sync the component state;
        introductionsFields[key].fieldValue = fieldValue;
        // sync preparedData with component state before saving to the database
        preparedData.introductions[key] = fieldValue;
      });
      const profileInfo = await api.client.service('users').patch(id, preparedData);
      this.props.setProfileInfo(profileInfo);
      // introductionsFields[fieldId].editActive = false;
      // this.setState({ // instrocutionsDelta is modified
      //   introductionsFields
      // });
      console.debug(profileInfo);
    }
    
    render() {
      const {
        introductionsFields,
        ...otherProps
      } = this.props;
      return (
        <WrappedComponent
        introductionsFields={this.state.introductionsFields}
          handleSectionFieldClick={this.handleSectionFieldClick}
          handleSectionFieldChange={this.handleSectionFieldChange}
          resetSectionField={this.resetSectionField}
          submitServiceRequest={this.submitServiceRequest}
          {...otherProps} />
      );
    }
  }

  const mapStateToProps = state => ({
  });
  
  const mapDispatchToProps = dispatch => ({
    setProfileInfo: (profileInfo, callback) => {
      dispatch(setProfileInfoAction(profileInfo, callback));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(UpdateIntroductionsHOC);
}

export default withUpdateIntroductions;
