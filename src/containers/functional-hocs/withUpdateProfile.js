import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  setProfileInfoAction,
  setLoggedOnAction,
  setConversationsAction,
  addConversationAction,
  addMessageAction,
  setUsersFiltersAction
} from '../../actions';

import authenticate from '../../utils/services/authenticate';
import fetchConversations from '../../utils/services/fetchConversations';
import fetchUserList from '../../utils/services/fetchUserList';
import { buildFiltersQuery } from '../../utils/helpers/commonHelpers';

import APIClient from '../../utils/services/APIClient';
const api = new APIClient();

function withUpdateProfile(WrappedComponent) {
  class UpdateProfileInfoHOC extends Component {
    constructor(props) {
      super(props);
      this.state = {
        profileInfoDelta: {
          profile: this.props.profileInfo ? Object.assign({}, this.props.profileInfo.profile) : null,
          preference: this.props.profileInfo ? Object.assign({}, this.props.profileInfo.preference): null
        }
      };
    }

    handleFormFieldChange = (fieldProps, fieldValue) => {
      const profileInfoDelta = this.updateProfileInfoDelta(fieldProps, fieldValue);
      this.setState({
        profileInfoDelta
      });
    }

    updateProfileInfoDelta = (fieldProps, fieldValue) => {
      const fieldId = fieldProps.fieldId;
      const multiSelection = fieldProps.isMulti;
      let profileInfoDelta = Object.assign({}, this.state.profileInfoDelta);
      let profileInfoIter = profileInfoDelta;
      if (!!fieldProps.fieldPath) {
        const pathTokens = fieldProps.fieldPath.split('.');
        pathTokens.forEach((pathToken, idx) => {
          profileInfoIter[pathToken] = profileInfoIter[pathToken] != null ? Object.assign({}, profileInfoIter[pathToken]) : {};
          profileInfoIter = profileInfoIter[pathToken];
        });
      }
      if (multiSelection) {
        if (fieldValue) {
          profileInfoIter[fieldId] = Object.keys(fieldValue).map((key, idx) => {
            return fieldValue[key];
          });
        } else {
          profileInfoIter[fieldId] = null;
        }
      } else {
        profileInfoIter[fieldId] = fieldValue;
      }
      return profileInfoDelta;
    }

    resetFormFields = () => {
      this.setState({
        profileInfoDelta: {
          profile: Object.assign({}, this.props.profileInfo.profile),
          preference: Object.assign({}, this.props.profileInfo.preference)
        }
      });
    }

    submitServiceRequest = async (callback) => {
      const self = this;
      if (this.props.profileInfo && this.props.profileInfo._id) {
        // udpate operation
        const id = this.props.profileInfo._id;
        const preparedData = Object.assign({}, this.state.profileInfoDelta);
        try {
          const profileInfo = await api.client.service('users').patch(id, preparedData);
          this.props.setProfileInfo(profileInfo);
        } catch(error) {
          console.error('Error updating user profile: ');
          console.error(error);
          callback && callback(true);
        }
      } else {
        // create operation - user registration
        const preparedData = Object.assign({}, this.props.profileInfo, this.state.profileInfoDelta, { accountStatus: 'ACTIVE' });
        try {
          const profileInfo = await api.client.service('users').create(preparedData);
          this.props.setProfileInfo(profileInfo, function(profileInfo) {
            console.debug(profileInfo);
          });
          // log on as current registered user
          const credentials = {
            email: preparedData.email,
            password: preparedData.password
          };
          authenticate(credentials).then((user) => {
            if (user != null) {
              // attach redux properties and methods to features API
              api.redux = {
                selfUser: user,
                addConversation: self.props.addConversation,
                addMessage: self.props.addMessage
              };
      
              self.props.setProfileInfo(user);
              return fetchConversations(user._id);
            }
          }).then((conversations) => {
            self.props.setConversations(conversations.data);

            // fetch first batch of users listing
            const filtersProp = {
              gender: self.props.profileInfo.profile.basic.gender === 'Male' ? 'Female' : 'Male',
              // minAge: 18,
              // maxAge: 100,
              // photoVerified: true,
              // country: this.props.profileInfo.profile.basic.residence.country,
              // region: this.props.profileInfo.profile.basic.residence.province
            };
            self.props.setUsersFilters(filtersProp);
            const query = {
              _id: {
                $ne: self.props.sessionStates.selfUserId
              },
              ...buildFiltersQuery(filtersProp),
              $sort: {
                updatedAt: -1
              }
            };
            return fetchUserList(query);
          }).then((result) => {
            // attach redux properties and methods to features API
            api.redux.userList = result.data;

            self.props.setUserList(result.data, result.total, result.limit, result.limit);
          }).catch((error) => {
            console.erroror('Error authenticating!', error);
          }).finally(() => {
            self.props.setLoggedOn(true, api.redux.selfUser._id);
          });
        } catch(error) {
          console.error('Error updating profile: ');
          console.error(error);
          const fieldId = Object.keys(error.errors)[0];
          const validationResult = {
            hasError: true,
            errorMessage: error.message,
            suppressFormErrorDisplay: false
          };
          self.props.updateFormFieldValidity(fieldId, validationResult);
          callback && callback(true);
        }
      }
    }

    updateUserProfile = async (payload) => {
      const self = this;
      if (this.props.profileInfo && this.props.profileInfo._id) {
        // udpate operation
        const id = this.props.profileInfo._id;
        const preparedData = Object.assign({}, this.props.profileInfo, payload);
        try {
          const profileInfo = await api.client.service('users').update(id, preparedData);
          this.props.setProfileInfo(profileInfo);
        } catch(error) {
          console.error('Error updating profile: ');
          console.error(error);
        }
      } else {
        // create operation - user registration
        const preparedData = Object.assign({}, this.props.profileInfo, payload, { accountStatus: 'ACTIVE' });
        try {
          const profileInfo = await api.client.service('users').create(preparedData);
          this.props.setProfileInfo(profileInfo);
          // log on as current registered user
          const credentials = {
            email: preparedData.email,
            password: preparedData.password
          };
          const user = await authenticate(credentials);
          if (user != null) {
            // attach redux properties and methods to features API
            api.redux = {
              selfUser: user,
              addConversation: self.props.addConversation,
              addMessage: self.props.addMessage
            };
            self.props.setProfileInfo(user);

            // fetch conversations
            fetchConversations(user._id, conversations => self.props.setConversations(conversations.data));

            // fetch user list
            const filtersProp = {
              gender: self.props.profileInfo.profile.basic.gender === 'Male' ? 'Female' : 'Male',
              // minAge: 18,
              // maxAge: 100,
              // photoVerified: true,
              // country: this.props.profileInfo.profile.basic.residence.country,
              // region: this.props.profileInfo.profile.basic.residence.province
            };
            self.props.setUsersFilters(filtersProp);
            const query = {
              _id: {
                $ne: self.props.sessionStates.selfUserId
              },
              ...buildFiltersQuery(filtersProp),
              $sort: {
                updatedAt: -1
              }
            };
            fetchUserList(query, users => self.props.setUserList(users.data, users.total, users.limit, users.limit));
          }
        } catch(error) {
          console.error('Error creating user profile: ');
          console.error(error);
        }
      }
    }

    render() {
      const {
        profileInfo,
        setProfileInfo,
        ...otherProps
      } = this.props;
      return (
        <WrappedComponent
          profileInfoDelta={this.state.profileInfoDelta}
          handleFormFieldChange={this.handleFormFieldChange}
          resetFormFields={this.resetFormFields}
          submitServiceRequest={this.submitServiceRequest}
          updateUserProfile={this.updateUserProfile}
          {...otherProps} />
      );
    }
  }

  const mapStateToProps = state => ({
    profileInfo: state.profileInfo
  });

  const mapDispatchToProps = dispatch => ({
    setLoggedOn: (isLoggedOn, selfUserId) => {
      dispatch(setLoggedOnAction(isLoggedOn, selfUserId));
    },
    setProfileInfo: (profileInfo, callback) => {
      dispatch(setProfileInfoAction(profileInfo, callback));
    },
    setConversations: (conversations) => {
      dispatch(setConversationsAction(conversations));
    },
    addConversation: (conversation) => {
      dispatch(addConversationAction(conversation));
    },
    addMessage: (message) => {
      dispatch(addMessageAction(message));
    },
    setUsersFilters: (filters) => {
      dispatch(setUsersFiltersAction(filters));
    }
  });

  return connect(mapStateToProps, mapDispatchToProps)(UpdateProfileInfoHOC);
}

export default withUpdateProfile;
