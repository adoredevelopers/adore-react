import React, { Component } from 'react';
import { connect } from 'react-redux';

// import { setLoggedOnAction, setProfileInfoAction } from '../../actions';

function withNotifications(WrappedComponent) {
  class NotificationsHOC extends Component {
    render() {
      const {
        ...otherProps
      } = this.props;
      return <WrappedComponent {...otherProps} />;
    }
  }

  const mapStateToProps = state => ({
    conversations: state.conversations
  });
  
  const mapDispatchToProps = dispatch => ({
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(NotificationsHOC);
}

export default withNotifications;
