import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addMessageAction } from '../../actions';

import APIClient from '../../utils/services/APIClient';
const api = new APIClient();

function withSendMessage(WrappedComponent) {
  class SendMessageHOC extends Component {
    sendMessage = (recipientId, message) => {
      const senderId = this.props.profileInfo._id;
      const messagePayload = {
        senderId,
        recipientId,
        message
      };
      api.client.service('messages').create(messagePayload);
    }

    render() {
      const {
        addMessage,
        ...otherProps
      } = this.props;
      return <WrappedComponent sendMessage={this.sendMessage} {...otherProps} />;
    }
  }

  const mapStateToProps = state => ({
    profileInfo: state.profileInfo
  });
  
  const mapDispatchToProps = dispatch => ({
    addMessage: (recipient, message) => {
      dispatch(addMessageAction(recipient, message));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(SendMessageHOC);
}

export default withSendMessage;
