import React, { Component } from 'react';
import { connect } from 'react-redux';

import { prependMessagesAction } from '../../actions';

import APIClient from '../../utils/services/APIClient';
const api = new APIClient();

function withFetchMoreMessages(WrappedComponent) {
  class FetchMoreMessagesHOC extends Component {
    fetchMoreMessages = (conversationId, beforeCreatedAt) => {
      const self = this;
      return api.client.service('conversations').find({
        query: {
          _id: conversationId,
          beforeCreatedAt: beforeCreatedAt
        },
      }).then(function(result) {
        const messages = result.data[0].messages;
        self.props.prependMessages(conversationId, messages);
        Promise.resolve();
      });
    }

    render() {
      const {
        prependMessages,
        ...otherProps
      } = this.props;
      return <WrappedComponent fetchMoreMessages={this.fetchMoreMessages} {...otherProps} />;
    }
  }

  const mapStateToProps = state => ({
  });
  
  const mapDispatchToProps = dispatch => ({
    prependMessages: (conversationId, messages) => {
      dispatch(prependMessagesAction(conversationId, messages));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(FetchMoreMessagesHOC);
}

export default withFetchMoreMessages;
