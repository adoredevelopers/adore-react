import React, { Component } from 'react';
import { connect } from 'react-redux';

import APIClient from '../../utils/services/APIClient';

import { setLoggedOnAction, setProfileInfoAction } from '../../actions';

const api = new APIClient();

function withSignOut(WrappedComponent) {
  class SignOutHOC extends Component {
    performLogOut = () => {
      this.props.setLoggedOn(false);
      this.props.setProfileInfo(null);
      api.client.logout();
    }

    render() {
      const {
        setLoggedOn,
        ...otherProps
      } = this.props;
      return <WrappedComponent performLogOut={this.performLogOut} {...otherProps} />;
    }
  }

  const mapStateToProps = state => ({
    sessionState: state.sessionState
  });
  
  const mapDispatchToProps = dispatch => ({
    setLoggedOn: (isLoggedOn) => {
      dispatch(setLoggedOnAction(isLoggedOn));
    },
    setProfileInfo: (profileInfo, callback) => {
      dispatch(setProfileInfoAction(profileInfo, callback));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(SignOutHOC);
}

export default withSignOut;
