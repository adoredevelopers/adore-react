import React, { Component } from 'react';

function withFieldValidations(WrappedComponent) {
  class ValidationsHOC extends Component {
    validate = (value) => {
      let isValid = true;
      const validations = this.props.validationRules;
      for (let i = 0; i < validations.length; i++) {
        isValid &= validations[i].method(value);
        if (!isValid) {
          return {
            hasError: true,
            errorMessage: validations[i].errorMessage
          };
        }
      }
      if (isValid) {
        return {
          hasError: false,
          errorMessage: null
        };
      }
    }

    render() {
      const {
        validationRules,
        ...otherProps
      } = this.props;
      return <WrappedComponent validate={this.validate} {...otherProps} />;
    }
  }

  return ValidationsHOC;
}

export default withFieldValidations;
