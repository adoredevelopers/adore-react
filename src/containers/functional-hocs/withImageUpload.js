import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as firebase from 'firebase';
import * as shortid from 'shortid';

import { setProfileInfoAction } from '../../actions';
import appConfig from '../../config/appConfig';

import APIClient from '../../utils/services/APIClient';
const api = new APIClient();

// Initialize Firebase
firebase.initializeApp(appConfig.storage[appConfig.imageStorage]);

function withImageUpload(WrappedComponent) {
  class ImageUploadHOC extends Component {
    constructor(props) {
      super(props);
      this.state = {
        profileImages:(this.props.profileInfo != null && this.props.profileInfo.displayImages != null)
          ?
          this.props.profileInfo.displayImages.map((image, idx) => {
            return Object.assign({}, image);
          })
          :
          []
      };
      this.imageStorage = appConfig.imageStorage;
      this.fireBaseStorageRef = firebase.storage().ref();
      this.imageDragIndex = -1;
      this.imageDropIndex = -1;
    }
    
    resolveImageIndex = (imageItem) => {
      const profileImages = this.state.profileImages;
      for (let i = 0; i < profileImages.length; i++) {
        const imageSize = Object.keys(imageItem[this.imageStorage])[0];
        if (imageItem[this.imageStorage][imageSize] === profileImages[i][this.imageStorage][imageSize]) {
          return i;
        }
      }
    }

    setImageDragIndex = (imageItem) => {
      this.imageDragIndex = this.resolveImageIndex(imageItem);
      console.debug(this.imageDragIndex);
    }

    setImageDropIndex = (imageItem) => {
      this.imageDropIndex = this.resolveImageIndex(imageItem);
      console.debug(this.imageDropIndex);
    }

    addImage = (resizedImages, callback) => {
      if (this.state.profileImages.length < 8) {
        let uploadPromises = [];
        let imageResolutions = [];
        const imageId = shortid.generate();
        resizedImages.forEach((resizedImage, idx) => {
          const imageResolution = Object.keys(resizedImage)[0];
          imageResolutions.push(imageResolution);
          const imageBlob = Object.values(resizedImage)[0];
          const uploadPromise = this.uploadImage(imageId, imageBlob, imageResolution);
          uploadPromises.push(uploadPromise);
        });
        Promise.all(uploadPromises).then((snapshots) => {
          let newImageSet = {};
          snapshots.forEach((snapshot, idx) => {
            newImageSet[imageResolutions[idx]] = snapshot.downloadURL;
          });
          let profileImages = this.state.profileImages;
          profileImages.push({
            [this.imageStorage]: newImageSet
          });
          this.setState({
            profileImages
          }, callback);
        }, (err) => {
          const errorMessage = 'failed to upload one or more images!';
          console.error(err);
          console.error(errorMessage);
          callback();
        });
      } else {
        const errorMessage = 'Failed to add image, maximum of 8 images has been reached.';
        console.error(errorMessage);
        callback();
      }
    }

    uploadImage = async (imageId, imageBlob, imageResolution, callback) => {
      const imagePathRef = this.fireBaseStorageRef.child(appConfig.storage[appConfig.imageStorage].avatarImagePathPrefix + '/' + imageId + '_' + imageResolution);
      if (callback != null && typeof callback === 'function') {
        imagePathRef.put(imageBlob).then(callback);
      } else {
        return imagePathRef.put(imageBlob);
      }
    }

    removeImage = (imageItem) => {
      if (this.state.profileImages.length > 1) {
        const profileImages = this.state.profileImages;
        const removeIndex = this.resolveImageIndex(imageItem);
        profileImages.splice(removeIndex, 1);
        this.setState({
          profileImages
        });
      } else {
        alert('Cannot remove the last image.');
      }
    }

    moveImage = () => {
      const profileImages = this.state.profileImages;
      const draggedImage = profileImages[this.imageDragIndex];
      profileImages.splice(this.imageDragIndex, 1);
      profileImages.splice(this.imageDropIndex, 0, draggedImage);
      this.setState({
        profileImages
      });
      this.imageDragIndex = -1;
      this.imageDropIndex = -1;
    }

    resetImages = () => {
      this.props.profileInfo && this.setState({
        profileImages: this.props.profileInfo.displayImages.map((image, idx) => {
          return Object.assign({}, image);
        })
      });
    }

    submitImages = async () => {
      const self = this;
      if (this.props.profileInfo && this.props.profileInfo._id) {
        const id = this.props.profileInfo._id;
        let preparedData = {
          displayImages: this.state.profileImages
        };
        const profileInfo = await api.client.service('users').patch(id, preparedData);
        this.props.setProfileInfo(profileInfo);
      } else {
        let preparedData = {
          displayImages: this.state.profileImages
        };
        const profileInfo = Object.assign({}, this.props.profileInfo, preparedData);
        this.props.setProfileInfo(profileInfo);
      }
      // populate the hidden displayImage input field
      // workaround, only in registration form page
      const displayImageInput = document.getElementById('displayImages');
      if (displayImageInput != null) {
        const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;
        nativeInputValueSetter.call(
          displayImageInput,
          JSON.stringify(this.state.profileImages)
        );
        const displayImageInputChangeEvent = new Event('input', { bubbles: true });
        displayImageInput.dispatchEvent(displayImageInputChangeEvent);
      }
    }

    render() {
      const {
        profileInfo,
        setProfileInfo,
        fieldProps,
        ...otherProps
      } = this.props;
      return (
        <WrappedComponent
          profileImages={this.state.profileImages}
          imageStorage={this.imageStorage}
          setImageDragIndex={this.setImageDragIndex}
          setImageDropIndex={this.setImageDropIndex}
          addImage={this.addImage}
          removeImage={this.removeImage}
          moveImage={this.moveImage}
          resetImages={this.resetImages}
          submitImages={this.submitImages}
          {...otherProps} />
      );
    }
  }

  const mapStateToProps = state => ({
    profileInfo: state.profileInfo
  });
  
  const mapDispatchToProps = dispatch => ({
    setProfileInfo: (profileInfo, callback) => {
      dispatch(setProfileInfoAction(profileInfo, callback));
    }
  });
  
  return connect(mapStateToProps, mapDispatchToProps)(ImageUploadHOC);
}

export default withImageUpload;
