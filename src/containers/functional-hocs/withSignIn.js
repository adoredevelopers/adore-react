import React, { Component } from 'react';
import { connect } from 'react-redux';

import authenticate from '../../utils/services/authenticate';
import UserProfile from '../../models/UserProfile';
import fetchConversations from '../../utils/services/fetchConversations';
import fetchUserList from '../../utils/services/fetchUserList';
import { buildFiltersQuery } from '../../utils/helpers/commonHelpers';
import withLoadingSpinner from '../../containers/functional-hocs/withLoadingSpinner';

import APIClient from '../../utils/services/APIClient';

import {
  setLoggedOnAction,
  setProfileInfoAction,
  setConversationsAction,
  addConversationAction,
  addMessageAction,
  setUserListAction,
  setUsersFiltersAction
} from '../../actions';

const api = new APIClient();

function withSignIn(WrappedComponent) {
  class SignInHOC extends Component {
    performLogIn = async (credential, callback) => {
      const self = this;
      let selfUserId = null;
      let user = null;
      try {
        if (credential) {
          user = await authenticate(Object.assign({strategy: 'local'}, credential));
        } else {
          user = await authenticate(); // using jwt access_token strategy
        }
        if (user != null) {
          const userProfile = new UserProfile();
          userProfile.initFromDatabase(user);
          // attach redux properties and methods to features API
          api.redux = {
            userProfile,
            selfUser: user,
            addConversation: self.props.addConversation,
            addMessage: self.props.addMessage
          };
          self.props.setProfileInfo(user);
          selfUserId = user._id;
          
          // fetch conversations
          fetchConversations(selfUserId, conversations => self.props.setConversations(conversations.data));

          // fetch user list
          const filtersProp = {
            gender: self.props.profileInfo.profile.basic.gender === 'Male' ? 'Female' : 'Male',
            // minAge: 18,
            // maxAge: 100,
            // photoVerified: true,
            // country: this.props.profileInfo.profile.basic.residence.country,
            // region: this.props.profileInfo.profile.basic.residence.province
          };
          self.props.setUsersFilters(filtersProp);
          const query = {
            _id: {
              $ne: selfUserId
            },
            ...buildFiltersQuery(filtersProp),
            $sort: {
              updatedAt: -1
            }
          };
          fetchUserList(query, users => self.props.setUserList(users.data, users.total, users.limit, users.limit));

          // logged on
          self.props.setLoggedOn(true, selfUserId);
        }
      } catch (err) {
        console.info('Failed to login: ');
        console.info(err);
        callback && callback();
      }
    }

    render() {
      const {
        setLoggedOn,
        ...otherProps
      } = this.props;
      return <WrappedComponent performLogIn={this.performLogIn} {...otherProps} />;
    }
  }

  const mapStateToProps = state => ({
    sessionStates: state.sessionStates,
    profileInfo: state.profileInfo,
    userProfile: state.userProfile,
    userList: state.userList
  });
  
  const mapDispatchToProps = dispatch => ({
    setLoggedOn: (isLoggedOn, selfUserId) => {
      dispatch(setLoggedOnAction(isLoggedOn, selfUserId));
    },
    setProfileInfo: (profileInfo, callback) => {
      dispatch(setProfileInfoAction(profileInfo, callback));
    },
    setConversations: (conversations) => {
      dispatch(setConversationsAction(conversations));
    },
    addConversation: (conversation) => {
      dispatch(addConversationAction(conversation));
    },
    addMessage: (message) => {
      dispatch(addMessageAction(message));
    },
    setUserList: (users, total, skip, limit) => {
      dispatch(setUserListAction(users, total, skip, limit));
    },
    setUsersFilters: (filters) => {
      dispatch(setUsersFiltersAction(filters));
    }
  });
  
  return withLoadingSpinner(connect(mapStateToProps, mapDispatchToProps)(SignInHOC));
}

export default withSignIn;
