import { connect } from 'react-redux';

import FilterSortToolbar from '../components/filter-sort-toolbar/FilterSortToolbar';
import withFetchUserList from '../containers/functional-hocs/withFetchUserList';
import { setUsersSortByAction } from '../actions';

const mapStateToProps = state => ({
  filters: state.userList.filters
});

const mapDispatchToProps = dispatch => ({
  setUsersSortBy: (sortBy) => {
    dispatch(setUsersSortByAction(sortBy))
  }
});

export default withFetchUserList(connect(mapStateToProps, mapDispatchToProps)(FilterSortToolbar));
