import React, { Component } from 'react';
import { connect } from 'react-redux';

import PreferenceInfoSection from '../components/preference-info-section/PreferenceInfoSection';

const buildBasicPreferenceInfo = (profileInfo) => {
  // put an example here
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.preference != null) {
    const preference = profileInfo.preference;
    let basicPreferenceInfoArray = [];
    if (preference.gender != null && preference.gender.length > 0) {
      if (preference.gender.length > 1) {
        basicPreferenceInfoArray.push('Gender doesn\'t matter');
      } else {
        basicPreferenceInfoArray.push(preference.gender[0]);
      }
    }
    if (preference.minAge != null && preference.maxAge != null) {
      basicPreferenceInfoArray.push('between ' + preference.minAge + ' and ' + preference.maxAge);
    } else if (preference.minAge != null) {
      basicPreferenceInfoArray.push('older than ' + preference.minAge);
    } else if (preference.maxAge != null) {
      basicPreferenceInfoArray.push('younger than ' + preference.maxAge);
    }
    if (preference.minHeight != null && preference.maxHeight != null) {
      basicPreferenceInfoArray.push('between ' + preference.minHeight + 'cm and ' + preference.maxHeight + 'cm');
    } else if (preference.minHeight != null) {
      basicPreferenceInfoArray.push('taller than ' + preference.minHeight);
    } else if (preference.maxHeight != null) {
      basicPreferenceInfoArray.push('shorter than ' + preference.maxHeight);
    }
    if (preference.martialStatus != null && preference.martialStatus.length > 0) {
      basicPreferenceInfoArray.push(preference.martialStatus);
    }
    if (preference.hasChildren != null && preference.hasChildren.length > 0) {
      basicPreferenceInfoArray.push(preference.hasChildren ? 'no children' : 'has children');
    }
    infoText = basicPreferenceInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildLanguagePreferenceInfo = (profileInfo) => {
  // put an example here
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.preference != null) {
    const preference = profileInfo.preference;
    let languagePreferenceInfoArray = [];
    if (preference.languages != null && preference.languages.length > 0) {
      languagePreferenceInfoArray.push('Speaks ' + preference.languages.join(', '));
    }
    infoText = languagePreferenceInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildExtraPreferenceInfo = (profileInfo) => {
  // put an example here
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.preference != null) {
    const preference = profileInfo.preference;
    let extraPreferenceInfoArray = [];
    if (preference.education != null && preference.education.length > 0) {
      extraPreferenceInfoArray.push(preference.education.join(', '));
    }
    if (preference.income != null && preference.income.length > 0) {
      extraPreferenceInfoArray.push(preference.income);
    }
    if (preference.religion != null && preference.religion.length > 0) {
      extraPreferenceInfoArray.push(preference.religion.join(', '));
    }
    if (preference.ethnicities != null && preference.ethnicities.length > 0) {
      extraPreferenceInfoArray.push(preference.ethnicities.join(', '));
    }
    infoText = extraPreferenceInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    };
  }
  return info;
};

const buildLifestylePreferenceInfo = (profileInfo) => {
  // put an example here
  let info = null;
  let infoText = '';
  if (profileInfo != null && profileInfo.preference != null) {
    const preference = profileInfo.preference;
    let lifestylePreferenceInfoArray = [];
    if (preference.smoking != null) {
      lifestylePreferenceInfoArray.push(preference.smoking + ' Smoke');
    }
    if (preference.drinking != null) {
      lifestylePreferenceInfoArray.push('Drinking ' + preference.drinking);
    }
    lifestylePreferenceInfoArray.join(', ');
  }
  if (infoText.length > 0) {
    info = {
      text: infoText
    }
  }
  return info;
};

class PreferenceInfoSectionContainer extends Component {
  constructor(props) {
    super(props);
    this.userId = this.props.match && this.props.match.params.userId;
  }

  resolveComponentProps = () => {
    let userProfile = null;
    if (this.userId == null) {
      userProfile = this.props.profileInfo;
    } else {
      // try to resolve from user list
      for (let i = 0; i < this.props.userList.users.length; i++) {
        if (this.userId === this.props.userList.users[i]._id) {
          userProfile = this.props.userList.users[i];
          break;
        }
      }
      // not found in user list, then check conversation message users
      if (userProfile == null) {
        const conversationKeys = Object.keys(this.props.conversations);
        for (let i = 0; i < conversationKeys.length; i++) {
          const conversation = this.props.conversations[conversationKeys[i]];
          if (this.userId === conversation.messageUser._id) {
            userProfile = conversation.messageUser;
            break;
          }
        }
      }
      // not resolved to any saved user
      // maybe make an API call
      // or return 404 page
    }
    // return infoData base on the resolved user profie
    return {
      infoData: {
        basicPreferenceInfo: buildBasicPreferenceInfo(userProfile),
        languagePreferenceInfo: buildLanguagePreferenceInfo(userProfile),
        extraPreferenceInfo: buildExtraPreferenceInfo(userProfile),
        lifestylePreferenceInfo: buildLifestylePreferenceInfo(userProfile),
      }
    };
  }

  render() {
    const {
      profileInfo,
      userList,
      conversations,
      ...otherProps
    } = this.props;
    return (
      <PreferenceInfoSection {...this.resolveComponentProps()} {...otherProps} />
    );
  }
}

const mapStateToProps = state => ({
  profileInfo: state.profileInfo,
  userList: state.userList,
  conversations: state.conversations
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(PreferenceInfoSectionContainer);
