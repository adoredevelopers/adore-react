import { connect } from 'react-redux';

import MessageActionControl from '../components/message-action-control/MessageActionControl';
import withSendMessage from './functional-hocs/withSendMessage';

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default withSendMessage(connect(mapStateToProps, mapDispatchToProps)(MessageActionControl));
