const navigationLinks = (pageName, ...params) => {
  switch (pageName) {
    case 'HOME_ROUTE':
      return '/';
    case 'REDIRECT_ROUTE':
      return '/redirect';
    case 'LOGIN_PAGE_ROUTE':
      return '/login';
    case 'REGISTRATION_PAGE_ROUTE':
      return '/register';
    case 'COMPLETE_PROFILE_PAGE_ROUTE':
      return '/complete-profile';
    case 'USER_LIST_PAGE_ROUTE':
      return '/users';
    case 'USER_DETAILS_PAGE_ROUTE':
      return params && params.length > 0 ? '/user/' + params[0] : '#';
    case 'EDIT_PROFILE_PAGE_ROUTE':
      return '/profile';
    case 'USER_ADORES_PAGE_ROUTE':
      return '/adores';
    case 'USER_MESSAGES_PAGE_ROUTE':
      return params && params.length > 0 ? '/messages/' + params[0] : '/messages';
    case 'NOTIFICATIONS_PAGE_ROUTE':
      return '/notifications';
    case 'SETTINGS_PAGE_ROUTE':
      return '/settings';
    default:
      return '#';
  }
};

export default navigationLinks;
