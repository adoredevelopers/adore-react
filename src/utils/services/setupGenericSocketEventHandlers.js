const setupGenericSocketEventHandlers = (api) => {
  api.socket.on('connect', function() {
    console.log('socket connected');
  });

  api.socket.on('custom event', function(payload) {
    console.log(payload);
  });
};

export default setupGenericSocketEventHandlers;
