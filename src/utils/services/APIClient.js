import feathers from '@feathersjs/client';
import socketio from '@feathersjs/socketio-client';
import io from 'socket.io-client';
import auth from '@feathersjs/authentication-client';
// import axios from 'axios';
// import localStorage from 'localstorage-memory';
import appConfig from '../../config/appConfig';
import setupGenericSocketEventHandlers from './setupGenericSocketEventHandlers';
import setupServiceSocketEventHandlers from './setupServiceSocketEventHandlers';

// singleton class
let instance = null;
class APIClient {
  constructor() {
    if (!instance) {
      console.debug('create new APIClient singleton instance to host: ' + appConfig.websocketHost);
      instance = this;

      this.socket = io(appConfig.websocketHost, {
        transports: ['websocket'],
        path: '/ws/',
        forceNew: true
      });
      // setup generic socket event handlers
      setupGenericSocketEventHandlers(this);

      this.client = feathers();
      this.client
        .configure(socketio(this.socket))
        .configure(auth({
          storageKey: 'auth'
        }));
      // setup feathers service socket event handlers
      setupServiceSocketEventHandlers(this);
    }
    return instance;
  }
}

export default APIClient;
