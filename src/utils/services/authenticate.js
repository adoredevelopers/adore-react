import APIClient from './APIClient';

const api = new APIClient();

// const authenticate = function(credentials, callback) {
//   const client = api.client;
//   return client.authenticate().then(function(response) {
//     console.info('Feathers Client has Authenticated with the JWT access token!');
//     return client.passport.verifyJWT(response.accessToken);
//   }).then(function(payload) {
//     console.info('JWT Payload', payload);
//     return client.service('users').get(payload.userId);
//   });
// };

const authenticate = async (credentials) => {
  const client = api.client;

  let authParams = null;
  if (credentials) {
    authParams = Object.assign({ strategy: 'local' }, credentials); 
  }

  const response = (authParams) ? await client.authenticate(authParams) : await client.reAuthenticate();
  
  console.info('Feathers Client has Authenticated with the JWT access token!');

  // verify the access token with JWT from feathers
  const payload = await client.reAuthenticate();
  console.debug('JWT Payload', payload);

  return payload.user;
};

export default authenticate;
