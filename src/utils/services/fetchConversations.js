import APIClient from './APIClient';
const api = new APIClient();

const client = api.client;
const fetchConversations = async (userId, callback) => {
  try {
    const conversations = await client.service('conversations').find({
      query: {
        owners: userId
      }
    });
    console.debug(conversations);
    callback(conversations);
  } catch (err) {
    console.error('Failed to load conversations: ');
    console.error(err);
  }
};

export default fetchConversations;
