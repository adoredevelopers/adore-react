import APIClient from './APIClient';
const api = new APIClient();

const client = api.client;
const fetchUserList = async (query, callback) => {
  try {
    const users = await client.service('users').find({
      query
    });
    console.debug(users);
    // attach redux properties and methods to features API
    api.redux.userList = users.data;
    callback(users);
  } catch (err) {
    console.error("Failed to load user list: ");
    console.error(err);
  }
};

export default fetchUserList;
