const setupServiceSocketEventHandlers = (api) => {
  api.client.service('conversations')
    .on('created', onCreateConversation);

  api.client.service('messages')
    .on('created', onCreateMessage);

  // handling functions
  function onCreateConversation(data) {
    let selfUserId = api.redux.selfUser._id;
    let conversationUserId = data.owners[0] === selfUserId ? data.owners[1] : data.owners[0];
    let users = api.redux.userList;
    for (let i = 0; i < users.length; i++) {
      if (users[i]._id === conversationUserId) {
        data.messageUser = users[i];
        break;
      }
    }
    api.redux.addConversation(data);
  }

  function onCreateMessage(data) {
    api.redux.addMessage(data);
  }
};

export default setupServiceSocketEventHandlers;
