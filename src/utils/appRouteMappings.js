import React from 'react';

import MainPageLayout from '../layouts/MainPageLayout';
import LoginPage from '../pages/login-page/LoginPage';
import RegistrationPage from '../pages/registration-page/RegistrationPage';
import UserListPage from '../pages/user-list-page/UserListPage';
import UserDetailsPage from '../pages/user-details-page/UserDetailsPage';
import EditProfilePage from '../pages/edit-profile-page/EditProfilePage';
import UserAdoresPage from '../pages/user-adores-page/UserAdoresPage';
import UserMessagesPage from '../pages/user-messages-page/UserMessagesPage';
import NotificationsPage from '../pages/notifications-page/NotificationsPage';
import SettingsPage from '../pages/settings-page/SettingsPage';

const appRouteMappings = (path, appProps, routeProps) => {
  switch (path) {
    case '/login':
      return <LoginPage {...appProps} {...routeProps} />;
    case '/register':
      return <RegistrationPage {...appProps} {...routeProps} />;
    case '/complete-profile':
      return <RegistrationPage {...appProps} completeProfile={true} {...routeProps} />;
    case '/users':
      return (
        <MainPageLayout>
          <UserListPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/user/:userId':
      return (
        <MainPageLayout>
          <UserDetailsPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/profile':
      return (
        <MainPageLayout>
          <EditProfilePage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/adores':
      return (
        <MainPageLayout>
          <UserAdoresPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/messages':
      return (
        <MainPageLayout>
          <UserMessagesPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/messages/:userId':
      return (
        <MainPageLayout>
          <UserMessagesPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/notifications':
      return (
        <MainPageLayout>
          <NotificationsPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    case '/settings':
      return (
        <MainPageLayout>
          <SettingsPage {...appProps} {...routeProps} />
        </MainPageLayout>
      );
    default:
      return <LoginPage {...appProps} {...routeProps} />;
  }
};

export default appRouteMappings;
