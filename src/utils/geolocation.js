const getGeolocation = (success, error) => {
  if (!navigator.geolocation){
    alert('Geolocation is not supported by your browser.');
    return;
  }

  navigator.geolocation.getCurrentPosition(success, error);
};

export default getGeolocation;
