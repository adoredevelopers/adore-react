import React from 'react';
import Fab from '@material-ui/core/Fab';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';

import DisplayImagesContainer from '../../containers/DisplayImagesContainer';
import ChangePhotosDialogContainer from '../../containers/ChangePhotosDialogContainer';
import FormTextField from '../../components/__form-field-elements/FormTextField';
import FormCheckboxField from '../../components/__form-field-elements/FormCheckboxField';
import FormRadioField from '../../components/__form-field-elements/FormRadioField';
import FormSelectionField from '../../components/__form-field-elements/FormSelectionField';
import { RequiredValidation } from '../validation-classes';

export function renderImageField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey) {
  let validationRules = [];
  if (fieldProps.isRequired) validationRules.push(RequiredValidation);
  return (
    <div className={classes.displayImagesFieldRow} key={uniqueKey}>
      <DisplayImagesContainer />
      <Fab
        color="secondary"
        aria-label="change photos"
        className={classes.changePhotosButton}
        onClick={parentHandlers.handleChangePhotosButtonClick}>
        <AddAPhotoIcon />
      </Fab>
      <ChangePhotosDialogContainer
        open={parentStates.changePhotosDialogOpen}
        maxWidth="sm"
        fullScreen={window.innerWidth < 600 ? true : false}
        fieldProps={fieldProps}
        validationRules={validationRules}
        validationResult={parentStates.validationResults[fieldProps.fieldId]}
        updateFormFieldValidity={parentHandlers.updateFormFieldValidity}
        onClose={parentHandlers.handleChangePhotoDialogRequestClose} />
    </div>
  );
};

export function renderTextField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey, isComboItem, isLastItem) {
const itemStyle = isLastItem ? {marginRight: '0'} : {};
  let validationRules = [];
  if (fieldProps.isRequired) validationRules.push(RequiredValidation);
  return (
    <div className={isComboItem ? classes.fieldRowItem : classes.fieldRow} style={itemStyle} key={uniqueKey}>
      <FormTextField
        fieldProps={fieldProps}
        dataObject={dataObject}
        validationRules={validationRules}
        validationResult={parentStates.validationResults[fieldProps.fieldId]}
        handleFormFieldChange={parentHandlers.handleFormFieldChange}
        updateFormFieldValidity={parentHandlers.updateFormFieldValidity} />
    </div>
  );
};

export function renderComboTextField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey) {
  const isComboItem = true;
  return (
    <div className={classes.fieldRow} key={uniqueKey}>
      {
        fieldProps.itemFields.map(function(itemFieldProps, idx) {
          const isLastItem = (idx === fieldProps.itemFields.length - 1);
          return renderTextField(classes, itemFieldProps, dataObject, parentStates, parentHandlers, 'comboFieldItem_' + idx, isComboItem, isLastItem);
        }, this)
      }
    </div>
  );
};

export function renderCheckboxField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey) {
  let validationRules = [];
  if (fieldProps.isRequired) validationRules.push(RequiredValidation);
  return (
    <div className={classes.fieldRow} key={uniqueKey}>
      <FormCheckboxField
        fieldProps={fieldProps}
        dataObject={dataObject}
        validationRules={validationRules}
        validationResult={parentStates.validationResults[fieldProps.fieldId]}
        handleFormFieldChange={parentHandlers.handleFormFieldChange}
        updateFormFieldValidity={parentHandlers.updateFormFieldValidity} />
    </div>
  );
};

export function renderRadioField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey) {
  let validationRules = [];
  if (fieldProps.isRequired) validationRules.push(RequiredValidation);
  return (
    <div className={classes.fieldRow} key={uniqueKey}>
      <FormRadioField
        fieldProps={fieldProps}
        dataObject={dataObject}
        validationRules={validationRules}
        validationResult={parentStates.validationResults[fieldProps.fieldId]}
        handleFormFieldChange={parentHandlers.handleFormFieldChange}
        updateFormFieldValidity={parentHandlers.updateFormFieldValidity} />
    </div>
  );
};

export function renderSelectionField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey, isComboItem, isLastItem) {
  const itemStyle = isComboItem && isLastItem ? {marginRight: '0'} : {};
  let validationRules = [];
  if (fieldProps.isRequired) validationRules.push(RequiredValidation);
  return (
    <div className={isComboItem ? classes.fieldRowItem : classes.fieldRow} style={itemStyle} key={uniqueKey}>
      <FormSelectionField
        multi={fieldProps.fieldSubType === 'multiSelectionField' || fieldProps.fieldSubType === 'creatableMultiSelectionField'}
        creatable={fieldProps.fieldSubType === 'creatableSelectionField' || fieldProps.fieldSubType === 'creatableMultiSelectionField'}
        fieldProps={fieldProps}
        dataObject={dataObject}
        validationRules={validationRules}
        validationResult={parentStates.validationResults[fieldProps.fieldId]}
        handleFormFieldChange={parentHandlers.handleFormFieldChange}
        updateFormFieldValidity={parentHandlers.updateFormFieldValidity}
        style={isComboItem ? {borderRadius: '0'} : {borderRadius: '0', width: '100%'}} />
    </div>
  );
};

export function renderComboSelectionField(classes, fieldProps, dataObject, parentStates, parentHandlers, uniqueKey) {
  const isComboItem = true;
  return (
    <div className={classes.fieldRow} key={uniqueKey}>
      {
        fieldProps.itemFields.map(function(itemFieldProps, idx) {
          const isLastItem = (idx === fieldProps.itemFields.length - 1);
          return renderSelectionField(classes, itemFieldProps, dataObject, parentStates, parentHandlers, 'comboFieldItem_' + idx, isComboItem, isLastItem);
        }, this)
      }
    </div>
  );
};

// params: classes, sectionFields, profileInfoDelta, parentStates, parentHandlers, keyPrefix
export function renderSectionFields(classes, sectionFields, dataObject, parentStates, parentHandlers, keyPrefix) {
  keyPrefix = keyPrefix ? keyPrefix : 'sectionField_';
  return sectionFields.map((sectionFieldProps, idx) => {
    switch (sectionFieldProps.fieldType) {
      case 'imageField':
        return renderImageField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'textField':
        return renderTextField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'comboTextField':
        return renderComboTextField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'checkboxField':
        return renderCheckboxField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'radioField':
        return renderRadioField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'selectionField':
        return renderSelectionField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      case 'comboSelectionField':
        return renderComboSelectionField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
      default:
        return renderTextField(
          classes,
          sectionFieldProps,
          dataObject,
          parentStates,
          parentHandlers,
          keyPrefix + idx
        );
    }
  });
};
