import moment from 'moment';

export function getAgeFromBirthDate(dateString) {
  return moment().diff(moment(dateString), 'years')
};

export function getZodiacFromBirthDate(dateString) {
  const zodiacArray = ['Capricorn', 'Aquarius', 'Pisces', 'Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius'];
  const date = new Date(dateString);
  const month = date.getMonth();
  const day = date.getDay();
  let zodiac = '';
  switch (month) {
    case 0: //January
      if (day < 20)
        zodiac = zodiacArray[0];
      else
        zodiac = zodiacArray[1];
      break;
    case 1: //February
      if (day < 19)
        zodiac = zodiacArray[1];
      else
        zodiac = zodiacArray[2];
      break;
    case 2: //March
      if (day < 21)
        zodiac = zodiacArray[2];
      else
        zodiac = zodiacArray[3];
      break;
    case 3: //April
      if (day < 20)
        zodiac = zodiacArray[3];
      else
        zodiac = zodiacArray[4];
      break;
    case 4: //May
      if (day < 21)
        zodiac = zodiacArray[4];
      else
        zodiac = zodiacArray[5];
      break;
    case 5: //June
      if (day < 21)
        zodiac = zodiacArray[5];
      else
        zodiac = zodiacArray[6];
      break;
    case 6: //July
      if (day < 23)
        zodiac = zodiacArray[6];
      else
        zodiac = zodiacArray[7];
      break;
    case 7: //August
      if (day < 23)
        zodiac = zodiacArray[7];
      else
        zodiac = zodiacArray[8];
      break;
    case 8: //September
      if (day < 23)
        zodiac = zodiacArray[8];
      else
        zodiac = zodiacArray[9];
      break;
    case 9: //October
      if (day < 23)
        zodiac = zodiacArray[9];
      else
        zodiac = zodiacArray[10];
      break;
    case 10: //November
      if (day < 22)
        zodiac = zodiacArray[10];
      else
        zodiac = zodiacArray[11];
      break;
    case 11: //December
      if (day < 22)
        zodiac = zodiacArray[11];
      else
        zodiac = zodiacArray[0];
      break;
    default:
      zodiac = '';
  }
  return zodiac;
};

export function getChineseZodiacFromBirthDate(dateString) {
  const cZodiacArray = ['Rat', 'Ox', 'Tiger', 'Rabbit', 'Dragon', 'Snake', 'Horse', 'Goat', 'Monkey', 'Rooster', 'Dog', 'Pig'];
  const date = new Date(dateString);
  const year = date.getFullYear();
  const index = year % 12 - 4;
  return cZodiacArray[index];
};

export function getDataFieldValueFromDataObjectWithFieldPathAndFieldId(dataObject, fieldProps) {
  if (fieldProps.fieldPath) {
    let pathTokens = fieldProps.fieldPath.split('.');
    for (let i = 0; i < pathTokens.length; i++) {
      if (dataObject) {
        dataObject = dataObject[pathTokens[i]];
      } else {
        return null; // return null as value if the data object or path objects are not defined
      }
    }
  }
  return dataObject ? dataObject[fieldProps.fieldId] : null;
};

export function addValueToSelectionFieldOptions(options, value) {
  const addSingleValueToSelectionFieldOptions = (options, singleValue) => {
    for (let i = 0; i < options.length; i++) {
      if (singleValue === options[i].value) {
        return options;
      }
    }
    options.push({
      label: singleValue.charAt(0).toUpperCase() + singleValue.slice(1),
      value: singleValue
    });
    return options;
  };

  if (Array.isArray(value)) {
    value.forEach((singleValue, idx) => {
      options = addSingleValueToSelectionFieldOptions(options, singleValue);
    });
    return options;
  } else {
    return addSingleValueToSelectionFieldOptions(options, value);
  }
};

export function mergeSelectionFieldOptions(options1, options2) {
  // using option.value as the unique identifier for this no-duplicate merging
  let options2Outliers = [];
  for (let i = 0; i < options2.length; i++) {
    let found = false;
    for (let j = 0; j < options1.length; j++) {
      if (options2[i].value === options1[j].value) { // found current option of options1 in options2, skip
        found = true;
        break;
      }
    }
    if (!found) options2Outliers.push(options2[i]);
  }
  return options2Outliers.length > 0 ? options1.concat(options2Outliers) : options1;
};

export function generateMessageOwnerIds(userId1, userId2) {
  let ownerIds = [];
  if (userId1.toString() < userId2.toString()) {
    ownerIds.push(userId1);
    ownerIds.push(userId2);
  } else {
    ownerIds.push(userId2);
    ownerIds.push(userId1);
  }
};

export function buildFiltersQuery(filtersProp) {
  let filters = {};
  if (filtersProp.minAge != null && filtersProp.maxAge != null) {
    const minAgeDate = moment().subtract(filtersProp.minAge, 'years').toDate(); // upperbound for date
    const maxAgeDate = moment().subtract(filtersProp.maxAge, 'years').toDate(); // lowerbound for date
    filters['profile.basic.gender'] = filtersProp.gender;
    filters['profile.basic.birthday'] = {
      $lte: minAgeDate,
      $gte: maxAgeDate
    };
  }
  // filters['photoVerified'] = true;
  if (filtersProp.country != null) {
    filters['profile.basic.residence.country'] = filtersProp.country;
  }
  if (filtersProp.region != null) {
    filters['profile.basic.residence.province'] = filtersProp.region;
  }
  return filters;
};

export function normalizeFormValues(values) {
  let normalizedValues = {};
  const keys = Object.keys(values);
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];
    const valueItem = values[key];
    if (key == 'displayImages') {
      normalizedValues[key] = JSON.parse(valueItem);
    }
    // check type Array first before Object, since Array is type Object and Object isn't necessarily Array
    if (valueItem instanceof Array) {
      normalizedValues[key] = valueItem.map((item) => {
        return item.value;
      });
    } else if (valueItem instanceof Object) {
      normalizedValues[key] = valueItem.value.toString().trim().length > 0 ? valueItem.value : null;
    } else {
      normalizedValues[key] = valueItem;
    }
  }
  return normalizedValues;
};
